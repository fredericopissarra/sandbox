#!/bin/bash

#DEC="-hwaccel vaapi -vaapi_device /dev/dri/renderD128"
ENC="mpeg2video"
DEC="-hwaccel cuvid"

usage() {
  echo -e "\e[1mUsage\e[m: `basename $0` [-v vbr] [-a abr]  <video1> [... <videoN>]"
}

if [ -z "$1" ]; then 
  usage
	exit 1
fi

interruption() {
  [[ -e "${TMPFNAME}" ]] && rm "${TMPFNAME}"
}

trap interruption INT

# Command line processing
VBR=''
ABR="128k"
while getopts "v:a:" OPT; do
  case $OPT in
    v) VBR=$OPTARG;;
    a) ABR=$OPTARG;;
    ?) usage; exit 1;;
  esac
done

shift $((OPTIND-1))

for i in "$@"; do
  if [ ! -f "$i" ]; then
    echo -e "\e[31;1mError\e[m: File '$i' can't be found!"
    continue
  fi

  FTYPE="${i#*.}"
  FTYPE="${FTYPE^^*}"
  if [ "$FTYPE" == "MPG" ] || [ "$FTYPE" == "MPEG" ]; then
    echo -e "\e[1;31mError\e[0m: File '$i' is already a MPEG video."
    continue
  fi

  VPARAMS="-c:v ${ENC}"
  FR='23.976'

  EXTRA_PARAMS="-map_metadata -1" # Retira metadados do vídeo destino.

  if [ ! -z "$VBR" ]; then
    VPARAMS="$VPARAMS -b:v ${VBR} -maxrate ${VBR} -bufsize ${VBR}"
  else
    W="`mediainfo --inform="Video;%Width%" "$i"`"
    H="`mediainfo --inform="Video;%Height%" "$i"`"
    BR="`bc <<< "scale=3; ($W * $H * $FR)/13.824" | sed -E 's/\..+$//'`"
    echo -e "\e[33;1mCalculated optimal video bitrate\e[m: ${BR} b/s"
    VPARAMS="$VPARAMS -maxrate ${BR} -bufsize ${BR} -b:v ${BR}"
  fi

  # Adicionar "-vf format=nv12,hwupload" se VAAPI
  VPARAMS="$VPARAMS -r ntsc-film"
  APARAMS="$APARAMS -b:a ${ABR} -ac 2 -ar 44.1k -async 1"

  TMPFNAME="$(mktemp /tmp/tmp_XXXXXXXXXX.mpg)"
  OFNAME="${i%.*}.mpg"

  echo -e "\e[1;33mConvering '\e[0m${i}\e[1;33m' to MPEG2...\e[0m"
  ffmpeg -v error -stats ${DEC} -y -i "$i" $VPARAMS $APARAMS $EXTRA_PARAMS $TMPFNAME && \
    mv "$TMPFNAME" "$OFNAME"
done

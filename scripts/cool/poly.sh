#!/bin/bash

C="254,278"
PP="30,362 212,44 350,287 272,287 212,187 70,430"

convert -size 500x500 xc:\#101 -alpha on -stroke green -strokewidth 2 -fill \#1C1 -draw "polygon $PP" \
        -distort SRT "$C 120" -fill \#1A1 \
        -draw "polygon $PP" -distort SRT "$C 120" -fill \#181 \
        -draw "polygon $PP" mce.png

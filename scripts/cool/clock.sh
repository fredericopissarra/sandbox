#!/bin/bash
#
# $ sudo apt install parallel
# $ parallel --citation   # to disable citation.

echo 'Creating frames...'
parallel -q \
convert -size 300x300 xc:black -stroke green \
  -draw '{= sub l { ($r,$v) = @_; $v *= atan2(1, 0) * 4 / 360; "line 150,150 ".int(150 - $r * sin($v)).",".int(150 + $r * cos($v)).";"; }; $_ = l(25, $_ / 2 + 180).l(50, $_ % 60 * 6 + 180); =}' \
  C{}.gif ::: {000..720} 

echo 'Creating GIF...'
convert C*gif clock.gif

echo 'Deleting temporary files...'
rm C*gif

echo 'Done'

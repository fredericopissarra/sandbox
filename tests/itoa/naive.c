#include <stdio.h>
#include <string.h>
#include <ctype.h>

void itoa10( int n, char *p )
{
  char buffer[12];
  char *e = buffer + sizeof buffer;
  char *q = e - 1;
  long long int m;  // Need better precision because -INT_MIN cannot be
                    // represented in an 'int'.

  m = ( n < 0 ) ? -n : n;

  *q = '\0';

  // These divisions will probably be transformed in a
  // single multiplication.
  do
    *--q = '0' + m % 10;
  while ( m /= 10 );

  if ( n < 0 )
    *--q = '-';

  memcpy( p, q, e - q );
}

int atoi10( const char *p )
{
  _Bool negative = 0;
  int oldn;
  int n = 0;

  if ( *p == '-' )
  {
    negative = 1;
    p++;
  }

  oldn = 0;
  while ( *p )
  {
    if ( ! isdigit( *p ) )
      break;

    n *= 10;
    n += *p - '0';

    if ( n < oldn )
    {
      n = oldn;
      break;
    }

    oldn = n;

    p++;
  }

  if ( negative )
    n = -n;

  return n;
}

int main( void )
{
  char buff[12];

  itoa10( 0, buff );

  puts( buff );

  printf( "%d\n", atoi10( "0" ) );
}

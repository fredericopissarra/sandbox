; hello64.asm
;
;   nasm -fwin64 -o hello64.o hello64.asm
;   ld -s -o hello64.exe hello64.o -lkernel32
;
; Add -DUSE_ANSI if you whish to print in color, using ANSI escape codes.
; This works in Win10/11 -- Don't know if works in older versions.
;

  ; It is prudent to tell NASM we are using x86_64 instructionsset.
  ; And, MS ABI (as well as SysV ABI) requires RIP relative addressing
  ; by default (PIE targets).
  bits  64
  default rel

  ; Some symbols (got from MSDN)
  %define ENABLE_VIRTUAL_TERMINAL_PROCESSING    4
  %define STDOUT_HANDLE                       -11

  ; It is nice to keep unmutable data in an read-only section.
  ; On Windows the system section for this is `.rdata`.
  section .rdata

msg:
%ifdef USE_ANSI
  db    `\033[1;31mH\033[1;32me\033[1;33ml\033[1;34ml\033[1;35mo\033[m`
%else
  db    `Hello`
%endif
  db    `\n`

msg_len equ $ - msg

  section .bss

  ; This is kept in memory because GetConsoleMode requires a pointer.
mode:
  resd  1

  section .text

  ; Functions from kernel32.dll.
  extern __imp_GetStdHandle
  extern __imp_WriteConsoleA
  extern __imp_ExitProcess
  %ifdef USE_ANSI
    extern __imp_GetConsoleMode
    extern __imp_SetConsoleMode
  %endif

  ; Stack structure.
  struc stk
              resq  4   ; shadow area
  .reserved:  resq  1
              resq  1   ; alignment.
  endstruc

  global _start

_start:
  sub   rsp,stk_size ; Reserve space for SHADOW AREA and one argument
                     ; (WriteConsoleA requires it).
                     ; On Windows RSP enters here already DQWORD aligned.

  mov   ecx,STDOUT_HANDLE
  call  [__imp_GetStdHandle]

  %ifdef USE_ANSI
    ; Since RBX is preserved between calls, I'll use it to save the handle.
    mov   rbx,rax

    mov   rcx,rax
    lea   rdx,[mode]
    call  [__imp_GetConsoleMode]

    ; Change the console mode. 
    mov   edx,[mode]
    or    edx,ENABLE_VIRTUAL_TERMINAL_PROCESSING
    mov   rcx,rbx
    call  [__imp_SetConsoleMode]

    mov   rcx,rbx
  %else
    mov   rcx,rax
  %endif
  ; Above: RCX is the first argument for WriteConsoleA.

  lea   rdx,[msg]
  mov   r8d,msg_len
  xor   r9d,r9d
  mov   qword [rsp + stk.reserved],0  ; 5th argument goes to the stack.
  call  [__imp_WriteConsoleA]

  ; Exit the program.
  xor   ecx,ecx
  jmp   [__imp_ExitProcess]

  ; Never reaches here.
  ; The normal thing to do should be restore RSP to its original state...

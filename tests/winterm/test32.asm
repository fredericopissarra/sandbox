; hello32.asm
;
; nasm -fwin32 -o hello32.o hello32.asm
; i686-w64-mingw32-ld -s -o hello32.exe hello32.o -lkernel32
;
; Defube USE_ANSI (-DUSE_ANSI) if you want colored text.
;
  bits  32

  ; Got from MSDN Library or Win32 SDK include files.
  %define ENABLE_VIRTUAL_TERMINAL_PROCESSING 4
  %define STDOUT_HANDLE -11

  section .rodata

msg:
  %ifdef USE_ANSI
    db    `\033[1;31mH\033[1;32me\033[1;33ml\033[1;34ml\033[1;35mo\033[m\r\n`
  %else
    db    `Hello\r\n`
  %endif
msg_len equ $ - msg

  section .text

  ; Imported from kernel32.dll
  extern __imp__GetStdHandle@4

  %ifdef USE_ANSI
    extern __imp__GetConsoleMode@8
    extern __imp__SetConsoleMode@8
  %endif

  extern __imp__WriteConsoleA@20
  extern __imp__ExitProcess@4

  global _start

_start:
  push  STDOUT_HANDLE
  call  [__imp__GetStdHandle@4]

  %ifdef USE_ANSI
    mov   ebx,eax                 ; Store handle in ebx (MS-ABI preserves ebx).

    sub   esp,4

    ; Habilita VT100 no Windows Terminal.
    push  esp
    push  eax
    call  [__imp__GetConsoleMode@8]
    mov   eax,[esp]

    add   esp,4

    or    eax,ENABLE_VIRTUAL_TERMINAL_PROCESSING
    push  eax
    push  ebx
    call  [__imp__SetConsoleMode@8]

    mov   eax,ebx
  %endif

  push  0
  push  0
  push  msg_len
  push  msg
  push  eax
  call  [__imp__WriteConsoleA@20]

  push  0
  jmp   [__imp__ExitProcess@4]

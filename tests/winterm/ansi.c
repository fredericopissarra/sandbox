#include <windows.h>

static void EnableANSI( HANDLE );

#if 0
static void DisableANSI( HANDLE );
#endif

int main( void )
{
  static const char msg[] = "\033[1;31mH\033[1;32me\033[1;33ml\033[1;34ml\033[1;35mo\033[m\n";
  DWORD writen;

  HANDLE hConsole;

  hConsole = GetStdHandle( STD_OUTPUT_HANDLE );

  EnableANSI( hConsole );

  WriteConsole( hConsole, msg, sizeof msg - 1, &writen, NULL );
}

void EnableANSI( HANDLE hConsole )
{ 
  DWORD mode;

  GetConsoleMode( hConsole, &mode );
  SetConsoleMode( hConsole, mode | ENABLE_VIRTUAL_TERMINAL_PROCESSING );
}

#if 0
void DisableANSI( HANDLE hConsole )
{ 
  DWORD mode;

  GetConsoleMode( hConsole, &mode );
  SetConsoleMode( hConsole, mode & ~ENABLE_VIRTUAL_TERMINAL_PROCESSING );
}
#endif

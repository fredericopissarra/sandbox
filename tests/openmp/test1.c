#include <unistd.h>
#include <stdio.h>

int main( void )
{
  static const char str[] = "Hello\n";

  // Usa todos os processadores...
  #pragma omp parallel
  write( STDOUT_FILENO, str, sizeof str - 1 );
}

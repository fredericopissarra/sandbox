#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include <time.h>

#include "../../src/cycle_counting.h"

double avg1( double *p, unsigned int nelems )
{
  double s = 0.0;

  for ( unsigned int i = 0; i < nelems; i++ )
    s += p[i];

  return s / nelems;
}

double avg2( double *p, unsigned int nelems )
{
  double s = 0.0;

  #pragma omp simd reduction(+:s)
  for ( unsigned int i = 0; i < nelems; i++ )
    s += p[i];

  return s / nelems;
}

// SLOWER!
double avg3( double *p, unsigned int nelems )
{
  double s = 0.0;

  #pragma omp parallel for reduction(+:s)
  for ( unsigned int i = 0; i < nelems; i++ )
    s += p[i];

  return s / nelems;
}

int main( void )
{
  counter_T c1, c2, c3;
  double v[512];
  double m1, m2, m3;

  srand( time( NULL ) );
  for ( int i = 0; i < sizeof v / sizeof v[0]; i++ )
    v[i] = rand();

  c1 = BEGIN_TSC();
  m1 = avg1( v, sizeof v / sizeof v[0] );
  c1 = END_TSC( c1 );

  c2 = BEGIN_TSC();
  m2 = avg2( v, sizeof v / sizeof v[0] );
  c2 = END_TSC( c2 );

  c3 = BEGIN_TSC();
  m3 = avg3( v, sizeof v / sizeof v[0] );
  c3 = END_TSC( c3 );

  printf( "%f (%" PRIu64 " ciclos), "
          "%f (%" PRIu64 " ciclos, %.2f%%) "
          "%f (%" PRIu64 " ciclos, %.2f%%)\n",
    m1, c1, m2, c2, 100.0 * c2 / c1, m3, c3, 100.0 * c3 / c1 );
}

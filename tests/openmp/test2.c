#include <stdio.h>
#include <omp.h>

int main( void )
{
  int i;

  // Usa todos os processadores...
  #pragma omp parallel for
  for ( i = 0; i < 8; i++ )
    printf( "%d\n", i );
}

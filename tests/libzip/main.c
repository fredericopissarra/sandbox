#include <stdio.h>
#include <stdlib.h>
#include <zip.h>

static void freebuffer_( char **p ) { free( *p ); }
static void zipclose_( zip_t **p ) { if ( *p ) zip_close( *p ); }
static void zipfclose_( zip_file_t **p ) { if ( *p ) zip_fclose( *p ); }

int main ( void )
{
  static const char name[] = "file.txt";
  struct zip_stat st;
  int err = 0;

  //Open the ZIP archive
  zip_t *z __attribute__((cleanup(zipclose_))) = zip_open ( "foo.zip", 0, &err );

  if ( ! z )
  {
    fputs( "ERROR opening foo.zip.\n", stderr );
    return EXIT_FAILURE;
  }

  //Search for the file of given name

  zip_stat_init ( &st );
  zip_stat ( z, name, 0, &st );

  //Alloc memory for its uncompressed contents
  char *contents __attribute__((cleanup(freebuffer_))) = malloc ( st.size + 1 );

  if ( ! contents )
  {
    fputs ( "ERROR allocating memory.\n", stderr );
    return EXIT_FAILURE;
  }

  //Read the compressed file
  zip_file_t *f __attribute__((cleanup(zipfclose_))) = zip_fopen ( z, name, 0 );

  if ( ! f )
  {
    fputs( "ERROR: cannot find entry in zip file.\n", stderr );
    return EXIT_FAILURE;
  }

  zip_fread ( f, contents, st.size );
  zip_fclose ( f ); f = NULL;

  zip_close ( z ); z = NULL;

  // Put NUL char.
  contents[st.size] = '\0';

  fputs( contents, stdout );

  return EXIT_SUCCESS;
}

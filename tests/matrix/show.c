#include <stdio.h>

void show_matrix( float m[][4] )
{
  int i, j;

  putchar( '[' );
  for ( i = 0; i < 3; i++ )
  {
    fputs( "  ", stdout );
    for ( j = 0; j < 4; j++ )
    {
      printf( "%4.2f ", m[i][j] );
    }
    fputs( "\n ", stdout );
  }

  fputs( "  ", stdout );
  for ( i = 0; i < 4; i++ )
    printf( "%4.2f ", m[3][i] );
  puts( "]" );
}

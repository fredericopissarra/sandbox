#include <x86intrin.h>

/* Multiplica usando SSE3 e macro de transposição. */
void mult4x4_3 ( float r[][4], float a[][4], float b[][4] )
{
  int i, j;
  __m128 m[4];
  __m128 *p, t;

  for ( i = 0 ; i < 4; i++ )
    m[i] = _mm_loadu_ps ( ( float * ) b[i] );

  _MM_TRANSPOSE4_PS ( m[0], m[1], m[2], m[3] );

  for ( i = 0; i < 4; i++ )
  {
    p = ( __m128 * ) a[i];

    for ( j = 0; j < 4; j++ )
    {
      t = _mm_mul_ps ( *p, m[j] );
      t = _mm_hadd_ps ( t, t );
      t = _mm_hadd_ps ( t, t );
      r[i][j] = _mm_cvtss_f32 ( t );
    }
  }
}

/* Multiplicação otimizada, usando SSE */
void mult4x4_4(float *r, float *a, float *b)
{
  int i, j;
  __m128 a2, b2, r2;

  for (i = 0; i < 16; i += 4)
  {
    b2 = _mm_set1_ps(b[i]);
    a2 = _mm_load_ps(a);
    r2 = _mm_mul_ps(a2, b2);

    for (j = 1; j < 4; j++)
    {
      b2 = _mm_set1_ps(b[i+j]);
      a2 = _mm_loadu_ps(&a[j*4]);
      r2 = _mm_add_ps(_mm_mul_ps(a2, b2), r2);
    }

    _mm_storeu_ps(&r[i], r2);
  }
}

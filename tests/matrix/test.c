#include <stdio.h>
#include <cycle_counting.h>

extern void mult4x4_1( float [][4], float [][4], float [][4] );
extern void mult4x4_2a( float [][4], float [][4], float [][4] );
extern void mult4x4_2b( float [][4], float [][4], float [][4] );
extern void mult4x4_3( float [][4], float [][4], float [][4] );
extern void mult4x4_4( float *, float *, float * );

extern void show_matrix( float [][4] );

int main( void )
{
  counter_T c1, c2a, c2b, c3, c4;

  // identity
  float a[][4] = { { 1, 0, 0, 0 },
                   { 0, 1, 0, 0 },
                   { 0, 0, 1, 0 },
                   { 0, 0, 0, 1 } };

  float b[][4] = { { 1, 2, 3, 4 },
                   { 5, 6, 7, 8 },
                   { 9, 10, 11, 12 },
                   { 13, 14, 15, 16 } };

  float r1[4][4], r2a[4][4], r2b[4][4], r3[4][4], r4[4][4];

  c1 = BEGIN_TSC();
    mult4x4_1( r1, a, b );
  c1 = END_TSC(c1);

  c2a = BEGIN_TSC();
    mult4x4_2a( r2a, a, b );
  c2a = END_TSC(c2a);

  c2b = BEGIN_TSC();
    mult4x4_2b( r2b, a, b );
  c2b = END_TSC(c2b);

  c3 = BEGIN_TSC();
    mult4x4_3( r3, a, b );
  c3 = END_TSC(c3);

  c4 = BEGIN_TSC();
    mult4x4_4( (float *)r4, (float *)a, (float *)b );
  c4 = END_TSC(c4);

  show_matrix( r1 );
  show_matrix( r2a );
  show_matrix( r2b );
  show_matrix( r3 );
  show_matrix( r4 );

  printf( "mult4x4_1()  [unrolled]          - cycles: %lu.\n"
          "mult4x4_2a() [classic]           - cycles: %lu.\n"
          "mult4x4_2b() [classic,optimized] - cycles: %lu.\n"
          "mult4x4_3()  [sse3, w/transpose] - cycles: %lu.\n"
          "mult4x4_4()  [sse3, optimized ]  - cycles: %lu.\n", c1, c2a, c2b, c3, c4 );
}

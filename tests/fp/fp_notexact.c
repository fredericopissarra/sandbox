#include <stdio.h>

struct dbl_struc
{
  unsigned long f: 52;
  unsigned long e: 11;
  unsigned long s: 1;
};

static void showfpstruc ( double );

int main ( void )
{
  int i;
  double f, sum, prod;

  f = 0.1;
  sum = 0.0;

  for ( i = 0; i < 10; ++i )
    sum += f;

  prod = f * 10.0;

  printf ( "sum = %.20f, product = %.20f\n", sum, f * 10.0 );

  showfpstruc ( sum );
  showfpstruc ( prod );
}

void showfpstruc ( double x )
{
  unsigned long d = * ( unsigned long * ) &x;
  struct dbl_struc *p = ( struct dbl_struc * ) &x;

  printf ( "%.20f -> 0x%016lx -> { f: 0x%016lx, e = %ld, s = %c }\n",
           x,
           d,
           ( unsigned long ) p->f, ( long ) p->e - 1023L, ( char ) p->s ? '-' : '+' );
}


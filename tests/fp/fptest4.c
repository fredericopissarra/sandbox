#include <stdio.h>
#include <stdlib.h>
#include <math.h>

float division ( float a, float b )
{
  return a / b;
}

int main ( int argc, char **argv )
{
  float f1;

  if ( ! argv[1] )
  {
    fputs ( "fptest4 <value>\n", stderr );
    return EXIT_FAILURE;
  }

  f1 = strtof ( argv[1], NULL );

  f1 = division ( f1, 0.0f );

  if ( isinf ( f1 ) )
    printf ( "Inifinity!\n" );
  else if ( isnan ( f1 ) )
    printf ( "Not a number!\n" );
  else
    printf ( "%.10g\n", f1 );

  return EXIT_SUCCESS;
}

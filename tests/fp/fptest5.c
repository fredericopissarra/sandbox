#include <stdio.h>

#define SHOW_RESULT(c__) \
  printf( "[%s]\n", (c__) ? "\033[1;32myes\033[m" : "\033[1;31mno\033[m" )

void testfp1 ( void )
{
  double x = 1.2;

  printf ( "x = 1.2 - 0.4 - 0.4 - 0.4; x == 0.0? " );

  x -= 0.4;
  x -= 0.4;
  x -= 0.4;

  /* x should be 0.0, right!? Wrong! */
  SHOW_RESULT ( x == 0.0 );
}

void testfp2 ( void )
{
  double x;
  double y;

  printf ( "x = (0.1 + 0.2) + 0.3; y = 0.1 + (0.2 + 0.3); x == y ? " );

  x = ( 0.1 + 0.2 ) + 0.3;
  y = 0.1 + ( 0.2 + 0.3 );

  /* x == y, right? Wrong! */
  SHOW_RESULT ( x == y );
}

void testfp3 ( void )
{
  double x;
  double y;

  printf ( "x = (0.1 * 0.2) * 0.3; y = 0.1 * (0.2 * 0.3); x == y? " );

  x = ( 0.1 * 0.2 ) * 0.3;
  y = 0.1 * ( 0.2 * 0.3 );

  /* x == y, right? Wrong! */
  SHOW_RESULT ( x == y );
}

void testfp4 ( void )
{
  double x;
  double y;

  printf ( "x = (0.1 + 0.2) * 0.3; y = (0.1 * 0.3) + (0.2 * 0.3); x == y? " );

  x = ( 0.1 + 0.2 ) * 0.3;
  y = ( 0.1 * 0.3 ) + ( 0.2 * 0.3 );

  /* x == y, right? Wrong! */
  SHOW_RESULT ( x == y );
}

int main ( void )
{
  testfp1();
  testfp2();
  testfp3();
  testfp4();

  return 0;
}

/*
    perf.c

    Compile with:
      cc -O2 -o perf perf.c
 */
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <inttypes.h>
#include <sys/ioctl.h>
#include <sys/syscall.h>

#include <linux/capability.h>
#include <linux/perf_event.h>
#include <asm/unistd.h>   // for __NR_perf_event_open.

static _Bool check_perfmon_capability( void )
{
  // Using 32 bits capabilities bitmasks is enough.
  struct __user_cap_header_struct chs = { .version = _LINUX_CAPABILITY_VERSION_1, .pid = getpid() };
  struct __user_cap_data_struct cds;

  // If the syscall fails, assume no capability for RAW sockets.
  // 'man capget' says there's no wrapper for this syscall.
  if ( syscall( SYS_capget, &chs, &cds ) )
    return 0;

  return !! ( cds.permitted & CAP_PERFMON );
}

int main( int argc, char **argv )
{
  int fd;
  uint64_t count;
  struct perf_event_attr pe = { 
    .type = PERF_TYPE_HARDWARE,
    .size = sizeof pe,
    .config = PERF_COUNT_HW_CPU_CYCLES,
    .disabled = 1,
    .exclude_kernel = 1,
    .exclude_hv = 1
  };

  if ( ! check_perfmon_capability() )
  {
    fputs( "ERROR: User don't have the priviledge to use performance monitoring.\nTry 'sudo'.\n", stderr );
    return EXIT_FAILURE;
  }

  // There's no wrapper for this syscall...
  fd = syscall( __NR_perf_event_open, &pe, getpid(), -1, -1, 0 );
  if ( fd < 0 )
  {
    fputs( "Error opening PMC.\n", stderr );
    return EXIT_FAILURE;
  }

  // Reseta e enable the performance counter.
  ioctl( fd, PERF_EVENT_IOC_RESET, 0 );
  ioctl( fd, PERF_EVENT_IOC_ENABLE, 0 );

  puts( "Measuring instruction count for this puts()..." );

  // Disable the performance counter (stop it) and read the descriptor.
  ioctl( fd, PERF_EVENT_IOC_DISABLE, 0 );
  read( fd, &count, sizeof count );
  close( fd );

  printf( "puts: %" PRIu64 " cycles.\n", count );

  return EXIT_SUCCESS;
}


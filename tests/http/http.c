/* http.c

   Compilar com:
      gcc -O2 -o http http.c

   Uso:
      ./http http://google.com/
*/

#define _GNU_SOURCE
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <regex.h>
#include <libgen.h>

static int getURIandPath( char *, char **, char ** );
static int opensocket( char *, struct sockaddr_in * );
static int sendRequest( int, char *, char * );

int main( int argc, char *argv[] )
{
  int fd;
  char *uri, *path;
  struct sockaddr_in sin;
  static char buffer[4096];
  ssize_t len;

  if ( argc != 2 )
  {
    fprintf( stderr,
             "Usage: %s <url>\n",
             basename( argv[0] ) );

    return EXIT_FAILURE;
  }

  // Separa a URI e o Path da URL.
  if ( ! getURIandPath( argv[1], &uri, &path ) )
  {
    fputs( "ERROR: Invalid URL.\n", stderr );
    return EXIT_FAILURE;
  }

  #ifndef NDEBUG
    fprintf( stderr,
            "DEBUG: Got URI : \"%s\"\n"
            "           Path: \"%s\"\n",
            uri, path );
  #endif

  // Cria o socket e conecta ao host dado pela URI.
  // Obtém o socket e os dados de endereçamento do host.
  fd = opensocket( uri, &sin );
  if ( fd < 0 )
  {
  error:
    free( uri );
    free( path );
    return EXIT_FAILURE;
  }

  #ifndef NDEBUG
    in_addr_t addr;

    addr = sin.sin_addr.s_addr;
    fprintf( stderr,
             "Connected to: %hhu.%hhu.%hhu.%hhu\n",
             addr, addr >> 8, addr >> 16, addr >> 24 );
  #endif

  // Envia uma requisição simples.
  if ( ! sendRequest( fd, uri, path ) )
  {
    close( fd );
    goto error;
  }

  // Mostra resposta do host.
  // FIXME: Esse não é o melhor jeito de fazer isso...
  puts("\nResponse:\n");
  do
  {
    len = recv( fd, buffer, sizeof buffer - 1, 0 );
    buffer[len] = 0;
    fputs( buffer, stdout );
  } while ( len == sizeof buffer - 1 );

  // Fecha o socket, libera recursos e sai.
  close( fd );
  free( uri );
  free( path );

  return EXIT_SUCCESS;
}

#define MATCH_LENGTH(a) ((a).rm_eo - (a).rm_so)

int getURIandPath( char *url, char **uri, char **path )
{
  // Usando Regex para separar a URI e Path, no formato:
  //    http://URL/path
  static const char regex[] =
    "^http://([^/]+)(.*)$";

  regex_t re;
  regmatch_t rem[3];
  int len;

  if ( regcomp( &re, regex, REG_EXTENDED ) )
    return 0;

  if ( regexec( &re, url, 3, rem, 0 ) )
  {
    regfree( &re );
    return 0;
  }
  
  // Pega a URI
  len = MATCH_LENGTH( rem[1] );
  *uri = malloc( len + 1 );
  memcpy( *uri, url + rem[1].rm_so, len );
  (*uri)[len] = 0;

  // Pega o Path (se tiver).
  len = MATCH_LENGTH( rem[2] );
  if ( ! len )
  {
    *path = strdup( "/" );
    regfree( &re );
    return 1;
  }
  
  *path = malloc( len + 1 );
  memcpy( *path, url + rem[2].rm_so, len );
  (*path)[len] = 0;

  regfree( &re );

  return 1;  
}

int opensocket( char *hostname, struct sockaddr_in *sin )
{
  int fd, err;
  struct addrinfo hints = { .ai_family = AF_INET };
  struct addrinfo *res;

  // Pega o IP do hostname.
  res = NULL;
  err = getaddrinfo( hostname, NULL, &hints, &res );
  if ( err )
  {
    if ( res ) freeaddrinfo( res );
    fprintf( stderr, "ERROR resolvind name: %s.\n",
                     gai_strerror( err ) );
    return -1;
  }
  *sin = *((struct sockaddr_in *)res->ai_addr);
  freeaddrinfo( res );

  // HTTP protocol
  sin->sin_port = htons(80);

  // Tenta criar o socket.
  fd = socket( AF_INET, SOCK_STREAM, IPPROTO_TCP );
  if ( fd < 0 )
  {
    fputs( "ERROR: Cannot create socket.\n", stderr );
    return -1;
  }

  // Tenta conectar ao host.
  if ( connect( fd, (struct sockaddr *)sin, sizeof *sin ) )
  {
    close( fd );
    fprintf( stderr, "ERROR: Cannot connect to \"%s\".", 
             hostname );
    return -1;
  }

  return fd;
}

static int sendRequest( int fd, char *uri, char *path )
{
  char *s;

  if ( asprintf( &s, "GET %s HTTP/1.1\r\n"
                     "Host: %s\r\n"
                     "\r\n",
                 path, uri ) < 0 )
    return 0;

  if ( send( fd, s, strlen( s ), 0 ) < 0 )
  {
    free( s );
    return 0;
  }

  free( s );
  return 1;
}

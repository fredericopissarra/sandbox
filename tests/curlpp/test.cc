#include <fstream>

#include <curlpp/cURLpp.hpp>
#include <curlpp/Easy.hpp>
#include <curlpp/Options.hpp>

int main()
{
  // Handle do cURLpp.
  curlpp::Easy request;

  // O stream que vai receber a resposta.
  std::ofstream ofs( "file.html" );

  // Ajusta a URL (note que https pode!).
  request.setOpt( 
    curlpp::Options::Url( "https://www.wikipedia.org" ) 
  );

  // Diz para escrever a resposta no ofstream 'ofs'.
  request.setOpt( curlpp::Options::WriteStream( &ofs ) );

  // Faz a requisição.
  request.perform();
}

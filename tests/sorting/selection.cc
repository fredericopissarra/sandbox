#include <common.h>

/* A "seleção" é o primeiro elemento do array, que será
   comparado com todos os demais, havendo a troca apenas se
   o elemento selecionado for "maior que" qualquer um dos
   demais elementos. */
void selectionSort( int *buffp, unsigned int size )
{
  int *lastp, *p, *q;

  /* Precisamos saber quando parar, postanto, calculamos o
     ponteiro do último elemento válido do array. */
  lastp = buffp + size - 1;

  /* Loop externo. Começamos no primeiro e vamos até o
     penúltimo elemento do array. */
  p = buffp;
  while ( p < lastp )
  {
    /* Loop interno, vamos do próximo elemento do array,
       depois do selectionado, até o último. */
    q = p + 1;
    while ( q <= lastp )
    {
      // Comparamos o elemento apontado por p, o elemento
      // selectionado, com o apontado por q, um dos outros
      // elementos do array. Trocamos um pelo outro se
      // o elemento selecionado for maior que o outro.
      if ( *p > *q )
        swap( p, q );

      q++;
    }

    p++;
  }
}

#ifndef COMMON_H_INCLUDED_
#define COMMON_H_INCLUDED_

static inline void swap( int *a, int *b ) { int t; t = *a; *a = *b; *b = t; }

#endif

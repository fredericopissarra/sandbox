#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include <string.h>
#include <time.h>

#include <algorithm>

#include <cycle_counting.h>

// 128 Ki elements.
#define ELEMENTS 131072

extern void bubbleSort( int *, unsigned int );
extern void selectionSort( int *, unsigned int );

static int cmp_( const void *a, const void *b )
{
  const int *p = (const int *)a;
  const int *q = (const int *)b;

  return ( *p > *q ) - ( *p < *q );
}

int main( void )
{
  counter_T c1, c2, c3, c4;
  int *buffp,
      *buff2, *buff3, *buff4,
      *p;
  unsigned int n;
  time_t t;
  struct tm *tm;
  char str[24];

  time( &t );
  tm = localtime( &t );
  strftime( str, sizeof str, "%d/%b/%Y %H:%M:%S", tm );
  printf( "Start: %s\n", str );

  srand( t );

  printf( "Filling buffer with %u random integers\n", ELEMENTS );

  // Buffer para os 3 testes...
  buffp = (int *) malloc( 4 * ELEMENTS * sizeof *buffp );
  if ( ! buffp )
  {
    fputs( "\033[1;31mERROR\033[m: Allocating memory.\n", stderr );
    return EXIT_FAILURE;
  }

  p = buffp;
  for ( n = 0; n < ELEMENTS; n++ )
    *p++ = rand();

  puts( "Copying elements to 3 more buffers." );
  buff2 = buffp + ELEMENTS;
  buff3 = buff2 + ELEMENTS;
  buff4 = buff3 + ELEMENTS;

  memcpy( buff2, buffp, ELEMENTS * sizeof *buffp );
  memcpy( buff3, buffp, ELEMENTS * sizeof *buffp );
  memcpy( buff4, buffp, ELEMENTS * sizeof *buffp );

  printf( "Sorting %u random integer elements... wait...", n );
  fflush( stdout );

  c1 = BEGIN_TSC();
  selectionSort( buffp, n );
  c1 = END_TSC( c1 );

  c2 = BEGIN_TSC();
  bubbleSort( buff2, n );
  c2 = END_TSC( c2 );

  c3 = BEGIN_TSC();
  qsort( buff3, n, sizeof *buff3, cmp_ );
  c3 = END_TSC( c3 );

  c4 = BEGIN_TSC();
  std::sort( buff4, buff4 + ELEMENTS );
  c4 = END_TSC( c4 );

  free( buffp );

  printf( "\n\n"
          "Selection sort: %16" PRIu64 " cycles (%.1f cycles/element).\n"
          "Bubble sort   : %16" PRIu64 " cycles (%.1f cycles/element).\n"
          "C qsort       : %16" PRIu64 " cycles (%.1f cycles/element).\n"
          "C++ std::sort : %16" PRIu64 " cycles (%.1f cycles/element).\n\n",
          c1, c1 / (double)n,
          c2, c2 / (double)n,
          c3, c3 / (double)n,
          c4, c4 / (double)n );

  time( &t );
  tm = localtime( &t );
  strftime( str, sizeof str, "%d/%b/%Y %H:%M:%S", tm );
  printf( "End: %s\n", str );

  return EXIT_SUCCESS;
}


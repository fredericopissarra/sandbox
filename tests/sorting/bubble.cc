#include <common.h>

void bubbleSort( int *buffp, unsigned int size )
{
  int *lastp, *p;

  // Guarda o último elemento do array.
  lastp = buffp + size - 1;

  // Vai decrementnado o ponteiro do último elemento,
  // porque vamos levando o maior deles para o final.
  while ( lastp > buffp )
  {
    // Compara em pares, realizando as trocas, se
    // necessário, até os dois últimos elementos.
    p = buffp;
    while ( p < lastp )
    {
      if ( *p > *(p + 1) )
        swap( p, p + 1 );

      p++;
    }

    lastp--;
  }
}

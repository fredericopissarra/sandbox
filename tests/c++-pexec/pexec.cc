#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <cstdio>
#include <cstdarg>
#include <string>
#include <vector>
#include <new>

#include <iostream>   // para testes.

// Mesma rotina com mais controle!
std::string pexec( const std::string& cmd, const std::vector<std::string>& args )
{
  pid_t pid;
  int p[2];

  // Tenta criar um pipe.
  if ( pipe( p ) )
    return "";

  // Tenta forkar o processo.
  pid = fork();

  // Se não conseguiu...
  if ( pid == -1 )
  {
    close( p[0] );
    close( p[1] );
    return "";
  }

  // Se estamos no processo filho...
  if ( ! pid )
  {
    close( p[0] );  // fecha o canal de leitura.

    // re-assinala os descritores stdout e stderr;
    dup2( p[1], STDOUT_FILENO );
    dup2( p[1], STDERR_FILENO );

    char **argsptr;

    // aloca espaço para os argumentos (argv).
    argsptr = new(std::nothrow) char *[args.size() + 2];
    if ( ! argsptr )
    {
      close( p[1] );
      exit( EXIT_FAILURE );
    }

    // Cria o array argv.
    int i = 1;
    argsptr[0] = ( char * )cmd.c_str();
    for ( auto& j : args )
      argsptr[i++] = ( char * )j.c_str();
    argsptr[i] = nullptr;

    // exec usando PATH.
    execvp( cmd.c_str(), argsptr );

    // Só chega aqui se execvp() falhou.

    delete [] argsptr;
    close( p[1] );
    exit( EXIT_FAILURE );
  }

  // Se chegou até aqui, é o processo pai.
  close( p[1] );  // fecha o canal de escrita.

  // Espera pelo término do processo filho.
  int status;

  waitpid( pid, &status, 0 ); 

  if ( ! WIFEXITED( status ) )
  {
  error:
    close( p[0] );
    return "";
  }

  // cria stream com base no descritor de entrada da pipe.
  FILE *f = fdopen( p[0], "r" );

  if ( ! f )
    goto error;

  // Lê uma linha de cada vez e assinala para result.
  char *buffer = nullptr;
  size_t size = 0;
  std::string result;

  // Não precisamos nos livrar dos '\n' aqui. Quero obter a string
  // de saída do procsso filho EXATAMENTE como ela é.
  while ( getline( &buffer, &size, f ) > 0 )
    result = result + buffer;

  // Libera o buffer.
  free( buffer );

  // Fecha o stream (e o descritor)
  fclose( f );

  return result;
}

// Exemplo de chamada.
int main( void )
{
  // Executa um simples 'ls -l'.
  std::string s = pexec( "ls", std::vector<std::string>{ "-l" } );

  std::cout << "Output:\n\n" << s << '\n';
}

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <inttypes.h>
#include <fcntl.h>

#include "../../src/cycle_counting.h"

#define NOINLINE __attribute__((noinline))

#define BUFFER_SIZE 256

NOINLINE static void readRandomBytes ( char *, char * );
static void showbuffer ( char * );

int main ( void )
{
  static char buffer[BUFFER_SIZE];
  uint64_t c;

  // NOTE: /dev/random uses entropy, but blocks!
  c = BEGIN_TSC();
  readRandomBytes ( "/dev/urandom", buffer );
  c = END_TSC ( c );

  puts ( "\nFrom /dev/urandom:" );
  showbuffer ( buffer );

  printf ( "/dev/urandom -> %" PRIu64 " cycles\n",
           c );

  return EXIT_SUCCESS;
}

void readRandomBytes ( char *dev, char *buffp )
{
  int fd;
  ssize_t size = 0;
  char *q = buffp;
  char *e = buffp + BUFFER_SIZE;

  if ( ( fd = open ( dev, O_RDWR ) ) == -1 )
  {
    perror ( "open" );
    exit ( EXIT_FAILURE );
  }

  if ( ( size = read( fd, buffp, e - buffp ) ) == -1)
  {
error:
    perror( "read" );
    exit( EXIT_FAILURE );
  }
  q = buffp + size;
  
  while ( q < e )
  {
    if ( ( size = read( fd, q, e - q ) ) == -1 )
      goto error;

    q += size;
  }
    
  close ( fd );
}

void showbuffer ( char *buffp )
{
  int i;

  i = 0;

  while ( i < BUFFER_SIZE )
  {
    if ( ! ( i % 16 ) ) printf ( "\n%08x:", i );

    printf ( " %02hhx", *buffp++ );
    i++;
  }

  putchar ( '\n' );
}

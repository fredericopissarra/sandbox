#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <pthread.h>

// Aceitaremos até 32 conexões simultâneas...
#define BACKLOG_LEN 32

struct connstruc_s {
  pthread_t tid;
  int sockfd;
  struct sockaddr saddr;
  socklen_t saddrlen;
};

// Protótipo da thread (desatachada) que trata as
// requisições e envia as respostas.
static void *thread_worker( void * );

int main( void )
{
  int bndsockfd;
  int connsockfd;
  int terminated = 0;
  struct sockaddr_in bndaddr = { .sin_family = AF_INET, .sin_port = htons(80) };  // Bind with IPv4 (ANY device), port 80.
  struct sockaddr connaddr; // IPv6 or IPv4 accepted connections.
  socklen_t connaddrlen;

  if ( getuid() )
  {
    fputs( "ERROR: Need root privilege to run this.\n", stderr );
    return EXIT_FAILURE;
  }

  if ( ( bndsockfd = socket( AF_INET, SOCK_STREAM, IPPROTO_TCP ) ) < 0 )
  {
    perror( "socket" );
    return EXIT_FAILURE;
  }

#ifdef REUSEADDR
  {
    int n = 1;

    setsockopt( bndsockfd, SOL_SOCKET, SO_REUSEADDR, &n, sizeof n );
  }
#endif

  if ( bind( bndsockfd, (struct sockaddr *)&bndaddr, sizeof bndaddr ) )
  {
    perror( "bind" );
  error:
    close( bndsockfd );
    return EXIT_FAILURE;
  }

  if ( listen( bndsockfd, BACKLOG_LEN ) )
  {
    perror( "listen" );
    goto error;
  }

  // Loop para aceitar requisições.
  do 
  {
    if ( ( connsockfd = accept( bndsockfd, &connaddr, &connaddrlen ) ) > 0 )
    {
      pthread_attr_t attr;
      struct connstruc_s *connstruc;

      // Aloca espaço para thread/connection info.
      if ( ! ( connstruc = malloc( sizeof *connstruc ) ) )
      {
        close( connsockfd );
        fputs( "ERROR allocating space for thread/connection structure.\n", stderr );
        continue;
      }

      connstruc->sockfd = connsockfd;
      connstruc->saddr = connaddr;
      connstruc->saddrlen = connaddrlen;

      pthread_attr_init( &attr );
      pthread_attr_setdetachstate( &attr, PTHREAD_CREATE_DETACHED );
      pthread_create( &connstruc->tid, NULL, thread_worker, connstruc );
      pthread_attr_destroy( &attr );
    }
  } while ( ! terminated );
  // FIXME: Mais tarde colocar código de término para esse loop...

  return EXIT_SUCCESS;
}

// Thread que envia resposta (ignorando a requisição).
static void *thread_worker( void *arg )
{
  static const char payload[] = "<html><body><h1>It Works!</h1></body></html>"; 
  struct connstruc_s *connstruc = arg;
  FILE *fsock;

  if ( fsock = fdopen( connstruc->sockfd, "r+" ) )
  {
    // OBS: A especificação do HTTP/1.1 NÃO requer que o payload
    // siga a terminação de linha com '\r\n'!
    // Aliás, o payload pode ser binário, depende do Content-Type!
    fprintf( fsock, "HTTP/1.1 200 OK\r\n"
                    "Content-Type: text/html\r\n"
                    "Content-Length: %zu\r\n"
                    "\r\n"
                    "%s",
             sizeof payload,
             payload );

    // Não é realmente necessário, mas coloco por garantia.
    fflush( fsock );
  }
  else
    perror( "fdopen" );

  // É preciso avisar ao host remoto que não vamos mais aceitar nada...
  shutdown( connstruc->sockfd, SHUT_RDWR );
  fclose( fsock );

  free( connstruc );

  return NULL;
}

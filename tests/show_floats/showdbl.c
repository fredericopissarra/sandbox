#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <libgen.h>
#include <math.h>

union dbl_u
{
  double d;
  unsigned long long int u;

  struct
  {
    // FIXED: Todos os campos precisam ser do mesmo tipo.
    unsigned long long int f:52;
    unsigned long long int e:11;
    unsigned long long int s:1;
  } __attribute__ ( ( packed ) );
};

int main ( int argc, char *argv[] )
{
  union dbl_u v;

  if ( ! argv[1] )
  {
    fprintf ( stderr, "Usage: %s <value>\n\n"
              "Shows the structure of a double based on <value>.\n",
              basename ( argv[0] ) );

    return EXIT_FAILURE;
  }

  errno = 0;
  v.d = strtod ( argv[1], NULL );

  if ( errno )
  {
    fputs ( "ERROR converting value to double.\n", stderr );
    return EXIT_FAILURE;
  }

  printf ( "%s -> S=%u, F=%llu, e=%lld (E=%llu): %.160g (0x%016llx)\n",
           argv[1], v.s, (unsigned long long int)v.f, (long long int) v.e - 1023 + ( v.e == 0 ), (unsigned long long int)v.e, v.d, v.u );

  if ( ! isfinite( v.d ) )
  {
    puts( "NAN" );
    return EXIT_SUCCESS;
  }

  // 4503599627370496 = 2⁵²

  // NOTE: $1 extension to printf not available to MinGW32.

  printf( "\\Latex:\n%s\\approx\\left(-1\\right)^%u\\cdot",
    argv[1], v.s );

  if ( isnormal( v.d ) )
    printf ( "\\left(1 + \\frac{%llu}{4503599627370496}\\right)"
             "\\cdot 2^{%lld}=%.160g\n",
             (unsigned long long int )v.f, ( long long int ) v.e - 1023, v.d );
  else
    printf( "\\frac{%llu}{4503599627370496}\\cdot 2^{-1022}=%.160g\n",
            (unsigned long long)v.f, v.d );

  return EXIT_SUCCESS;
}

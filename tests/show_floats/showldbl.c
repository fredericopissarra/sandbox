#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <libgen.h>
#include <math.h>

union ldbl_u
{
  long double d;

  unsigned __int128 u;

  struct
  {
    // FIXED: Todos os campos precisam ser do mesmo tipo.
    unsigned long long int f;
    unsigned long long int e:15;
    unsigned long long int s:1;
  } __attribute__ ( ( packed ) );
};

int main ( int argc, char *argv[] )
{
  union ldbl_u v;

  if ( ! argv[1] )
  {
    fprintf ( stderr, "Usage: %s <value>\n\n"
              "Shows the structure of a long double based on <value>.\n",
              basename ( argv[0] ) );

    return EXIT_FAILURE;
  }

  errno = 0;
  v.d = strtold ( argv[1], NULL );

  if ( errno )
  {
    fputs ( "ERROR converting value to double.\n", stderr );
    return EXIT_FAILURE;
  }

  printf ( "%s -> S=%u, F=%llu, e=%lld (E=%llu): %.160Lg (0x%04hx%016llx)\n",
           argv[1], 
           v.s, 
           (unsigned long long int)v.f & ~(1ULL << 63), 
           (long long int) v.e - 16383 + ( v.e == 0 ), 
           (unsigned long long int)v.e, 
           v.d, 
           (unsigned short int)( v.u >> 64 ) & 0xffff,
           (unsigned long long int)( v.u & 0xffffffffffffffffULL ) );

  if ( ! isfinite( v.d ) )
  {
    puts( "NAN" );
    return EXIT_SUCCESS;
  }

  // 4503599627370496 = 2⁵²

  // NOTE: $1 extension to printf not available to MinGW32.

  printf( "\\Latex:\n%s\\approx\\left(-1\\right)^%u\\cdot",
    argv[1], v.s );

  if ( isnormal( v.d ) )
    printf ( "\\left(1 + \\frac{%llu}{9223372036854775808}\\right)"
             "\\cdot 2^{%lld}=%.160Lg\n",
             (unsigned long long int )v.f & ~( 1ULL << 63 ), ( long long int ) v.e - 16383, v.d );
  else
    printf( "\\frac{%llu}{9223372036854775808}\\cdot 2^{-1022}=%.160g\n",
            (unsigned long long)v.f, v.d );

  return EXIT_SUCCESS;
}

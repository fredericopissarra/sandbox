#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <libgen.h>
#include <math.h>
#include <float.h>

union flt_u
{
  float d;
  unsigned int u;

  struct
  {
    unsigned int f:23;
    unsigned int e:8;
    unsigned int s:1;
  } __attribute__ ( ( packed ) );
};

int main ( int argc, char *argv[] )
{
  union flt_u v;
  double d;

  if ( ! argv[1] )
  {
    fprintf ( stderr, "Usage: %s <value>\n\n"
              "Shows the structure of a double based on <value>.\n",
              basename ( argv[0] ) );

    return EXIT_FAILURE;
  }

  errno = 0;
  d = strtod ( argv[1], NULL ); // yep... double...

  if ( errno || fabs( d ) > FLT_MAX )
  {
    fputs ( "ERROR converting value to double.\n", stderr );
    return EXIT_FAILURE;
  }

  v.d = d;

  printf ( "%s -> S=%u, F=%u, e=%d (E=%d): %.160g (0x%08x)\n",
           argv[1], v.s, v.f, ( int ) v.e - 127 + ( v.e == 0 ), ( int ) v.e, v.d, v.u );

  // 8388608 = 2²³
  printf( "\nLaTeX:\n%s\\approx\\left(-1\\right)^%u\\cdot", argv[1], v.s );

  // NOTE: $1 extension to printf not available on MinGW.
  if ( isnormal( v.d ) )
    printf( "\\left(1 + \\frac{%u}{8388608}\\right)\\cdot 2^{%d}=%.160g\n",
      (unsigned int)v.f, (int)v.e - 127, v.d );
  else
    printf( "\\frac{%u}{8388608}\\cdot 2^{-126}=%.160g\n",
      (unsigned int)v.f, v.d );

  return EXIT_SUCCESS;
}

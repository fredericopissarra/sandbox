// memusage.c
//
//  $ cc -O2 -o memusage memusage.c
//
#define _GNU_SOURCE
#include <unistd.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/resource.h>

/*
  From 'man 5 proc':

  /proc/[pid]/statm
    Provides information about memory usage, measured in pages.  The columns are:

      size       (1) total program size
                 (same as VmSize in /proc/[pid]/status)
      resident   (2) resident set size
                 (same as VmRSS in /proc/[pid]/status)
      share      (3) shared pages (i.e., backed by a file)
      text       (4) text (code)
      lib        (5) library (unused in Linux 2.6)
      data       (6) data + stack
      dt         (7) dirty pages (unused in Linux 2.6)

 */
struct memusage_s
{
  uint64_t size;
  uint64_t resident;
  uint64_t shared;
  uint64_t text;
  uint64_t data;
};

static int get_memusage ( pid_t, struct memusage_s * );

int main ( void )
{
  struct memusage_s mu;
  struct rusage ru;

  if ( get_memusage ( getpid(), &mu ) )
  {
    fputs ( "ERROR getting memory usage.\n", stderr );
    return EXIT_FAILURE;
  }

  printf ( "From procfs (used):\n"
           "\tProcess Size: %"PRIu64" KiB\n"
           "\tResident Set: %"PRIu64" KiB\n"
           "\tShared Pages: %"PRIu64"\n"
           "\tText Section Size: %"PRIu64" KiB\n"
           "\tData + Stack Size: %"PRIu64" KiB\n",
           mu.size, mu.resident, mu.shared, mu.text, mu.data );

  if ( getrusage ( RUSAGE_SELF, &ru ) == -1 )
  {
    fputs ( "ERROR getting resource usage.\n", stderr );
    return EXIT_FAILURE;
  }

  printf ( "\nFrom getrusage():\n"
           "\tMaximum Resident Set: %lu KiB\n"
           "\tIntegral shared text size: %lu KiB\n"
           "\tIntegral unshared data size: %lu KiB\n"
           "\tIntegral unshared stack size: %lu KiB\n"
           "\tPage reclaims: %lu\n"
           "\tPage faults: %lu\n"
           "\tSwaps: %lu\n",
           ru.ru_maxrss,
           ru.ru_ixrss,
           ru.ru_idrss,
           ru.ru_isrss,
           ru.ru_minflt,
           ru.ru_majflt,
           ru.ru_nswap );

  return EXIT_SUCCESS;
}

// NOTE: Unfortunately some fields from rusage structure aren't implemented.
//       So we have to get them from procfs.
static int get_memusage ( pid_t pid, struct memusage_s *memp )
{
  FILE *f;
  char fname[24]; // 24 bytes is sufficient.

  snprintf ( fname, sizeof fname, "/proc/%d/statm", pid );

  if ( ! ( f = fopen ( fname, "r" ) ) )
    return -1;

  if ( fscanf ( f, "%"PRIu64" %"PRIu64" %"PRIu64 " %"PRIu64" %"PRIu64,
                &memp->size,
                &memp->resident,
                &memp->shared,
                &memp->text,
                &memp->data ) != 5 )
  {
    fclose ( f );
    return -1;
  }

  fclose ( f );
  return 0;
}

// posix_timer.c
//
//   linkar com librt.
//
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <signal.h>

static void notify( union sigval );

int main( void )
{
  timer_t tid;

  // Notificação por thread.
  struct sigevent sev = { .sigev_notify = SIGEV_THREAD,
                          .sigev_notify_function = notify };

  
  // Notar que o outro campo de `timespec` é em nanossegundos!
  struct itimerspec its = { .it_interval.tv_sec = 1,  // Intervalo de 1 em 1 segundo.
                            .it_value.tv_sec = 1 };   // arma o timer (0=disarma, !=0, arma [tempo do disparo inicial]).

  if ( timer_create( CLOCK_REALTIME, &sev, &tid ) )
  {
    fputs( "Error registering timer.\n", stderr );
    return EXIT_FAILURE;
  }

  printf( "Timer %d criado...\n", tid );

  if ( timer_settime( tid, 0, &its, NULL ) )
  {
    fprintf( stderr, "Error setting timer %d.\n", tid );
    return EXIT_FAILURE;
  }

  sleep(10);  // Põe essa thead para dormir por 10 segundos.

  its.it_value = (struct timespec){ 0 };  // Disarma timer.
  timer_settime( tid, 0, &its, NULL );  

  timer_delete( tid );

  return EXIT_SUCCESS;
}

static void notify( union sigval sv )
{
  static int tictoc = 0;

  puts ( ! tictoc ? "tic" : "toc" );
  tictoc = ~tictoc;
}


// alsa-test.c
//
//   cc -O2 -o alsa-test alsa-test.c -lasound -lm
//
// Natural A (440 Hz), 10 seconds output, sinusoidal, ALSA.

#include <stdio.h>
#include <stdlib.h>
#include <alsa/asoundlib.h>
#include <alsa/pcm.h>
#include <math.h>

#define BUFFER_LEN 48000
#define FS 48000  /* sampling frequency */
#define F  440    /* Output frequency */

static void close_handle_ ( snd_pcm_t **handle )
{
  if ( *handle )
    snd_pcm_close ( *handle );
}

static void free_buffer_ ( float **p )
{
  free ( *p );
}

int main ( void )
{
  float *buffer __attribute__ ( ( cleanup ( free_buffer_ ) ) ) = NULL;;
  int err;

  snd_pcm_t *handle __attribute__ ( ( cleanup ( close_handle_ ) ) ) = NULL;

  // ERROR HANDLING
  if ( ( err = snd_pcm_open ( &handle, "default", SND_PCM_STREAM_PLAYBACK, 0 ) ) < 0 )
  {
    fprintf ( stderr, "Playback open error: %s\n", snd_strerror ( err ) );

    return EXIT_FAILURE;
  }

  // Setup device parameters
  if ( ( err = snd_pcm_set_params ( handle,
                                    SND_PCM_FORMAT_FLOAT,
                                    SND_PCM_ACCESS_RW_INTERLEAVED,
                                    1,      // 1 channel
                                    FS,     // 48 kHz rate.
                                    1,      // allow resampling
                                    500000 ) ) < 0 )  // 1/2 secons latency?
  {
    fprintf ( stderr, "Playback open error: %s\n", snd_strerror ( err ) );
    return EXIT_FAILURE;
  }

  // SINE WAVE
  printf ( "Sine tone at %d Hz ", F );

  buffer = malloc ( BUFFER_LEN * sizeof * buffer );

  if ( ! buffer )
  {
    fputs ( "ERROR allocating output buffer.\n", stderr );
    return EXIT_FAILURE;
  }

  // Sine wave...
  //                          2*π*f
  //   buffer[k] = sin ( k * -------)
  //                           fs
  // Amplitude: +/-1.
  //
  for ( int i = 0; i < BUFFER_LEN; i++ )
    buffer[i] = sin ( i * 2.0 * M_PI * F / FS );

  // Output @ 48 kHz...
  for ( int i = 0; i < 10 * 48000 / BUFFER_LEN; i++ ) // 10 seconds
  {
    snd_pcm_sframes_t frames;

    frames = snd_pcm_writei ( handle, buffer, BUFFER_LEN );

    // If error occur, try to recover.
    if ( frames < 0 )
      frames = snd_pcm_recover ( handle, frames, 0 );

    // if error still there...
    if ( frames < 0 )
    {
      fprintf ( stderr, "snd_pcm_writei failed: %s\n", snd_strerror ( frames ) );
      return EXIT_FAILURE;
    }

    // Not enough bytes writen, info.
    if ( frames > 0 && frames < BUFFER_LEN )
      fprintf ( stderr, "Short write (expected %d, wrote %li)\n", BUFFER_LEN, frames );
  }

  return EXIT_SUCCESS;
}

#ifndef _WEBSERVER_
#define _WEBSERVER_

#include <microhttpd.h>
#include <iostream>
#include <string.h>
#include <vector>

#include "Controller.hpp"

class WebServer {
private:
  unsigned short port;
  struct MHD_Daemon *daemon;

  /** List of controllers this server has. */
  std::vector<Controller *> controllers;

  static int request_handler ( 
                 void *cls, struct MHD_Connection *connection,
                 const char *url, const char *method, const char *version,
                 const char *upload_data, size_t *upload_data_size, void **ptr 
              );
public:
  WebServer ( unsigned short p ) : port(p), daemon(nullptr) {}
  ~WebServer() { stop(); }

  void addController ( Controller *controller )
  { controllers.push_back ( controller ); }

  bool start();
  void stop();
};
#endif

#ifndef _CONTROLLER_
#define _CONTROLLER_

#include <string>
#include <sstream>
#include <microhttpd.h>

/**
 * Base controller for handling http requests.
 */
struct Controller {
  /**
   * Check if given path and method are handled by this controller.
   */
  virtual bool validPath ( const char *path, const char *method ) = 0;

  /**
   * User defined http response.
   */
  virtual void createResponse ( struct MHD_Connection *connection,
                                const char *url, const char *method, 
                                const char *upload_data, size_t *upload_data_size, 
                                std::stringstream &response ) = 0;

  /**
   * Handles given request.
   */
  virtual int handleRequest ( 
              struct MHD_Connection *connection,
              const char *url, const char *method,
              const char *upload_data, size_t *upload_data_size 
          ) = 0;

};

/**
 * The dynamic controller is a controller for creating user defined pages.
 */
struct DynamicController: public Controller {
  int handleRequest ( 
         struct MHD_Connection *connection,
         const char *url, const char *method,
         const char *upload_data, size_t *upload_data_size );
};

#endif

#include <microhttpd.h>
#include "Controller.hpp"

int DynamicController::handleRequest ( struct MHD_Connection *connection,
                                       const char *url, 
                                       const char *method, 
                                       const char *upload_data,
                                       size_t *upload_data_size )
{
  std::stringstream response_string;

  createResponse ( connection, url, method, upload_data, upload_data_size, response_string );

  //Send response.
  struct MHD_Response *response = MHD_create_response_from_buffer ( 
                                    response_string.str().length(),
                                    ( void * ) response_string.str().c_str(), 
                                    MHD_RESPMEM_MUST_COPY 
                                  );

  int status = MHD_queue_response ( connection, MHD_HTTP_OK, response );

  MHD_destroy_response ( response );

  return status;
}


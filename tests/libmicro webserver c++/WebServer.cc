#include <unistd.h>
#include <cstddef>
#include <cstring>
#include "WebServer.hpp"
#include "Controller.hpp"

int WebServer::request_handler ( 
        void *cls, 
        struct MHD_Connection *connection,
        const char *url, const char *method, const char *version,
        const char *upload_data, size_t *upload_data_size, 
        void **ptr 
     )
{
  std::cerr << "Request: " << url << ", Method: " << method << std::endl;

  WebServer *server = static_cast<WebServer *>( cls );
  Controller *controller = nullptr;

  for ( size_t i = 0; i < server->controllers.size(); i++ )
  {
    Controller *c = server->controllers.at ( i );

    if ( c->validPath ( url, method ) )
    {
      controller = c;
      break;
    }
  }

  if ( ! controller )
  {
    std::cerr << "Path not found.\n";

    static const char notFound[] = "<html><body>Path not found</body></html>";

    struct MHD_Response *response = MHD_create_response_from_buffer ( strlen( notFound ),
                                                                      (void *)notFound, 
                                                                      MHD_RESPMEM_PERSISTENT );

    int status = MHD_queue_response ( connection, MHD_HTTP_NOT_FOUND, response );

    MHD_destroy_response( response );

    return status;
  }

  return controller->handleRequest ( connection, url, method, upload_data, upload_data_size );
}

bool WebServer::start()
{
  // FIXME: Change to MHD_INTERNAL_POLLING_THREAD | MHD_USE_POLL to test if it works.
  // FIXME: Add HTTPS support later.
  daemon = MHD_start_daemon ( MHD_USE_THREAD_PER_CONNECTION,
                              port, 
                              NULL, NULL,               /* Accept policy callback and struct */
                              request_handler, this,    /* Handler callback and struct */
                              MHD_OPTION_END );

  if ( daemon )
  {
    std::cerr << "WebServer started.\n";
    return true;
  }

  return false;
}

void WebServer::stop()
{
  if ( daemon )
  {
    MHD_stop_daemon( daemon );

    std::cerr << "WebServer stoped.\n";
  }

  daemon = nullptr;
}

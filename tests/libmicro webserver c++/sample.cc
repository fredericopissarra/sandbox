#include "WebServer.hpp"
#include <time.h>
#include <signal.h>
#include <termios.h>
//#include <fstream>
#include <string>

using namespace std::literals;

static bool terminated = false;

// Simple controler for root path.
class MyController: public DynamicController {
public:
  bool validPath ( const char *path, const char *method )
  { 
    if ( path && method )
      return ( std::string(path) == "/" ) && ( std::string(method) == "GET" );

    return false;
  }

  void createResponse ( struct MHD_Connection *connection,
                        const char *url, const char *method, const char *upload_data,
                        size_t *upload_data_size, std::stringstream& response );
};

void MyController::createResponse ( struct MHD_Connection *connection,
                                    const char *url, const char *method, const char *upload_data,
                                    size_t *upload_data_size, std::stringstream &response )
{
    char buff[16];
    time_t time_cur;
    struct tm *time_now;

    time ( &time_cur );
    time_now = localtime ( &time_cur );
    strftime( buff, sizeof buff, "%T", time_now );

    response << "<html>"
                  "<head>"
                    "<title>Hello World from cpp</title>"
                  "</head>"
                  "<body>"
                    "Hello World at " << std::string( buff ) << "!"
                  "</body>"
                "</html>";
}

static void sigint_hndlr( int );

//--- previsão para implementar HTTPS.
//static std::string readFile( const std::string& filename );
 
int main ( int argc, char **argv )
{
  struct termios tios, oldtios;

  MyController myPage;

  WebServer server ( 8080 );
  server.addController ( &myPage );

  signal( SIGINT, sigint_hndlr );

  // Disable terminal echo.
  tcgetattr( STDIN_FILENO, &oldtios );
  tios = oldtios;
  tios.c_lflag &= ~ECHO;
  tcsetattr( STDIN_FILENO, TCSANOW, &tios );

  server.start();

  while ( ! terminated )
    pause();    // Interrompido por SIGINT!

  // Return terminal to original state.
  tcsetattr( STDIN_FILENO, TCSANOW, &oldtios );
}

void sigint_hndlr( int sig ) { terminated = true; }

//--- previsão para implementar HTTPS.
//std::string readFile( const std::string& filename )
//{
//  ifstream ifs( filename );
//  std::string s;
//
//  if ( ! fs )
//    throw runtime_error( "Unable to open '"s + filename + "'" );
//
//  while ( std::string line; std::getline( ifs, line ) )
//    s += line;
//
//  return s; 
//}


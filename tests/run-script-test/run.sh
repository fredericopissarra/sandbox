#!/bin/bash

MYDIR=.
MYFILE=hello     # Simple executable file inside this 'archive'.
OUTFILE="${MYDIR}/${MYFILE}"

# Get the next line after ###EOF### where tail will get the binary data.
NEXTLINE=$((`grep -n --binary-files=text '^###EOF###' "$0" | cut -d: -f1` + 1))

# Extract (should test for error conditions).
tail -n +${NEXTLINE} "$0" > ${OUTFILE}

# Execute.
chmod u+x ${OUTFILE}
${OUTFILE}

# Necessary to stop the script here!
exit 0

# This MUST BE the last line in this script!
###EOF###

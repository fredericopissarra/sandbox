#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <inttypes.h>

#define WIDTH  1280
#define HEIGHT 720

// SPIR-V fragment shader (test.frag compiled with glslc).
static const unsigned char frag_shader_spv[] =
{
  0x03, 0x02, 0x23, 0x07, 0x00, 0x00, 0x01, 0x00, 0x07, 0x00, 0x08, 0x00, 0x13, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x11, 0x00, 0x02, 0x00,
  0x01, 0x00, 0x00, 0x00, 0x0b, 0x00, 0x06, 0x00, 0x01, 0x00, 0x00, 0x00, 0x47, 0x4c, 0x53, 0x4c, 0x2e, 0x73, 0x74, 0x64, 0x2e, 0x34, 0x35, 0x30,
  0x00, 0x00, 0x00, 0x00, 0x0e, 0x00, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x0f, 0x00, 0x07, 0x00, 0x04, 0x00, 0x00, 0x00,
  0x04, 0x00, 0x00, 0x00, 0x6d, 0x61, 0x69, 0x6e, 0x00, 0x00, 0x00, 0x00, 0x09, 0x00, 0x00, 0x00, 0x0c, 0x00, 0x00, 0x00, 0x10, 0x00, 0x03, 0x00,
  0x04, 0x00, 0x00, 0x00, 0x07, 0x00, 0x00, 0x00, 0x03, 0x00, 0x03, 0x00, 0x02, 0x00, 0x00, 0x00, 0xc2, 0x01, 0x00, 0x00, 0x05, 0x00, 0x04, 0x00,
  0x04, 0x00, 0x00, 0x00, 0x6d, 0x61, 0x69, 0x6e, 0x00, 0x00, 0x00, 0x00, 0x05, 0x00, 0x06, 0x00, 0x09, 0x00, 0x00, 0x00, 0x6f, 0x75, 0x74, 0x46,
  0x72, 0x61, 0x67, 0x43, 0x6f, 0x6c, 0x6f, 0x72, 0x00, 0x00, 0x00, 0x00, 0x05, 0x00, 0x04, 0x00, 0x0c, 0x00, 0x00, 0x00, 0x69, 0x6e, 0x43, 0x6f,
  0x6c, 0x6f, 0x72, 0x00, 0x47, 0x00, 0x04, 0x00, 0x09, 0x00, 0x00, 0x00, 0x1e, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x47, 0x00, 0x04, 0x00,
  0x0c, 0x00, 0x00, 0x00, 0x1e, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x13, 0x00, 0x02, 0x00, 0x02, 0x00, 0x00, 0x00, 0x21, 0x00, 0x03, 0x00,
  0x03, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x16, 0x00, 0x03, 0x00, 0x06, 0x00, 0x00, 0x00, 0x20, 0x00, 0x00, 0x00, 0x17, 0x00, 0x04, 0x00,
  0x07, 0x00, 0x00, 0x00, 0x06, 0x00, 0x00, 0x00, 0x04, 0x00, 0x00, 0x00, 0x20, 0x00, 0x04, 0x00, 0x08, 0x00, 0x00, 0x00, 0x03, 0x00, 0x00, 0x00,
  0x07, 0x00, 0x00, 0x00, 0x3b, 0x00, 0x04, 0x00, 0x08, 0x00, 0x00, 0x00, 0x09, 0x00, 0x00, 0x00, 0x03, 0x00, 0x00, 0x00, 0x17, 0x00, 0x04, 0x00,
  0x0a, 0x00, 0x00, 0x00, 0x06, 0x00, 0x00, 0x00, 0x03, 0x00, 0x00, 0x00, 0x20, 0x00, 0x04, 0x00, 0x0b, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00,
  0x0a, 0x00, 0x00, 0x00, 0x3b, 0x00, 0x04, 0x00, 0x0b, 0x00, 0x00, 0x00, 0x0c, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x2b, 0x00, 0x04, 0x00,
  0x06, 0x00, 0x00, 0x00, 0x0e, 0x00, 0x00, 0x00, 0x00, 0x00, 0x80, 0x3f, 0x36, 0x00, 0x05, 0x00, 0x02, 0x00, 0x00, 0x00, 0x04, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x03, 0x00, 0x00, 0x00, 0xf8, 0x00, 0x02, 0x00, 0x05, 0x00, 0x00, 0x00, 0x3d, 0x00, 0x04, 0x00, 0x0a, 0x00, 0x00, 0x00,
  0x0d, 0x00, 0x00, 0x00, 0x0c, 0x00, 0x00, 0x00, 0x51, 0x00, 0x05, 0x00, 0x06, 0x00, 0x00, 0x00, 0x0f, 0x00, 0x00, 0x00, 0x0d, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x51, 0x00, 0x05, 0x00, 0x06, 0x00, 0x00, 0x00, 0x10, 0x00, 0x00, 0x00, 0x0d, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00,
  0x51, 0x00, 0x05, 0x00, 0x06, 0x00, 0x00, 0x00, 0x11, 0x00, 0x00, 0x00, 0x0d, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x50, 0x00, 0x07, 0x00,
  0x07, 0x00, 0x00, 0x00, 0x12, 0x00, 0x00, 0x00, 0x0f, 0x00, 0x00, 0x00, 0x10, 0x00, 0x00, 0x00, 0x11, 0x00, 0x00, 0x00, 0x0e, 0x00, 0x00, 0x00,
  0x3e, 0x00, 0x03, 0x00, 0x09, 0x00, 0x00, 0x00, 0x12, 0x00, 0x00, 0x00, 0xfd, 0x00, 0x01, 0x00, 0x38, 0x00, 0x01, 0x00
};

#define frag_shader_spv_size ( sizeof frag_shader_spv )

// SPIR-V vertex shader (test.vert compiled with glslc)
static const unsigned char vert_shader_spv[] =
{
  0x03, 0x02, 0x23, 0x07, 0x00, 0x00, 0x01, 0x00, 0x07, 0x00, 0x08, 0x00, 0x2c, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x11, 0x00, 0x02, 0x00,
  0x01, 0x00, 0x00, 0x00, 0x0b, 0x00, 0x06, 0x00, 0x01, 0x00, 0x00, 0x00, 0x47, 0x4c, 0x53, 0x4c, 0x2e, 0x73, 0x74, 0x64, 0x2e, 0x34, 0x35, 0x30,
  0x00, 0x00, 0x00, 0x00, 0x0e, 0x00, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x0f, 0x00, 0x09, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x04, 0x00, 0x00, 0x00, 0x6d, 0x61, 0x69, 0x6e, 0x00, 0x00, 0x00, 0x00, 0x09, 0x00, 0x00, 0x00, 0x0b, 0x00, 0x00, 0x00, 0x10, 0x00, 0x00, 0x00,
  0x22, 0x00, 0x00, 0x00, 0x03, 0x00, 0x03, 0x00, 0x02, 0x00, 0x00, 0x00, 0xc2, 0x01, 0x00, 0x00, 0x05, 0x00, 0x04, 0x00, 0x04, 0x00, 0x00, 0x00,
  0x6d, 0x61, 0x69, 0x6e, 0x00, 0x00, 0x00, 0x00, 0x05, 0x00, 0x05, 0x00, 0x09, 0x00, 0x00, 0x00, 0x6f, 0x75, 0x74, 0x43, 0x6f, 0x6c, 0x6f, 0x72,
  0x00, 0x00, 0x00, 0x00, 0x05, 0x00, 0x04, 0x00, 0x0b, 0x00, 0x00, 0x00, 0x69, 0x6e, 0x43, 0x6f, 0x6c, 0x6f, 0x72, 0x00, 0x05, 0x00, 0x06, 0x00,
  0x0e, 0x00, 0x00, 0x00, 0x67, 0x6c, 0x5f, 0x50, 0x65, 0x72, 0x56, 0x65, 0x72, 0x74, 0x65, 0x78, 0x00, 0x00, 0x00, 0x00, 0x06, 0x00, 0x06, 0x00,
  0x0e, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x67, 0x6c, 0x5f, 0x50, 0x6f, 0x73, 0x69, 0x74, 0x69, 0x6f, 0x6e, 0x00, 0x05, 0x00, 0x03, 0x00,
  0x10, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x05, 0x00, 0x03, 0x00, 0x14, 0x00, 0x00, 0x00, 0x55, 0x42, 0x4f, 0x00, 0x06, 0x00, 0x08, 0x00,
  0x14, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x70, 0x72, 0x6f, 0x6a, 0x65, 0x63, 0x74, 0x69, 0x6f, 0x6e, 0x4d, 0x61, 0x74, 0x72, 0x69, 0x78,
  0x00, 0x00, 0x00, 0x00, 0x06, 0x00, 0x06, 0x00, 0x14, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x6d, 0x6f, 0x64, 0x65, 0x6c, 0x4d, 0x61, 0x74,
  0x72, 0x69, 0x78, 0x00, 0x06, 0x00, 0x06, 0x00, 0x14, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x76, 0x69, 0x65, 0x77, 0x4d, 0x61, 0x74, 0x72,
  0x69, 0x78, 0x00, 0x00, 0x05, 0x00, 0x03, 0x00, 0x16, 0x00, 0x00, 0x00, 0x75, 0x62, 0x6f, 0x00, 0x05, 0x00, 0x04, 0x00, 0x22, 0x00, 0x00, 0x00,
  0x69, 0x6e, 0x50, 0x6f, 0x73, 0x00, 0x00, 0x00, 0x47, 0x00, 0x04, 0x00, 0x09, 0x00, 0x00, 0x00, 0x1e, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x47, 0x00, 0x04, 0x00, 0x0b, 0x00, 0x00, 0x00, 0x1e, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x48, 0x00, 0x05, 0x00, 0x0e, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x0b, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x47, 0x00, 0x03, 0x00, 0x0e, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00,
  0x48, 0x00, 0x04, 0x00, 0x14, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x05, 0x00, 0x00, 0x00, 0x48, 0x00, 0x05, 0x00, 0x14, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x23, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x48, 0x00, 0x05, 0x00, 0x14, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x07, 0x00, 0x00, 0x00, 0x10, 0x00, 0x00, 0x00, 0x48, 0x00, 0x04, 0x00, 0x14, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x05, 0x00, 0x00, 0x00,
  0x48, 0x00, 0x05, 0x00, 0x14, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x23, 0x00, 0x00, 0x00, 0x40, 0x00, 0x00, 0x00, 0x48, 0x00, 0x05, 0x00,
  0x14, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x07, 0x00, 0x00, 0x00, 0x10, 0x00, 0x00, 0x00, 0x48, 0x00, 0x04, 0x00, 0x14, 0x00, 0x00, 0x00,
  0x02, 0x00, 0x00, 0x00, 0x05, 0x00, 0x00, 0x00, 0x48, 0x00, 0x05, 0x00, 0x14, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x23, 0x00, 0x00, 0x00,
  0x80, 0x00, 0x00, 0x00, 0x48, 0x00, 0x05, 0x00, 0x14, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x07, 0x00, 0x00, 0x00, 0x10, 0x00, 0x00, 0x00,
  0x47, 0x00, 0x03, 0x00, 0x14, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x47, 0x00, 0x04, 0x00, 0x16, 0x00, 0x00, 0x00, 0x22, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x47, 0x00, 0x04, 0x00, 0x16, 0x00, 0x00, 0x00, 0x21, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x47, 0x00, 0x04, 0x00,
  0x22, 0x00, 0x00, 0x00, 0x1e, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x13, 0x00, 0x02, 0x00, 0x02, 0x00, 0x00, 0x00, 0x21, 0x00, 0x03, 0x00,
  0x03, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x16, 0x00, 0x03, 0x00, 0x06, 0x00, 0x00, 0x00, 0x20, 0x00, 0x00, 0x00, 0x17, 0x00, 0x04, 0x00,
  0x07, 0x00, 0x00, 0x00, 0x06, 0x00, 0x00, 0x00, 0x03, 0x00, 0x00, 0x00, 0x20, 0x00, 0x04, 0x00, 0x08, 0x00, 0x00, 0x00, 0x03, 0x00, 0x00, 0x00,
  0x07, 0x00, 0x00, 0x00, 0x3b, 0x00, 0x04, 0x00, 0x08, 0x00, 0x00, 0x00, 0x09, 0x00, 0x00, 0x00, 0x03, 0x00, 0x00, 0x00, 0x20, 0x00, 0x04, 0x00,
  0x0a, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x07, 0x00, 0x00, 0x00, 0x3b, 0x00, 0x04, 0x00, 0x0a, 0x00, 0x00, 0x00, 0x0b, 0x00, 0x00, 0x00,
  0x01, 0x00, 0x00, 0x00, 0x17, 0x00, 0x04, 0x00, 0x0d, 0x00, 0x00, 0x00, 0x06, 0x00, 0x00, 0x00, 0x04, 0x00, 0x00, 0x00, 0x1e, 0x00, 0x03, 0x00,
  0x0e, 0x00, 0x00, 0x00, 0x0d, 0x00, 0x00, 0x00, 0x20, 0x00, 0x04, 0x00, 0x0f, 0x00, 0x00, 0x00, 0x03, 0x00, 0x00, 0x00, 0x0e, 0x00, 0x00, 0x00,
  0x3b, 0x00, 0x04, 0x00, 0x0f, 0x00, 0x00, 0x00, 0x10, 0x00, 0x00, 0x00, 0x03, 0x00, 0x00, 0x00, 0x15, 0x00, 0x04, 0x00, 0x11, 0x00, 0x00, 0x00,
  0x20, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x2b, 0x00, 0x04, 0x00, 0x11, 0x00, 0x00, 0x00, 0x12, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x18, 0x00, 0x04, 0x00, 0x13, 0x00, 0x00, 0x00, 0x0d, 0x00, 0x00, 0x00, 0x04, 0x00, 0x00, 0x00, 0x1e, 0x00, 0x05, 0x00, 0x14, 0x00, 0x00, 0x00,
  0x13, 0x00, 0x00, 0x00, 0x13, 0x00, 0x00, 0x00, 0x13, 0x00, 0x00, 0x00, 0x20, 0x00, 0x04, 0x00, 0x15, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00,
  0x14, 0x00, 0x00, 0x00, 0x3b, 0x00, 0x04, 0x00, 0x15, 0x00, 0x00, 0x00, 0x16, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x20, 0x00, 0x04, 0x00,
  0x17, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x13, 0x00, 0x00, 0x00, 0x2b, 0x00, 0x04, 0x00, 0x11, 0x00, 0x00, 0x00, 0x1a, 0x00, 0x00, 0x00,
  0x02, 0x00, 0x00, 0x00, 0x2b, 0x00, 0x04, 0x00, 0x11, 0x00, 0x00, 0x00, 0x1e, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x3b, 0x00, 0x04, 0x00,
  0x0a, 0x00, 0x00, 0x00, 0x22, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x2b, 0x00, 0x04, 0x00, 0x06, 0x00, 0x00, 0x00, 0x24, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x80, 0x3f, 0x20, 0x00, 0x04, 0x00, 0x2a, 0x00, 0x00, 0x00, 0x03, 0x00, 0x00, 0x00, 0x0d, 0x00, 0x00, 0x00, 0x36, 0x00, 0x05, 0x00,
  0x02, 0x00, 0x00, 0x00, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x03, 0x00, 0x00, 0x00, 0xf8, 0x00, 0x02, 0x00, 0x05, 0x00, 0x00, 0x00,
  0x3d, 0x00, 0x04, 0x00, 0x07, 0x00, 0x00, 0x00, 0x0c, 0x00, 0x00, 0x00, 0x0b, 0x00, 0x00, 0x00, 0x3e, 0x00, 0x03, 0x00, 0x09, 0x00, 0x00, 0x00,
  0x0c, 0x00, 0x00, 0x00, 0x41, 0x00, 0x05, 0x00, 0x17, 0x00, 0x00, 0x00, 0x18, 0x00, 0x00, 0x00, 0x16, 0x00, 0x00, 0x00, 0x12, 0x00, 0x00, 0x00,
  0x3d, 0x00, 0x04, 0x00, 0x13, 0x00, 0x00, 0x00, 0x19, 0x00, 0x00, 0x00, 0x18, 0x00, 0x00, 0x00, 0x41, 0x00, 0x05, 0x00, 0x17, 0x00, 0x00, 0x00,
  0x1b, 0x00, 0x00, 0x00, 0x16, 0x00, 0x00, 0x00, 0x1a, 0x00, 0x00, 0x00, 0x3d, 0x00, 0x04, 0x00, 0x13, 0x00, 0x00, 0x00, 0x1c, 0x00, 0x00, 0x00,
  0x1b, 0x00, 0x00, 0x00, 0x92, 0x00, 0x05, 0x00, 0x13, 0x00, 0x00, 0x00, 0x1d, 0x00, 0x00, 0x00, 0x19, 0x00, 0x00, 0x00, 0x1c, 0x00, 0x00, 0x00,
  0x41, 0x00, 0x05, 0x00, 0x17, 0x00, 0x00, 0x00, 0x1f, 0x00, 0x00, 0x00, 0x16, 0x00, 0x00, 0x00, 0x1e, 0x00, 0x00, 0x00, 0x3d, 0x00, 0x04, 0x00,
  0x13, 0x00, 0x00, 0x00, 0x20, 0x00, 0x00, 0x00, 0x1f, 0x00, 0x00, 0x00, 0x92, 0x00, 0x05, 0x00, 0x13, 0x00, 0x00, 0x00, 0x21, 0x00, 0x00, 0x00,
  0x1d, 0x00, 0x00, 0x00, 0x20, 0x00, 0x00, 0x00, 0x3d, 0x00, 0x04, 0x00, 0x07, 0x00, 0x00, 0x00, 0x23, 0x00, 0x00, 0x00, 0x22, 0x00, 0x00, 0x00,
  0x51, 0x00, 0x05, 0x00, 0x06, 0x00, 0x00, 0x00, 0x25, 0x00, 0x00, 0x00, 0x23, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x51, 0x00, 0x05, 0x00,
  0x06, 0x00, 0x00, 0x00, 0x26, 0x00, 0x00, 0x00, 0x23, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x51, 0x00, 0x05, 0x00, 0x06, 0x00, 0x00, 0x00,
  0x27, 0x00, 0x00, 0x00, 0x23, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x50, 0x00, 0x07, 0x00, 0x0d, 0x00, 0x00, 0x00, 0x28, 0x00, 0x00, 0x00,
  0x25, 0x00, 0x00, 0x00, 0x26, 0x00, 0x00, 0x00, 0x27, 0x00, 0x00, 0x00, 0x24, 0x00, 0x00, 0x00, 0x91, 0x00, 0x05, 0x00, 0x0d, 0x00, 0x00, 0x00,
  0x29, 0x00, 0x00, 0x00, 0x21, 0x00, 0x00, 0x00, 0x28, 0x00, 0x00, 0x00, 0x41, 0x00, 0x05, 0x00, 0x2a, 0x00, 0x00, 0x00, 0x2b, 0x00, 0x00, 0x00,
  0x10, 0x00, 0x00, 0x00, 0x12, 0x00, 0x00, 0x00, 0x3e, 0x00, 0x03, 0x00, 0x2b, 0x00, 0x00, 0x00, 0x29, 0x00, 0x00, 0x00, 0xfd, 0x00, 0x01, 0x00,
  0x38, 0x00, 0x01, 0x00
};

#define vert_shader_spv_size ( sizeof vert_shader_spv )

// Ponteiros, estruturas e handles que precisam ser globais para serem
// liberados via atexit.
static GLFWwindow *window;

static VkInstance             instance;
static VkPhysicalDevice       gpu;
static VkDevice               device;
static VkSurfaceKHR           surface;
static VkSwapchainKHR         swapchain;
static VkPipelineLayout       pl_layout;
static VkDescriptorSetLayout  ds_layout;
static VkPipeline             pipeline;
static VkDescriptorPool       dpool;
static VkRenderPass           renderpass;
static VkCommandPool          cmd_pool;
static VkSemaphore            sema_present;
static VkSemaphore            sema_render;
static VkImage                depth_img;
static VkImageView            depth_view;
static VkDeviceMemory         depth_mem;

static VkFence                *fences;
static VkImageView            *img_views;
static VkImage                *images;
static VkFramebuffer          *fbuffers;
static VkCommandBuffer        *cmd_buffers;
static VkExtensionProperties  *dev_ext_props;

static const char             **req_inst_exts;
static const char             **dev_exts;

static uint32_t               n_inst_exts;
static uint32_t               n_dev_exts;
static uint32_t               n_images;

// Ponteiros para funções da API.
static PFN_vkGetPhysicalDeviceSurfaceCapabilitiesKHR GetPhysicalDeviceSurfaceCapabilitiesKHR;
static PFN_vkGetPhysicalDeviceSurfaceFormatsKHR GetPhysicalDeviceSurfaceFormatsKHR;
static PFN_vkCreateSwapchainKHR CreateSwapchainKHR;
static PFN_vkDestroySwapchainKHR DestroySwapchainKHR;
static PFN_vkGetSwapchainImagesKHR GetSwapchainImagesKHR;
static PFN_vkAcquireNextImageKHR AcquireNextImageKHR;
static PFN_vkQueuePresentKHR QueuePresentKHR;

// Funções auxiliares
static _Bool  createInstance( const char **, uint32_t, VkInstance * );
static _Bool  getPhysicalDevice( VkInstance, VkPhysicalDevice * );
static _Bool  createLogicalDevice( VkPhysicalDevice, uint32_t, uint32_t, const char **, VkDevice * );
static int    getPhysDevGraphicsQueue( VkPhysicalDevice );
static _Bool  getPhysDevExtenssions( VkPhysicalDevice,
                                     uint32_t *,
                                     VkExtensionProperties **, const char *** );
static _Bool  getPFN( VkInstance, VkDevice );
static _Bool  getSurfaceColorFormat( VkPhysicalDevice, VkSurfaceKHR, VkSurfaceFormatKHR * );
static VkCompositeAlphaFlagBitsKHR getCompositeAlpha( VkSurfaceCapabilitiesKHR * );
static _Bool  createSwapchain( VkDevice, VkSurfaceKHR, const VkSurfaceCapabilitiesKHR *,
                               const VkSurfaceFormatKHR *,
                               VkCompositeAlphaFlagBitsKHR,
                               VkSwapchainKHR * );
static _Bool  getSwapchainImages( VkDevice, VkSwapchainKHR, uint32_t *, VkImage ** );
static _Bool  createImageViews( VkDevice, const VkSurfaceFormatKHR *, uint32_t, VkImage *, VkImageView ** );
static _Bool  createCommandPool( VkDevice, uint32_t, VkCommandPool * );
static _Bool  allocateCommandBuffers( VkDevice, 
                                      VkCommandPool, 
                                      uint32_t,
                                      VkCommandBuffer ** );
static _Bool  selectDepthStencilFormat( VkPhysicalDevice, VkFormat * );
static _Bool  createDepthStencilImage( VkDevice, const VkSurfaceCapabilitiesKHR *, VkFormat, VkImage * );
static _Bool  createDepthStencilImageView( VkDevice, VkImage, VkFormat, VkImageView * );
static _Bool  createWaitFences( VkDevice, uint32_t, VkFence ** );

static void   destroyInstanceSurfaceAndDevice( void );
static void   destroySwapchain( void );
static void   destroyPipeline( void );
static void   destroyDescriptorSetLayout( void );
static void   destroyDescriptorPool( void );
static void   destroyCommandPool( void );
static void   destroyFramebuffers( void );
static void   destroyFences( void );
static void   destroySemaphores( void );
static void   destroyDepthStencilBuffer( void );

int main ( int argc, char **argv )
{
  VkResult res;

  // Handles
  VkCompositeAlphaFlagBitsKHR alpha_fmt;
  VkFormat depth_fmt;
  VkImageView fb_views[2];
  VkShaderModule frag_shader;

  // Estruturas
  VkSurfaceCapabilitiesKHR surf_caps;
  VkSurfaceFormatKHR color_fmt;
  VkPhysicalDeviceMemoryProperties gpu_mem;
  VkMemoryRequirements mem_reqs;

  int queue_index;

  // 1) Inicializa o GLFW.
  glfwInit ();
  glfwWindowHint ( GLFW_CLIENT_API, GLFW_NO_API );

  atexit( glfwTerminate );

  window = glfwCreateWindow ( WIDTH, HEIGHT, "Vulkan Test", NULL, NULL );
  if ( ! window )
  {
    fputs ( "Error creating a GLFW window\n", stderr );
    return 1;
  }

  /*
    2) Pega os nomes das instâncias do Vulkan necessárias.
       Se retornar NULL, então Vulkan não está disponível.
  */
  req_inst_exts = glfwGetRequiredInstanceExtensions ( &n_inst_exts );
  if ( ! req_inst_exts )
  {
    fputs ( "Could not find any Vulkan extensions\n", stderr );
    return 2;
  }

  /* 3) Cria a instância do Vulkan. */
  if ( ! createInstance( req_inst_exts, n_inst_exts, &instance ) )
    return 3;

  /* 4) Determina a lista de dispositivos gráficos de hardware (GPUs) desde computador...
        Neste exemplo usaremos o primeiro que encontrar. */
  if ( ! getPhysicalDevice( instance, &gpu ) )
    return 4;

  /* 5) Determina que família de fila (queue) a ser usada.
        Nesse exemplo, usaremos a primeira "gráfica" que encontrar. */
  queue_index = getPhysDevGraphicsQueue( gpu );
  if ( queue_index < 0 )
  {
    fputs ( "Could not find a queue family with graphics support\n", stderr );
    return 5;
  }

  /* 6) Checa se a família de fila suporta "presentation". */
  if ( ! glfwGetPhysicalDevicePresentationSupport ( instance, gpu, queue_index ) )
  {
    fputs ( "The selected queue family does not support present mode\n", stderr );
    return 6;
  }

  /* 7) Pega todas as extensões Vulkan do dispositivo... */
  if ( ! getPhysDevExtenssions( gpu, &n_dev_exts, &dev_ext_props, &dev_exts ) )
    return 7;

  /* 8) Cria dispositivo virtual para o Vulkan. */
  if ( ! createLogicalDevice( gpu, queue_index, n_dev_exts, dev_exts, &device ) )
    return 8;

  /* 9) Pega pointeiros para funções específicas.
        Esse pedaço de código seria um equivalente ao GLEW. */
  if ( ! getPFN( instance, device ) )
    return 9;

  /* 10) Cria superfície para a janela. */
  res = glfwCreateWindowSurface ( instance, window, NULL, &surface );
  if ( res != VK_SUCCESS )
  {
    fprintf ( stderr, "glfwCreateWindowSurface() failed (%d)\n", res );
    return 10;
  }

  atexit ( destroyInstanceSurfaceAndDevice );

  /* 11) Determina o formato para cores da superfície. */
  if ( ! getSurfaceColorFormat( gpu, surface, &color_fmt ) )
    return 11;

  /* 12) Pega informação de capacidades sobre a superfície. */
  res = GetPhysicalDeviceSurfaceCapabilitiesKHR ( gpu, surface, &surf_caps );
  if ( res != VK_SUCCESS )
  {
    fprintf ( stderr, "vkGetPhysicalDeviceSurfaceCapabilitiesKHR() failed (%d)\n", res );
    return 12;
  }

  // Assume o tamanho se não conseguiu obter a capacidade?
  if ( ( int ) surf_caps.currentExtent.width < 0 )
  {
    surf_caps.currentExtent.width  = WIDTH;
    surf_caps.currentExtent.height = HEIGHT;
  }

  /* 13) Seleciona o formato alfa composto. */
  alpha_fmt = getCompositeAlpha ( &surf_caps );

  /* 14) Cria o swapchain. */
  if ( ! createSwapchain( device, surface, &surf_caps, &color_fmt, alpha_fmt, &swapchain ) )
    return 14;

  atexit ( destroySwapchain );

  /* 15) Pega as imagens do swapchain. */
  if ( ! getSwapchainImages( device, swapchain, &n_images, &images ) )
    return 15;

  /* 16) Cria image views para o swapchain. */
  if ( ! createImageViews( device, &color_fmt, n_images, images, &img_views ) )
    return 16;

  /* 17) Cria o command pool. */
  if ( ! createCommandPool( device, queue_index, &cmd_pool ) )
    return 17;

  /* 18) Aloca os command buffers - um para cada imagem. */
  if ( ! allocateCommandBuffers( device, cmd_pool, n_images, &cmd_buffers ) )
    return 18;

  /* 19) Seleciona o formato do depth buffer. */
  if ( ! selectDepthStencilFormat( gpu, &depth_fmt ) )
    return 19;

  /* 20) Cria a imagem do depth buffer. */
  if ( ! createDepthStencilImage( device, &surf_caps, depth_fmt, &depth_img ) )
    return 20;

  /* 21) Aloca memória para o depth buffer. */
  vkGetPhysicalDeviceMemoryProperties ( gpu, &gpu_mem );

  vkGetImageMemoryRequirements ( device, depth_img, &mem_reqs );

  int mem_type_idx = -1;
  for ( int i = 0; i < gpu_mem.memoryTypeCount; i++ )
  {
    if ( ( mem_reqs.memoryTypeBits & ( 1 << i ) ) &&
         ( gpu_mem.memoryTypes[i].propertyFlags & VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT ) )
    {
      mem_type_idx = i;
      break;
    }
  }

  if ( mem_type_idx < 0 )
  {
    fputs ( "Could not find a suitable type of graphics memory\n", stderr );
    return 21;
  }

  VkMemoryAllocateInfo alloc_info =
  {
    .sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO,
    .allocationSize = mem_reqs.size,
    .memoryTypeIndex = mem_type_idx
  };

  res = vkAllocateMemory ( device, &alloc_info, NULL, &depth_mem );
  if ( res != VK_SUCCESS )
  {
    fprintf ( stderr, "vkAllocateMemory() for depth stencil failed (%d)\n", res );
    return 21;
  }

  /* 22) Liga a memória alocada à imagem do depth buffer. */
  res = vkBindImageMemory ( device, depth_img, depth_mem, 0 );
  if ( res != VK_SUCCESS )
  {
    fprintf ( stderr, "vkBindImageMemory() for depth stencil failed (%d)\n", res );
    return 22;
  }

  /* 23) Cria a depth stencil view. */
  if ( ! createDepthStencilImageView( device, depth_img, depth_fmt, &depth_view ) )
    return 23;

  atexit( destroyDepthStencilBuffer );

  /* 24) Ajusta o render pass. */
  VkAttachmentDescription attachments[] =
  {
    {
      // Color attachment
      .flags          = 0,
      .format         = color_fmt.format,
      .samples        = VK_SAMPLE_COUNT_1_BIT,
      .loadOp         = VK_ATTACHMENT_LOAD_OP_CLEAR,
      .storeOp        = VK_ATTACHMENT_STORE_OP_STORE,
      .stencilLoadOp  = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
      .stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
      .initialLayout  = VK_IMAGE_LAYOUT_UNDEFINED,
      .finalLayout    = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR
    },
    {
      // Depth attachment
      .flags          = 0,
      .format         = depth_fmt,
      .samples        = VK_SAMPLE_COUNT_1_BIT,
      .loadOp         = VK_ATTACHMENT_LOAD_OP_CLEAR,
      .storeOp        = VK_ATTACHMENT_STORE_OP_STORE,
      .stencilLoadOp  = VK_ATTACHMENT_LOAD_OP_CLEAR,
      .stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
      .initialLayout  = VK_IMAGE_LAYOUT_UNDEFINED,
      .finalLayout    = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL
    }
  };

  VkAttachmentReference color_ref =
  {
    .attachment = 0,
    .layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL
  };

  VkAttachmentReference depth_ref =
  {
    .attachment = 1,
    .layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL
  };

  VkSubpassDescription subpass =
  {
    .pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS,
    .colorAttachmentCount = 1,
    .pColorAttachments = &color_ref,
    .pDepthStencilAttachment = &depth_ref
  };

  VkSubpassDependency dependencies[] =
  {
    {
      .srcSubpass      = VK_SUBPASS_EXTERNAL,
      .dstSubpass      = 0,
      .srcStageMask    = VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT,
      .dstStageMask    = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
      .srcAccessMask   = VK_ACCESS_MEMORY_READ_BIT,
      .dstAccessMask   = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT,
      .dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT
    },
    {
      .srcSubpass      = 0,
      .dstSubpass      = VK_SUBPASS_EXTERNAL,
      .srcStageMask    = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
      .dstStageMask    = VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT,
      .srcAccessMask   = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT,
      .dstAccessMask   = VK_ACCESS_MEMORY_READ_BIT,
      .dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT
    }
  };

  VkRenderPassCreateInfo pass_info =
  {
    .sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO,
    .attachmentCount = 2,
    .pAttachments = attachments,
    .subpassCount = 1,
    .pSubpasses = &subpass,
    .dependencyCount = 2,
    .pDependencies = dependencies
  };

  res = vkCreateRenderPass ( device, &pass_info, NULL, &renderpass );
  if ( res != VK_SUCCESS )
  {
    fprintf ( stderr, "vkCreateRenderPass() failed (%d)\n", res );
    return 24;
  }

  /* 25) Cria os frame buffers. */
  fb_views[1] = depth_view;

  VkFramebufferCreateInfo fb_info =
  {
    .sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO,
    .renderPass = renderpass,
    .attachmentCount = 2,
    .pAttachments = fb_views,
    .width = surf_caps.currentExtent.width,
    .height = surf_caps.currentExtent.height,
    .layers = 1
  };

  fbuffers = calloc ( n_images, sizeof *fbuffers );
  for ( uint32_t i = 0; i < n_images; i++ )
  {
    fb_views[0] = img_views[i];
    res = vkCreateFramebuffer ( device, &fb_info, NULL, &fbuffers[i] );
    if ( res != VK_SUCCESS )
    {
      fprintf ( stderr, "vkCreateFramebuffer() %d failed (%d)\n", i, res );
      return 25;
    }
  }

  atexit( destroyFramebuffers );

  /* 26) Cria semáforos para sincronizar comandos de desenho com imagens. */
  VkSemaphoreCreateInfo bake_sema = { .sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO };

  if ( vkCreateSemaphore ( device, &bake_sema, NULL, &sema_present ) != VK_SUCCESS ||
       vkCreateSemaphore ( device, &bake_sema, NULL, &sema_render ) != VK_SUCCESS )
  {
    fputs ( "Failed to create Vulkan semaphores\n", stderr );
    return 26;
  }

  atexit( destroySemaphores );

  /* 27) Cria wait fences. */
  if ( ! createWaitFences( device, n_images, &fences ) )
    return 27;

  atexit( destroyFences );

  /* 28) Prepara o triângulo. */
  static const float vertices[] =
  {
    // Position           Color
     1.0f,  1.0f,  0.0f,  1.0f,  0.0f,  0.0f,
    -1.0f,  1.0f,  0.0f,  0.0f,  1.0f,  0.0f,
     0.0f, -1.0f,  0.0f,  0.0f,  0.0f,  1.0f
  };

  static const int indices[] = {0, 1, 2};

  static const float mvp[] =
  {
    // Projection Matrix (60deg FOV, 3:2 aspect ratio, [1.0, 256.0] clipping plane range)
    1.155,  0.000,  0.000,  0.000,
    0.000,  1.732,  0.000,  0.000,
    0.000,  0.000, -1.008, -1.000,
    0.000,  0.000, -2.008,  0.000,

    // Model Matrix (identity)
    1.0f, 0.0f, 0.0f, 0.0f,
    0.0f, 1.0f, 0.0f, 0.0f,
    0.0f, 0.0f, 1.0f, 0.0f,
    0.0f, 0.0f, 0.0f, 1.0f,

    // View Matrix (distance of 2.5)
    1.0f,  0.0f,  0.0f,  0.0f,
    0.0f,  1.0f,  0.0f,  0.0f,
    0.0f,  0.0f,  1.0f,  0.0f,
    0.0f,  0.0f, -2.5f,  1.0f
  };

  struct data_
  {
    const void *bytes;
    int size;
    VkBufferUsageFlagBits usage;
    VkDeviceMemory memory;
    VkBuffer buffer;
  };

  static struct data_ data[] =
  {
    { vertices, sizeof vertices, VK_BUFFER_USAGE_VERTEX_BUFFER_BIT,  VK_NULL_HANDLE, VK_NULL_HANDLE},
    { indices,  sizeof indices,  VK_BUFFER_USAGE_INDEX_BUFFER_BIT,   VK_NULL_HANDLE, VK_NULL_HANDLE},
    { mvp,      sizeof mvp,      VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, VK_NULL_HANDLE, VK_NULL_HANDLE}
  };

  for ( uint32_t i = 0; i < 3; i++ )
  {
    VkBufferCreateInfo buf_info =
    {
      .sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
      .size = data[i].size,
      .usage = data[i].usage
    };

    res = vkCreateBuffer ( device, &buf_info, NULL, &data[i].buffer );
    if ( res != VK_SUCCESS )
    {
      fprintf ( stderr, "vkCreateBuffer() %d failed (%d)\n", i, res );
      return 28;
    }

    vkGetBufferMemoryRequirements ( device, data[i].buffer, &mem_reqs );

    int type_idx = -1;

    for ( int j = 0; j < gpu_mem.memoryTypeCount; j++ )
    {
      const uint32_t flags = VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT;

      if ( ( mem_reqs.memoryTypeBits & ( 1 << j ) ) &&
           ( gpu_mem.memoryTypes[j].propertyFlags & flags ) == flags )
      {
        mem_type_idx = j;
        break;
      }
    }

    if ( mem_type_idx < 0 )
    {
      fprintf ( stderr, "Could not find an appropriate memory type (%d)\n", i );
      return 28;
    }

    alloc_info.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
    alloc_info.allocationSize = mem_reqs.size;
    alloc_info.memoryTypeIndex = mem_type_idx;

    res = vkAllocateMemory ( device, &alloc_info, NULL, &data[i].memory );
    if ( res != VK_SUCCESS )
    {
      fprintf ( stderr, "vkAllocateMemory() %d failed (%d)\n", i, res );
      return 28;
    }

    void *buf;
    res = vkMapMemory ( device, data[i].memory, 0, alloc_info.allocationSize, 0, &buf );
    if ( res != VK_SUCCESS )
    {
      fprintf ( stderr, "vkMapMemory() %d failed (%d)\n", i, res );
      return 28;
    }

    memcpy ( buf, data[i].bytes, data[i].size );
    vkUnmapMemory ( device, data[i].memory );

    res = vkBindBufferMemory ( device, data[i].buffer, data[i].memory, 0 );
    if ( res != VK_SUCCESS )
    {
      fprintf ( stderr, "vkBindBufferMemory() %d failed (%d)\n", i, res );
      return 28;
    }
  }

  /* 29) Describe the MVP to a uniform descriptor. */
  VkDescriptorBufferInfo uniform_info =
  {
    .buffer = data[2].buffer,
    .offset = 0,
    .range = data[2].size
  };

  /* 30) Create descriptor set layout. */
  VkDescriptorSetLayoutBinding ds_bind =
  {
    .descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
    .descriptorCount = 1,
    .stageFlags = VK_SHADER_STAGE_VERTEX_BIT
  };

  VkDescriptorSetLayoutCreateInfo ds_info =
  {
    .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO,
    .bindingCount = 1,
    .pBindings = &ds_bind
  };

  res = vkCreateDescriptorSetLayout ( device, &ds_info, NULL, &ds_layout );
  if ( res != VK_SUCCESS )
  {
    fprintf ( stderr, "vkCreateDescriptorSetLayout() failed (%d)\n", res );
    return 30;
  }

  atexit( destroyDescriptorSetLayout );

  /* 31) Create pipeline layout. */
  VkPipelineLayoutCreateInfo pl_info =
  {
    .sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO,
    .setLayoutCount = 1,
    .pSetLayouts = &ds_layout
  };

  res = vkCreatePipelineLayout ( device, &pl_info, NULL, &pl_layout );
  if ( res != VK_SUCCESS )
  {
    fprintf ( stderr, "vkCreatePipelineLayout() failed (%d)\n", res );
    return 31;
  }

  /* 32) Prepare shaders. */
  VkShaderModuleCreateInfo mod_info =
  {
    .sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO,
    .codeSize = frag_shader_spv_size,
    .pCode = ( uint32_t * ) frag_shader_spv
  };

  res = vkCreateShaderModule ( device, &mod_info, NULL, &frag_shader );
  if ( res != VK_SUCCESS )
  {
    fprintf ( stderr, "vkCreateShaderModule() for fragment shader failed (%d)\n", res );
    return 32;
  }

  mod_info.codeSize = vert_shader_spv_size;
  mod_info.pCode = ( uint32_t * ) vert_shader_spv;

  VkShaderModule vert_shader;
  res = vkCreateShaderModule ( device, &mod_info, NULL, &vert_shader );
  if ( res != VK_SUCCESS )
  {
    fprintf ( stderr, "vkCreateShaderModule() for vertex shader failed (%d)\n", res );
    return 32;
  }

  /* 33) Create graphics pipeline. */
  VkPipelineInputAssemblyStateCreateInfo asm_info =
  {
    .sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO,
    .topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST
  };

  VkPipelineRasterizationStateCreateInfo raster_info =
  {
    .sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO,
    .polygonMode = VK_POLYGON_MODE_FILL,
    .frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE,
    .lineWidth = 1.0f
  };

  VkPipelineColorBlendAttachmentState cblend_att = { .colorWriteMask = 0x0f };

  VkPipelineColorBlendStateCreateInfo cblend_info =
  {
    .sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO,
    .attachmentCount = 1,
    .pAttachments = &cblend_att
  };

  VkPipelineViewportStateCreateInfo vp_info =
  {
    .sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO,
    .viewportCount = 1,
    .scissorCount = 1
  };

  static const VkDynamicState dyn_vars[] =
  {
    VK_DYNAMIC_STATE_VIEWPORT,
    VK_DYNAMIC_STATE_SCISSOR
  };

  VkPipelineDynamicStateCreateInfo dyn_info =
  {
    .sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO,
    .pDynamicStates = dyn_vars,
    .dynamicStateCount = 2
  };

  VkPipelineDepthStencilStateCreateInfo depth_info =
  {
    .sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO,
    .depthTestEnable = VK_TRUE,
    .depthWriteEnable = VK_TRUE,
    .depthCompareOp = VK_COMPARE_OP_LESS_OR_EQUAL,
    .back.failOp = VK_STENCIL_OP_KEEP,
    .back.passOp = VK_STENCIL_OP_KEEP,
    .back.compareOp = VK_COMPARE_OP_ALWAYS,
    .front = depth_info.back
  };

  VkPipelineMultisampleStateCreateInfo ms_info =
  {
    .sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO,
    .rasterizationSamples = VK_SAMPLE_COUNT_1_BIT
  };

  VkVertexInputBindingDescription vb_info =
  {
    //.binding = 0,
    .stride = 6 * sizeof ( float ), // position and color
    .inputRate = VK_VERTEX_INPUT_RATE_VERTEX
  };

  VkVertexInputAttributeDescription vert_att[] =
  {
    {
      // Position
      .binding  = 0,
      .location = 0,
      .format   = VK_FORMAT_R32G32B32_SFLOAT,
      .offset   = 0
    },
    {
      // Color
      .binding  = 0,
      .location = 1,
      .format   = VK_FORMAT_R32G32B32_SFLOAT,
      .offset   = 3 * sizeof ( float )
    }
  };

  VkPipelineVertexInputStateCreateInfo vert_info =
  {
    .sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO,
    .vertexBindingDescriptionCount = 1,
    .pVertexBindingDescriptions = &vb_info,
    .vertexAttributeDescriptionCount = 2,
    .pVertexAttributeDescriptions = vert_att
  };

  VkPipelineShaderStageCreateInfo shader_stages[] =
  {
    {
      .sType  = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
      .stage  = VK_SHADER_STAGE_VERTEX_BIT,
      .module = vert_shader,
      .pName  = "main"
    },
    {
      .sType  = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
      .stage  = VK_SHADER_STAGE_FRAGMENT_BIT,
      .module = frag_shader,
      .pName  = "main"
    }
  };

  VkGraphicsPipelineCreateInfo pipe_info =
  {
    .sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO,
    .layout = pl_layout,
    .stageCount = 2,
    .pStages = shader_stages,
    .pVertexInputState = &vert_info,
    .pInputAssemblyState = &asm_info,
    .pRasterizationState = &raster_info,
    .pColorBlendState = &cblend_info,
    .pMultisampleState = &ms_info,
    .pViewportState = &vp_info,
    .pDepthStencilState = &depth_info,
    .renderPass = renderpass,
    .pDynamicState = &dyn_info
  };

  res = vkCreateGraphicsPipelines ( device, VK_NULL_HANDLE, 1, &pipe_info, NULL, &pipeline );
  if ( res != VK_SUCCESS )
  {
    fprintf ( stderr, "vkCreateGraphicsPipelines() failed (%d)\n", res );
    return 33;
  }

  atexit( destroyPipeline );

  /* 34) Destroy shader modules (now that they have already been incorporated into the pipeline). */
  vkDestroyShaderModule ( device, vert_shader, NULL );
  vkDestroyShaderModule ( device, frag_shader, NULL );

  /* 35) Create a descriptor pool for our descriptor set. */
  VkDescriptorPoolSize ps_info =
  {
    .type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
    .descriptorCount = 1
  };

  VkDescriptorPoolCreateInfo dpool_info =
  {
    .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO,
    .poolSizeCount = 1,
    .pPoolSizes = &ps_info,
    .maxSets = 1
  };

  res = vkCreateDescriptorPool ( device, &dpool_info, NULL, &dpool );
  if ( res != VK_SUCCESS )
  {
    fprintf ( stderr, "vkCreateDescriptorPool() failed (%d)\n", res );
    return 35;
  }

  atexit( destroyDescriptorPool );

  /* 36) Allocate a descriptor set.
  	     Descriptor sets let us pass additional data into our shaders. */
  VkDescriptorSetAllocateInfo ds_alloc_info =
  {
    .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO,
    .descriptorPool = dpool,
    .descriptorSetCount = 1,
    .pSetLayouts = &ds_layout
  };

  VkDescriptorSet desc_set;
  res = vkAllocateDescriptorSets ( device, &ds_alloc_info, &desc_set );
  if ( res != VK_SUCCESS )
  {
    fprintf ( stderr, "vkAllocateDescriptorSets() failed (%d)\n", res );
    return 36;
  }

  /* 37) Set up the descriptor set. */
  VkWriteDescriptorSet write_info =
  {
    .sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
    .dstSet = desc_set,
    .descriptorCount = 1,
    .descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
    .pBufferInfo = &uniform_info,
    .dstBinding = 0
  };

  vkUpdateDescriptorSets ( device, 1, &write_info, 0, NULL );

  /* 38) Construct the command buffers.
  	     This is where we place the draw commands, which are executed by the GPU later. */
  VkCommandBufferBeginInfo cbuf_info =
  {
    .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO
  };

  static const VkClearValue clear_values[] = {
    { .color = { { 0.0f, 0.0f, 0.0f, 1.0f } } },
    { .depthStencil = { 1.0f, 0 } }
  };

  VkRenderPassBeginInfo rp_info =
  {
    .sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO,
    .renderPass = renderpass,
    .renderArea.offset.x = 0,
    .renderArea.offset.y = 0,
    .renderArea.extent = surf_caps.currentExtent,
    .clearValueCount = 2,
    .pClearValues = clear_values
  };

  VkRect2D *scissor = &rp_info.renderArea;

  VkViewport viewport =
  {
    .height = ( float ) surf_caps.currentExtent.height,
    .width = ( float ) surf_caps.currentExtent.width,
    .minDepth = 0.0f,
    .maxDepth = 1.0f
  };

  for ( uint32_t i = 0; i < n_images; i++ )
  {
    res = vkBeginCommandBuffer ( cmd_buffers[i], &cbuf_info );
    if ( res != VK_SUCCESS )
    {
      // FIXME: Precisa liberar os outros buffers (se já iniciados)?
      fprintf ( stderr, "vkBeginCommandBuffer() %d failed (%d)\n", i, res );
      return 38;
    }

    rp_info.framebuffer = fbuffers[i];
    vkCmdBeginRenderPass ( cmd_buffers[i], &rp_info, VK_SUBPASS_CONTENTS_INLINE );

    vkCmdSetViewport ( cmd_buffers[i], 0, 1, &viewport );
    vkCmdSetScissor ( cmd_buffers[i], 0, 1, scissor );

    vkCmdBindDescriptorSets ( cmd_buffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS, pl_layout, 0, 1, &desc_set, 0, NULL );
    vkCmdBindPipeline ( cmd_buffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline );

    VkDeviceSize offset = 0;
    vkCmdBindVertexBuffers ( cmd_buffers[i], 0, 1, &data[0].buffer, &offset );
    vkCmdBindIndexBuffer ( cmd_buffers[i], data[1].buffer, 0, VK_INDEX_TYPE_UINT32 );

    #define N_INDICES 3
    vkCmdDrawIndexed ( cmd_buffers[i], N_INDICES, 1, 0, 0, 1 );

    vkCmdEndRenderPass ( cmd_buffers[i] );
    res = vkEndCommandBuffer ( cmd_buffers[i] );
    if ( res != VK_SUCCESS )
    {
      // FIXME: Precisa liberar os outros buffers (se já iniciados)?
      fprintf ( stderr, "vkEndCommandBuffer() %d failed (%d)\n", i, res );
      return 38;
    }
  }

  atexit( destroyCommandPool );

  /* 39) Prepare main loop. */
  VkPipelineStageFlags wait_stage = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;

  VkSubmitInfo submit_info =
  {
    .sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
    .pWaitDstStageMask = &wait_stage,
    .pWaitSemaphores = &sema_present,
    .waitSemaphoreCount = 1,
    .pSignalSemaphores = &sema_render,
    .signalSemaphoreCount = 1,
    .commandBufferCount = 1
  };

  VkPresentInfoKHR present_info =
  {
    .sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR,
    .swapchainCount = 1,
    .pSwapchains = &swapchain,
    .pWaitSemaphores = &sema_render,
    .waitSemaphoreCount = 1
  };

  VkQueue queue;
  vkGetDeviceQueue ( device, queue_index, 0, &queue );

  /* 40) Main loop. */
  unsigned long long max64 = -1;

  while ( ! glfwWindowShouldClose ( window ) )
  {
    glfwPollEvents();

    int idx;

    res = AcquireNextImageKHR ( device, swapchain, max64, sema_present, NULL, &idx );
    if ( res != VK_SUCCESS )
    {
      fprintf ( stderr, "vkAcquireNextImageKHR() failed (%d)\n", res );
      return 40;
    }

    res = vkWaitForFences ( device, 1, &fences[idx], VK_TRUE, max64 );
    if ( res != VK_SUCCESS )
    {
      fprintf ( stderr, "vkWaitForFences() failed (%d)\n", res );
      return 40;
    }

    res = vkResetFences ( device, 1, &fences[idx] );
    if ( res != VK_SUCCESS )
    {
      fprintf ( stderr, "vkResetFences() failed (%d)\n", res );
      return 40;
    }

    submit_info.pCommandBuffers = &cmd_buffers[idx];
    res = vkQueueSubmit ( queue, 1, &submit_info, fences[idx] );
    if ( res != VK_SUCCESS )
    {
      fprintf ( stderr, "vkQueueSubmit() failed (%d)\n", res );
      return 40;
    }

    present_info.pImageIndices = &idx;
    res = QueuePresentKHR ( queue, &present_info );
    if ( res != VK_SUCCESS && res != VK_SUBOPTIMAL_KHR )
    {
      fprintf ( stderr, "vkQueuePresentKHR() failed (%d)\n", res );
      return 40;
    }
  }

  /* 41) Clean-up. */
  for ( uint32_t i = 0; i < 3; i++ )
  {
    vkDestroyBuffer ( device, data[i].buffer, NULL );
    vkFreeMemory ( device, data[i].memory, NULL );
  }

  glfwDestroyWindow ( window );

  return 0;
}

// Cria instância do Vulkan.
_Bool createInstance( const char **req_inst_exts, uint32_t n_inst_exts, VkInstance *instance )
{
  VkApplicationInfo app_info =
  {
    .sType = VK_STRUCTURE_TYPE_APPLICATION_INFO,
    .pApplicationName = "Hello Triangle",
    .applicationVersion = VK_MAKE_VERSION ( 1, 0, 0 ),
    .pEngineName = "No Engine",
    .engineVersion = VK_MAKE_VERSION ( 1, 0, 0 ),
    .apiVersion = VK_API_VERSION_1_0
  };

  VkInstanceCreateInfo create_info =
  {
    .sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO,
    .pApplicationInfo = &app_info,
    .enabledExtensionCount = n_inst_exts,
    .ppEnabledExtensionNames = req_inst_exts
  };

  VkResult res = vkCreateInstance ( &create_info, NULL, instance );
  if ( res != VK_SUCCESS )
  {
    fprintf ( stderr, "vkCreateInstance() failed (%d)\n", res );
    return 0;
  }

  return 1;
}

// Tenta pegar o dispositivo físico.
_Bool getPhysicalDevice( VkInstance instance, VkPhysicalDevice *gpu )
{
  uint32_t n_gpus = 0;

  VkResult res = vkEnumeratePhysicalDevices ( instance, &n_gpus, NULL );
  if ( res != VK_SUCCESS || n_gpus == 0 )
  {
    fprintf ( stderr, "No graphics hardware was found (physical device count = %d) (%d)\n", n_gpus, res );
    return 0;
  }

  // Usa o primeiro dispositivo físico que encontrar.
  n_gpus = 1;

  res = vkEnumeratePhysicalDevices ( instance, &n_gpus, gpu );
  if ( res != VK_SUCCESS )
  {
    fprintf ( stderr, "vkEnumeratePhysicalDevices() failed (%d)\n", res );
    return 0;
  }

  return 1;
}

// Tenta criar o dispositivo lógico.
_Bool createLogicalDevice( VkPhysicalDevice gpu,
                           uint32_t queue_index,
                           uint32_t n_dev_exts, const char **dev_exts,
                           VkDevice *device )
{
  float priority = 1.0f;

  VkDeviceQueueCreateInfo queue_info =
  {
    .sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO,
    .queueFamilyIndex = queue_index,
    .queueCount = 1,
    .pQueuePriorities = &priority
  };

  VkDeviceCreateInfo device_info =
  {
    .sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO,
    .queueCreateInfoCount = 1,
    .pQueueCreateInfos = &queue_info,
    .enabledExtensionCount = n_dev_exts,
    .ppEnabledExtensionNames = dev_exts
  };

  VkResult res = vkCreateDevice ( gpu, &device_info, NULL, device );
  if ( res != VK_SUCCESS )
  {
    fprintf ( stderr, "vkCreateDevice() failed (%d)\n", res );
    return 0;
  }

  return 1;
}

// Tenta pegar o índice do queue que permite gráficos.
// Retorna -1 se não achou.
int getPhysDevGraphicsQueue( VkPhysicalDevice gpu )
{
  int queue_index;
  uint32_t n_queues;
  VkQueueFamilyProperties *qfp;

  vkGetPhysicalDeviceQueueFamilyProperties ( gpu, &n_queues, NULL );
  if ( n_queues == 0 )
  {
    fputs ( "No queue families were found\n", stderr );
    return 5;
  }

  qfp = malloc ( n_queues * sizeof ( VkQueueFamilyProperties ) );
  if ( ! qfp )
  {
    fputs ( "Error allocating space for VkQueueFamilyProperties array.\n", stderr );
    return -1;
  }

  vkGetPhysicalDeviceQueueFamilyProperties ( gpu, &n_queues, qfp );

  queue_index = -1;
  for ( uint32_t i = 0; i < n_queues; i++ )
    if ( qfp[i].queueFlags & VK_QUEUE_GRAPHICS_BIT )
    {
      queue_index = i;
      break;
    }

  free ( qfp );

  return queue_index;
}

// Obtém as extensões do dispositivo físico.
_Bool getPhysDevExtenssions( VkPhysicalDevice gpu,
                             uint32_t *n_dev_exts, 
                             VkExtensionProperties **dev_ext_props, const char ***dev_exts )
{
  VkResult res;
  VkExtensionProperties *dep;
  const char **de;

  // tenta pegar a quantidade de extensões...
  res = vkEnumerateDeviceExtensionProperties ( gpu, NULL, n_dev_exts, NULL );
  if ( res != VK_SUCCESS || *n_dev_exts == 0 )
  {
    fputs ( "Could not find any Vulkan device extensions.\n", stderr );
    return 0;
  }

  dep = calloc ( *n_dev_exts, sizeof *dep );
  if ( ! dep )
  {
    fputs ( "ERROR allocating array of extension properties.\n", stderr );
    return 0;
  }

  // tenta obter as propriedades...
  res = vkEnumerateDeviceExtensionProperties ( gpu, NULL, n_dev_exts, dep );
  if ( res != VK_SUCCESS )
  {
    free( dep );
    fprintf ( stderr, "vkEnumerateDeviceExtensionProperties() failed (%d)\n", res );
    return 0;
  }

  // Tenta alocar espaço para os ponteiros para strings...
  de = calloc ( *n_dev_exts, sizeof *de );
  if ( ! de )
  {
    free( dep );
    fputs ( "ERROR allocating array of strings for extension names.\n", stderr );
    return 0;
  }

  *dev_ext_props = dep;
  *dev_exts = de;

  // Copia os 'n_dev_exts' ponteiros.
  for ( uint32_t i = 0; i < *n_dev_exts; i++ )
    de[i] = dep[i].extensionName;

  return 1;
}

_Bool getPFN( VkInstance instance, VkDevice device )
{
  #define TOTAL_FPTRS 7

  int tally = 0;

  GetPhysicalDeviceSurfaceCapabilitiesKHR = ( PFN_vkGetPhysicalDeviceSurfaceCapabilitiesKHR ) vkGetInstanceProcAddr ( instance, "vkGetPhysicalDeviceSurfaceCapabilitiesKHR" );
  GetPhysicalDeviceSurfaceFormatsKHR = ( PFN_vkGetPhysicalDeviceSurfaceFormatsKHR ) vkGetInstanceProcAddr ( instance, "vkGetPhysicalDeviceSurfaceFormatsKHR" );
  CreateSwapchainKHR = ( PFN_vkCreateSwapchainKHR ) vkGetDeviceProcAddr ( device, "vkCreateSwapchainKHR" );
  DestroySwapchainKHR = ( PFN_vkDestroySwapchainKHR ) vkGetDeviceProcAddr ( device, "vkDestroySwapchainKHR" );
  GetSwapchainImagesKHR = ( PFN_vkGetSwapchainImagesKHR ) vkGetDeviceProcAddr ( device, "vkGetSwapchainImagesKHR" );
  AcquireNextImageKHR = ( PFN_vkAcquireNextImageKHR ) vkGetDeviceProcAddr ( device, "vkAcquireNextImageKHR" );
  QueuePresentKHR = ( PFN_vkQueuePresentKHR ) vkGetDeviceProcAddr ( device, "vkQueuePresentKHR" );

  // conta quantos conseguiu fazer bounding...
  tally += !!GetPhysicalDeviceSurfaceCapabilitiesKHR;
  tally += !!GetPhysicalDeviceSurfaceFormatsKHR;
  tally += !!CreateSwapchainKHR;
  tally += !!DestroySwapchainKHR;
  tally += !!GetSwapchainImagesKHR;
  tally += !!AcquireNextImageKHR;
  tally += !!QueuePresentKHR;

  if ( tally != TOTAL_FPTRS )
  {
    fprintf ( stderr, "Error loading KHR extension methods (found %d/7)\n", tally );
    return 0;
  }

  return 1;
}

_Bool getSurfaceColorFormat( VkPhysicalDevice gpu, VkSurfaceKHR surface, VkSurfaceFormatKHR *color_fmt )
{
  uint32_t n_color_formats = 0;
  VkSurfaceFormatKHR *colors;
  VkResult res;

  // tenta pegar o # de formatos...
  res = GetPhysicalDeviceSurfaceFormatsKHR ( gpu, surface, &n_color_formats, NULL );
  if ( res != VK_SUCCESS || n_color_formats == 0 )
  {
    fputs ( "Could not find any color formats for the window surface\n", stderr );
    return 0;
  }

  // tenta alocar espaço para os formatos...
  colors = malloc ( n_color_formats * sizeof *colors );
  if ( ! colors )
  {
    fputs ( "Error allocating VkSurfaceFormatKHR array.\n", stderr );
    return 0;
  }

  // tenta pegar os 'n_color_formats'...
  res = GetPhysicalDeviceSurfaceFormatsKHR ( gpu, surface, &n_color_formats, colors );
  if ( res != VK_SUCCESS )
  {
    free ( colors );
    fprintf ( stderr, "GetPhysicalDeviceSurfaceFormatsKHR() failed (%d)\n", res );
    return 0;
  }

  // Procura pelo formato B8G8R8A8_UNORM (windows?).
  memset( color_fmt, 0, sizeof *color_fmt );

  for ( uint32_t i = 0; i < n_color_formats; i++ )
    if ( colors[i].format == VK_FORMAT_B8G8R8A8_UNORM )
    {
      *color_fmt = colors[i];
      break;
    }

  free ( colors );

  // Se não achou o formato, sai com erro.
  if ( color_fmt->format == VK_FORMAT_UNDEFINED )
  {
    fputs ( "The window surface does not define a B8G8R8A8 color format\n", stderr );
    return 0;
  }

  return 1;
}

VkCompositeAlphaFlagBitsKHR getCompositeAlpha( VkSurfaceCapabilitiesKHR *surf_caps )
{
  VkCompositeAlphaFlagBitsKHR alpha_fmt = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;

  static const VkCompositeAlphaFlagBitsKHR alpha_list[] =
  {
    VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR,
    VK_COMPOSITE_ALPHA_PRE_MULTIPLIED_BIT_KHR,
    VK_COMPOSITE_ALPHA_POST_MULTIPLIED_BIT_KHR,
    VK_COMPOSITE_ALPHA_INHERIT_BIT_KHR
  };

  for ( uint32_t i = 0; i < sizeof alpha_list / sizeof alpha_list[0]; i++ )
    if ( surf_caps->supportedCompositeAlpha & alpha_list[i] )
    {
      alpha_fmt = alpha_list[i];
      break;
    }

  return alpha_fmt;
}

_Bool createSwapchain( VkDevice device, 
                       VkSurfaceKHR surface,
                       const VkSurfaceCapabilitiesKHR *surf_caps,
                       const VkSurfaceFormatKHR *color_fmt,
                       VkCompositeAlphaFlagBitsKHR alpha_fmt,
                       VkSwapchainKHR *swapchain )
{
  VkResult res;
  uint32_t n_swap_images = surf_caps->minImageCount + 1;

  if ( surf_caps->maxImageCount > 0 && n_swap_images > surf_caps->maxImageCount )
    n_swap_images = surf_caps->maxImageCount;

  VkImageUsageFlags img_usage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT |
                                ( surf_caps->supportedUsageFlags & ( VK_IMAGE_USAGE_TRANSFER_SRC_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT ) );

  VkSwapchainCreateInfoKHR swap_info =
  {
    .sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR,
    //.pNext = NULL,
    .surface = surface,
    .minImageCount = n_swap_images,
    .imageFormat = color_fmt->format,
    .imageColorSpace = color_fmt->colorSpace,
    .imageExtent = surf_caps->currentExtent,
    .imageUsage = img_usage,
    .preTransform = ( VkSurfaceTransformFlagBitsKHR ) surf_caps->currentTransform,
    .imageArrayLayers = 1,
    .imageSharingMode = VK_SHARING_MODE_EXCLUSIVE,
    //.queueFamilyIndexCount = 0,
    //.pQueueFamilyIndices = NULL,
    .presentMode = VK_PRESENT_MODE_FIFO_KHR,
    //.oldSwapchain = NULL,
    .clipped = VK_TRUE,
    .compositeAlpha = alpha_fmt
  };

  res = CreateSwapchainKHR ( device, &swap_info, NULL, swapchain );
  if ( res != VK_SUCCESS )
  {
    fprintf ( stderr, "vkCreateSwapchainKHR() failed (%d)\n", res );
    return 0;
  }

  return 1;

}

_Bool getSwapchainImages( VkDevice device, VkSwapchainKHR swapchain, uint32_t *n_images, VkImage **images )
{
  // tenta pegar o # de imagens do swapchain.
  VkResult res = GetSwapchainImagesKHR ( device, swapchain, n_images, NULL );
  if ( *n_images == 0 || res != VK_SUCCESS )
  {
    fputs ( "Could not find any swapchain images\n", stderr );
    return 0;
  }

  // tenta alocar handles das imagens.
  VkImage *imgp = calloc ( *n_images, sizeof *images );
  if ( ! imgp )
  {
    fputs ( "Error allocating VkImage array.\n", stderr );
    return 0;
  }

  // tenta obter os handles.
  res = GetSwapchainImagesKHR ( device, swapchain, n_images, imgp );
  if ( res != VK_SUCCESS )
  {
    free( imgp );
    fprintf ( stderr, "vkGetSwapchainImagesKHR() failed (%d)\n", res );
    return 0;
  }

  *images = imgp;

  return 1;
}

_Bool createImageViews( VkDevice device, const VkSurfaceFormatKHR *color_fmt,
                        uint32_t n_images, VkImage *images, VkImageView **img_views )
{
  VkImageViewCreateInfo iv_info =
  {
    .sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
    .pNext = NULL,
    .format = color_fmt->format,
    .components = ( VkComponentMapping )
    {
      .r = VK_COMPONENT_SWIZZLE_R,
      .g = VK_COMPONENT_SWIZZLE_G,
      .b = VK_COMPONENT_SWIZZLE_B,
      .a = VK_COMPONENT_SWIZZLE_A
    },
    .subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
    .subresourceRange.baseMipLevel = 0,
    .subresourceRange.levelCount = 1,
    .subresourceRange.baseArrayLayer = 0,
    .subresourceRange.layerCount = 1,
    .viewType = VK_IMAGE_VIEW_TYPE_2D,
    .flags = 0
  };

  // Tenta alocar image views.
  VkImageView *ivp = calloc ( n_images, sizeof *ivp );
  if ( ! ivp )
  {
    fputs ( "Error allocating VkImageView array.\n", stderr );
    return 0;
  }

  // Cria image views.
  for ( uint32_t i = 0; i < n_images; i++ )
  {
    iv_info.image = images[i];
    VkResult res = vkCreateImageView ( device, &iv_info, NULL, &ivp[i] );
    if ( res != VK_SUCCESS )
    {
      free( ivp );
      fprintf ( stderr, "vkCreateImageView() %d failed (%d)\n", i, res );
      return 0;
    }
  }

  *img_views = ivp;

  return 1;
}

_Bool createCommandPool( VkDevice device, uint32_t queue_index, VkCommandPool *cmd_pool )
{
  VkResult res;

  VkCommandPoolCreateInfo cpool_info =
  {
    .sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO,
    .queueFamilyIndex = queue_index,
    .flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT
  };

  res = vkCreateCommandPool ( device, &cpool_info, NULL, cmd_pool );
  if ( res != VK_SUCCESS )
  {
    fprintf ( stderr, "vkCreateCommandPool() failed (%d)\n", res );
    return 0;
  }

  return 1;

}

_Bool allocateCommandBuffers( VkDevice device, 
                              VkCommandPool cmd_pool, 
                              uint32_t n_images,
                              VkCommandBuffer **cmd_buffers )
{
  VkResult res;

  VkCommandBufferAllocateInfo cbuf_alloc_info =
  {
    .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
    .commandPool = cmd_pool,
    .level = VK_COMMAND_BUFFER_LEVEL_PRIMARY,
    .commandBufferCount = n_images
  };

  *cmd_buffers = calloc ( n_images, sizeof **cmd_buffers );
  if ( ! *cmd_buffers )
  {
    fputs ( "Error allocating VkCommandBuffer array.\n", stderr );
    return 0;
  }

  res = vkAllocateCommandBuffers ( device, &cbuf_alloc_info, *cmd_buffers );
  if ( res != VK_SUCCESS )
  {
    free ( *cmd_buffers );
    *cmd_buffers = NULL;
    fprintf ( stderr, "vkAllocateCommandBuffers() failed (%d)\n", res );
    return 0;
  }

  return 1;

}

_Bool createWaitFences( VkDevice device, uint32_t n_images, VkFence **fences )
{
  VkFenceCreateInfo fence_info =
  {
    .sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO,
    .flags = VK_FENCE_CREATE_SIGNALED_BIT
  };

  VkFence *fp;

  fp = calloc ( n_images, sizeof *fp );
  if ( ! fp )
  {
    fputs ( "Error allocating VkFence array.\n", stderr );
    return 0;
  }

  for ( uint32_t i = 0; i < n_images; i++ )
  {
    VkResult res;

    res = vkCreateFence ( device, &fence_info, NULL, &fp[i] );
    if ( res != VK_SUCCESS )
    {
      free( fp );
      fprintf ( stderr, "vkCreateFence() failed (%d)\n", res );
      return 0;
    }
  }

  *fences = fp;

  return 1;
}

_Bool selectDepthStencilFormat( VkPhysicalDevice gpu, VkFormat *depth_fmt )
{
  static const VkFormat formats[] =
  {
    VK_FORMAT_D32_SFLOAT_S8_UINT,
    VK_FORMAT_D32_SFLOAT,
    VK_FORMAT_D24_UNORM_S8_UINT,
    VK_FORMAT_D16_UNORM_S8_UINT,
    VK_FORMAT_D16_UNORM
  };

  *depth_fmt = VK_FORMAT_UNDEFINED;

  for ( uint32_t i = 0; i < sizeof formats / sizeof formats[0]; i++ )
  {
    VkFormatProperties cfg;

    vkGetPhysicalDeviceFormatProperties ( gpu, formats[i], &cfg );

    if ( cfg.optimalTilingFeatures & VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT )
    {
      *depth_fmt = formats[i];
      break;
    }
  }

  if ( *depth_fmt == VK_FORMAT_UNDEFINED )
  {
    fprintf ( stderr, "Could not find a suitable depth format\n" );
    return 0;
  }

  return 1;
}

_Bool createDepthStencilImage( VkDevice device, const VkSurfaceCapabilitiesKHR *surf_caps, VkFormat depth_fmt, VkImage *depth_img )
{
  VkResult res;

  VkImageCreateInfo dimg_info =
  {
    .sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO,
    .imageType = VK_IMAGE_TYPE_2D,
    .format = depth_fmt,
    .extent = ( VkExtent3D )
    {
      .width = surf_caps->currentExtent.width,
      .height = surf_caps->currentExtent.height,
      .depth = 1
    },
    .mipLevels = 1,
    .arrayLayers = 1,
    .samples = VK_SAMPLE_COUNT_1_BIT,
    .tiling = VK_IMAGE_TILING_OPTIMAL,
    .usage = VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT
  };

  res = vkCreateImage ( device, &dimg_info, NULL, depth_img );
  if ( res != VK_SUCCESS )
  {
    fprintf ( stderr, "vkCreateImage() for depth stencil failed (%d)\n", res );
    return 0;
  }

  return 1;
}

_Bool createDepthStencilImageView( VkDevice device, VkImage depth_img, VkFormat depth_fmt, VkImageView *depth_view )
{
  VkResult res;

  VkImageAspectFlagBits aspect = VK_IMAGE_ASPECT_DEPTH_BIT;

  if ( depth_fmt >= VK_FORMAT_D16_UNORM_S8_UINT )
    aspect |= VK_IMAGE_ASPECT_STENCIL_BIT;

  VkImageViewCreateInfo dview_info =
  {
    .sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
    .viewType = VK_IMAGE_VIEW_TYPE_2D,
    .image = depth_img,
    .format = depth_fmt,
    //.subresourceRange.baseMipLevel = 0,
    .subresourceRange.levelCount = 1,
    //.subresourceRange.baseArrayLayer = 0,
    .subresourceRange.layerCount = 1,
    .subresourceRange.aspectMask = aspect
  };

  res = vkCreateImageView ( device, &dview_info, NULL, depth_view );
  if ( res != VK_SUCCESS )
  {
    fprintf ( stderr, "vkCreateImageView() for depth stencil failed (%d)\n", res );
    return 23;
  }
}

// Destrutores.
void destroyInstanceSurfaceAndDevice( void )
{
  vkDestroyDevice ( device, NULL );

  vkDestroySurfaceKHR ( instance, surface, NULL );
  vkDestroyInstance ( instance, NULL );

  free ( dev_exts );
  free ( dev_ext_props );
}

void destroySwapchain( void )
{
  for ( uint32_t i = 0; i < n_images; i++ )
    vkDestroyImageView ( device, img_views[i], NULL );

  free ( img_views );
  free ( images );

  DestroySwapchainKHR ( device, swapchain, NULL );
}

void destroyPipeline( void )
{
  vkDestroyPipelineLayout ( device, pl_layout, NULL );
  vkDestroyPipeline ( device, pipeline, NULL );
  vkDestroyRenderPass ( device, renderpass, NULL );

}

void destroyDescriptorSetLayout( void )
{ vkDestroyDescriptorSetLayout ( device, ds_layout, NULL ); }

void destroyDescriptorPool( void )
{ vkDestroyDescriptorPool ( device, dpool, NULL ); }

void destroyCommandPool( void )
{
  //vkFreeCommandBuffers(device, cmd_pool, n_images, cmd_buffers);
  vkDestroyCommandPool ( device, cmd_pool, NULL );
  free ( cmd_buffers );
}

void destroyFramebuffers( void )
{
  for ( uint32_t i = 0; i < n_images; i++ )
    vkDestroyFramebuffer ( device, fbuffers[i], NULL );
  free ( fbuffers );
}

void destroyFences( void )
{
  for ( uint32_t i = 0; i < n_images; i++ )
    vkDestroyFence ( device, fences[i], NULL );
  free ( fences );
}

void destroySemaphores( void )
{
  vkDestroySemaphore ( device, sema_present, NULL );
  vkDestroySemaphore ( device, sema_render, NULL );
}

void destroyDepthStencilBuffer( void )
{
  vkDestroyImageView ( device, depth_view, NULL );
  vkDestroyImage ( device, depth_img, NULL );
  vkFreeMemory ( device, depth_mem, NULL );
}

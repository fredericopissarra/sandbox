#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>

#include <lua.h>
#include <lauxlib.h>
#include <lualib.h>

// Para fechar a instância do LUA "automagicamente".
static void closelua_( lua_State **L )
{ if ( *L ) { lua_close( *L ); *L = NULL; } }

// Exemplo de passagem de argumentos e recuperação, chamando uma função em lua.
int main ( void )
{
  lua_State *L __attribute__((cleanup( closelua_ )));

  // Cria instância...
  L = luaL_newstate();
  if ( ! L )
  {
    fputs( "Cannot instantiate LUA.\n", stderr );
    return EXIT_FAILURE;
  }

  // Carrega todas as libs padrão.
  luaL_openlibs( L );

  // Para testar se um valor global é colocado ou não na pilha como NIL, se não existir!
  luaL_dostring( L, "" );

  lua_getglobal( L, "X" );
  if ( lua_isnil( L, -1 ) )
    printf( "nil colocado na pilha. Stack top = %d\n", lua_gettop( L ) );

  // Joga fora o resultado que está na pilha.
  lua_pop( L, 1 );

  return EXIT_SUCCESS;
}

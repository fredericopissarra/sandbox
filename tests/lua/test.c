#include <stdio.h>
#include <stdlib.h>

#include <lua.h>
#include <lauxlib.h>
#include <lualib.h>

// To destroy a LUA instance autoMAGICALLY...
static void closelua_( lua_State **L )
{ if ( *L ) { lua_close( *L ); *L = NULL; } }

// ATTENTION: The LUA stack grows UP and starts at 1 (not 0).
// We can use negative values to use offset based in the stack top
// (last item pushed). I mean the stack can be accesses from -1 (last)
// to -N (first pushed item). Use `lua_gettop()` to get how many items
// were pushed.
//
// lua_pop() pops N elements...

// Another useful detail is that `integers` don't exist _per se_...
// they are the type `ptrdiff_t`. The "numeric" type is `double`.
// Use lua_pushTYPE and lua_toTYPE (where TYPE is 'number', for example)
// to push or get an element from the stack.


// Function to be called from LUA script! 
static int calledFromLuaFunc( lua_State *L )
{
  // LUA pushes the arguments in order (different from C last to first cdecl convention).
  // Here I'm getting the only pushed argument (X).
  // luaL_checknumber() works like lua_tonumber(), but checks the type...
  double d = luaL_checknumber( L, 1 );

  d *= 3;

  // Pushes the result to the stack.
  // The argument previously in the stack will be discarded by the lua engine.

  lua_pushnumber( L, d );

  // Always return the number of results pushed.
  return 1;
}
  
int main ( void )
{
  lua_State *L __attribute__((cleanup( closelua_ )));

  // Create LUA instance.
  L = luaL_newstate();
  if ( ! L )
  {
    fputs( "Cannot instantiate LUA.\n", stderr );
    return EXIT_FAILURE;
  }

  // Load all available libs (You can load them individually
  // but for this example this is easier).
  luaL_openlibs( L );

  // Add objects to global list. This makes X=3 in LUA.
  lua_pushinteger( L, 3 );
  lua_setglobal( L, "X" );

  // Register 'myfunc' as out called function from LUA.
  lua_pushcfunction( L, calledFromLuaFunc );
  lua_setglobal( L, "myfunc" );

  // Now we can load the LUA script from a file.
  // There are other ways to load a string as the script.
  // See LUA documentation.
  if ( luaL_dofile( L, "test.lua" ) )
  {
    // If luaL_dofile() fails, it will push the error message in LUA stack
    // we have to get the string and show to the user, than pop it.

    fputs( lua_tostring( L, -1 ), stderr );
    lua_pop( L, 1 );

    return EXIT_FAILURE;
  }

  // The script was loaded ok... now we put the 'main' (from LUA script)
  // in the stack and call it.
  //
  // Note, when called from C a LUA function must receive the number of arguments
  // IF there are any... In out case, 'main' have no arguments, then to call it is
  // as simple as:
  lua_getglobal( L, "main" );   // get the global 'main' object.

  // Is it a function?
  if ( ! lua_isfunction( L, 1 ) )
  {
    // No? Discard it and exit with error.
    lua_pop( L, 1 );
    fputs( "ERROR: 'main' isn't a funcion.\n", stderr );
    return EXIT_FAILURE;
  }

  // Finally call the object in the stack.
  // Here we have 0 arguments, 1 result and the final 0 is an
  // error handler (in the lua stack) or 0 (if the error object
  // pushed, on errors).
  //
  // lua_pcall() will return 0 in case of success.
  if ( lua_pcall( L, 0, 1, 0 ) )
  {
    // In case of error, get the string on top of the stack,
    // print it and pop.
    fputs( lua_tostring( L, -1 ), stderr ); 
    lua_pop( L, 1 );
    return EXIT_FAILURE;
  }

  // Well... everything is ok then LUA left an integer in the stack...
  printf( "main returns %td\n", lua_tointeger( L, -1 ) );

  // Pop the result and we are finished.
  lua_pop( L, 1 );

  return EXIT_SUCCESS;
}

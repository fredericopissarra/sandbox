#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <gmp.h>
#include <cycle_counting.h>

__attribute__((noinline))
double f( double a, double b, double c )
{ return a * b + c; }

// as rotinas abaixo trabalham apenas com 2 "casas decimais fracionárias",
// convertendo os doubles para inteiros. g() tem o problema de que os valores podem
// exceder a precisão de int64_t. h() não tem esse problema, mas é lenta por causa da
// libgmp.

__attribute__((noinline))
double g( double a, double b, double c )
{
  int64_t r, s, t;

  r = a * 100.0;
  s = b * 100.0;
  t = c * 100.0;

  // A divisão final por 100.0 (double) é necessária que seja 'double'!
  return ( ( r * s ) / 100 + t ) / 100.0;
}

// Faz a mesma coisa que acima, mas usando aritmética de precisão múltipla integral
__attribute__((noinline))
double h( double a, double b, double c )
{
  mpz_t r, s, t;
  double d;

  mpz_init_set_d( r, a * 100.0 );
  mpz_init_set_d( s, b * 100.0 );
  mpz_init_set_d( t, c * 100.0 );

  mpz_mul( r, r, s );       // r *= s;
  mpz_div_ui( r, r, 100 );  // r /= 100
  mpz_add( r, r, t );       // r += t;

  d = mpz_get_d( r );

  // Livra-se dos objetos.
  mpz_clears( r, s, t, NULL );

  return d / 100.0;
}

int main( void )
{
  counter_T c1, c2, c3;
  double a, b, c, r1, r2, r3;

  fputs( "3 values: ", stdout ); fflush( stdout );
  if ( scanf( "%lf %lf %lf", &a, &b, &c ) != 3 )
  {
    fputs( "ERROR: Wrong values.\n", stderr );
    return EXIT_FAILURE;
  }

  c1 = BEGIN_TSC();
  r1 = f( a, b, c );
  c1 = END_TSC( c1 );

  c2 = BEGIN_TSC();
  r2 = g( a, b, c );
  c2 = END_TSC( c2 );

  c3 = BEGIN_TSC();
  r3 = h( a, b, c );  
  c3 = END_TSC( c3 );

  printf( "f=%.2f (%lu cycles)\n"
          "g=%.2f (%lu cycles)\n"
          "h=%.2f (%lu cycles)\n",
          r1, c1,
          r2, c2,
          r3, c3 );

  return EXIT_SUCCESS;
}

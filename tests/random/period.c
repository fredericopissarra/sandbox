#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define SAMPLES 1000

int main( void )
{
  unsigned int i, n, tmp;
  unsigned int dice[6] = { 0 };

  srand( time( NULL ) );

  // roll the D6 dice 400 times.
  i = SAMPLES, n = 32;

  while ( i-- )
  {
    if ( ! n-- )
    {
      putchar( '\n' );
      n = 31;
    }

    tmp = rand() % 6;
    dice[tmp]++;

    printf( " %d", tmp + 1 );
  }

  printf("\nDistribution (%u samples, ideal: %.1f%%):\n", SAMPLES, 100.0/6.0 );
  for ( i = 0; i < 6; i++ )
    printf( "%d:%.1f%% ", i+1, 100.0 * dice[i] / SAMPLES );
  putchar('\n');
}

  bits  16

  section _STACK stack align=2

  dw  512
_stktop:

  section _TEXT class=CODE align=2

..start:
  ; Is this needed?
  mov   ax,_STACK
  mov   ss,ax
  mov   sp,_stktop wrt _STACK

  mov   ax,0x13
  int   0x10

  call  setup_palette
  call  copy_pic_to_framebuffer

  xor   ah,ah
  int   0x16

  mov   ax,3
  int   0x10

  mov   ax,0x4c00
  int   0x21

copy_pic_to_framebuffer:
  push  ds
  mov   ax,0xa000
  mov   es,ax
  xor   di,di
  mov   ax,PICTURE_SEG
  mov   ds,ax
  mov   si,picture wrt PICTURE_SEG
  mov   cx,(320*200)/4
  rep   movsd
  pop   ds
  ret

; Rewrites VGA palette.
setup_palette:
  push  ds
  mov   ax,PALETTE_SEG
  mov   ds,ax
  mov   si,palette wrt PALETTE_SEG
  xor   cx,cx
  mov   dx,0x3c8
.loop:
  mov   al,cl
  out   dx,al

  ; Load BGR entry and recalc color using 6 bits per component (truncate).
  lodsw       ; AL = B, AH = G
  shr   al,2
  shr   ah,2
  mov   bx,ax
  lodsb       ; AL = R
  shr   al,2

  inc   dx

  ; First R, than G and finally B.
  out   dx,al
  mov   al,bh
  out   dx,al
  mov   al,bl
  out   dx,al

  dec   dx

  inc   cx
  test  ch,ch
  jz    .loop

  pop   ds
  ret

  section PALETTE_SEG

palette:
  incbin "image.tga",18,768

  section PICTURE_SEG

picture:
  incbin "image.tga",18+768,(320*200)

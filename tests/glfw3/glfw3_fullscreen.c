#include <stdlib.h>
#include <stdio.h>
#include <GLFW/glfw3.h>
#include <GL/gl.h>

#define WINDOW_TITLE "My Window"

// Usando FULLHD nesse exemplo.
#define WINDOW_WIDTH 1920
#define WINDOW_HEIGHT 1080

#define FSAA_SAMPLES 16

static int check_videomode ( GLFWmonitor *, int, int, int, int, int, int );

// Callbacks.
static void error_callback ( int, const char * );
static void key_callback ( GLFWwindow *, int, int, int, int );

// A função de desenho de quadros.
static void draw ( void );

int main ( void )
{
  GLFWmonitor *primaryMonitor;
  GLFWwindow *mainWindow;

  glfwSetErrorCallback ( error_callback );

  if ( !glfwInit() )
    return EXIT_FAILURE;

  atexit ( glfwTerminate );

  primaryMonitor = glfwGetPrimaryMonitor();

  if ( primaryMonitor == NULL )
    return EXIT_FAILURE;

  // Check video mode geometry and refreshrate.
  if ( ! check_videomode ( primaryMonitor, WINDOW_WIDTH, WINDOW_HEIGHT, 8, 8, 8, 60 ) )
  {
    fprintf ( stderr, "Video mode %dx%d (RGBA:8888) @ 60Hz\n",
              WINDOW_WIDTH, WINDOW_HEIGHT );
    return EXIT_FAILURE;
  }

  // defaults to R8G8B8A8 (32 bits bitplanes).
  glfwWindowHint ( GLFW_RED_BITS,   8 );
  glfwWindowHint ( GLFW_GREEN_BITS, 8 );
  glfwWindowHint ( GLFW_BLUE_BITS,  8 );
  glfwWindowHint ( GLFW_ALPHA_BITS, 8 );

  // overrides.
  // 60 fps, double buffering, not-resizeable and not decorated window,
  // OpenGL 4.2 context core profile.
  glfwWindowHint ( GLFW_REFRESH_RATE,          60 );
  glfwWindowHint ( GLFW_DOUBLEBUFFER,          1 );
  glfwWindowHint ( GLFW_RESIZABLE,             0 );
  glfwWindowHint ( GLFW_DECORATED,             0 );
  glfwWindowHint ( GLFW_CONTEXT_VERSION_MAJOR, 4 );
  glfwWindowHint ( GLFW_CONTEXT_VERSION_MINOR, 2 );
  glfwWindowHint ( GLFW_OPENGL_PROFILE,        GLFW_OPENGL_CORE_PROFILE );

  // Full Screen Anti-Aliasing (maybe this is unwanted [low performance?]).
  glfwWindowHint ( GLFW_SAMPLES, FSAA_SAMPLES );

  // Create Window...
  mainWindow = glfwCreateWindow ( WINDOW_WIDTH, WINDOW_HEIGHT,
                                  WINDOW_TITLE,
                                  primaryMonitor,
                                  NULL );

  if ( ! mainWindow )
    return EXIT_FAILURE;

  glfwSetKeyCallback ( mainWindow, key_callback );

  glfwMakeContextCurrent ( mainWindow );

  glfwShowWindow ( mainWindow );

  // Hide the cursor.
  glfwSetInputMode ( mainWindow, GLFW_CURSOR, GLFW_CURSOR_DISABLED );

  // "Events" loop.
  while ( ! glfwWindowShouldClose ( mainWindow ) )
  {
    // drawcode here.
    draw();

    glfwSwapBuffers ( mainWindow );
    glfwPollEvents();
  }

  glfwDestroyWindow ( mainWindow );

  return EXIT_SUCCESS;
}

void error_callback ( int error, const char *description )
{
  fprintf ( stderr, "Error: %s\n", description );
}

void key_callback ( GLFWwindow *window, int key, int scancode, int action, int mods )
{
  if ( key == GLFW_KEY_ESCAPE && action == GLFW_PRESS )
    glfwSetWindowShouldClose ( window, 1 );
}

void draw ( void )
{
  glClear ( GL_COLOR_BUFFER_BIT );
  glFlush();
}

int check_videomode ( GLFWmonitor *monitor,
                      int width, int height,
                      int red, int green, int blue,
                      int refreshrate )
{
  const GLFWvidmode *modeptr;
  int count;

  modeptr = ( GLFWvidmode * ) glfwGetVideoModes ( monitor, &count );

  if ( modeptr )
    while ( count-- )
    {
      if ( modeptr->width == width &&
           modeptr->height == height &&
           modeptr->redBits == red &&
           modeptr->greenBits == green &&
           modeptr->blueBits == blue &&
           modeptr->refreshRate == refreshrate )
        return 1;

      modeptr++;
    }

  return 0;
}


#include <stdio.h>

#include <ft2build.h>
#include FT_FREETYPE_H

#include <GL/glew.h>

#define GLFW_INCLUDE_GLCOREARB
#include <GLFW/glfw3.h>

static const char *VERTEX_SHADER = "#version 410 core\n"
                                   "in vec4 in_Position;\n"
                                   "out vec2 texCoords;\n"
                                   "void main(void) {\n"
                                   " gl_Position = vec4(in_Position.xy, 0, 1);\n"
                                   " texCoords = in_Position.zw;\n"
                                   "}\n";


static const char *FRAGMENT_SHADER = "#version 410 core\n"
                                     "precision highp float;\n"
                                     "uniform sampler2D tex;\n"
                                     "uniform vec4 color;\n"
                                     "in vec2 texCoords;\n"
                                     "out vec4 fragColor;\n"
                                     "void main(void) {\n"
                                     " fragColor = vec4(1, 1, 1, texture(tex, texCoords).r) * color;\n"
                                     "}\n";

// Vetor2D e coordenada de textura
struct vec2d {
  float x, y, s, t;
};

static void render_text ( const char *str, FT_Face face, float x, float y, float sx, float sy )
{
  glPixelStorei ( GL_UNPACK_ALIGNMENT, 1 );

  const FT_GlyphSlot glyph = face->glyph;

  // Obtém o glyph, monta a textura e a desenha...
  while ( *str )
  {
    // FIXME: Não considera UTF-8 ou UTF-16 ainda!
    if ( FT_Load_Char ( face, *str++, FT_LOAD_RENDER ) != 0 )
      continue;

    // Carrega a nova para a GPU (apenas RED de 8 bits)...
    glTexImage2D ( GL_TEXTURE_2D, 0, GL_R8,
                   glyph->bitmap.width, glyph->bitmap.rows,
                   0, GL_RED, GL_UNSIGNED_BYTE, glyph->bitmap.buffer );

    // Obtém os tamanhos e posições do glyph.
    const float vx = x + glyph->bitmap_left * sx;
    const float vy = y + glyph->bitmap_top * sy;
    const float w = glyph->bitmap.width * sx;
    const float h = glyph->bitmap.rows * sy;

    // Monta o retângulo com 2 triângulos.
    // Poderia ser um triangle-fan.
    struct vec2d data[6] =
      {
        {vx, vy, 0, 0},
        {vx, vy - h, 0, 1},
        {vx + w, vy, 1, 0},
        {vx + w, vy, 1, 0},
        {vx, vy - h, 0, 1},
        {vx + w, vy - h, 1, 1}
      };

    // Carrega coordenadas para a GPU e desenha a textura no retângulo informado
    // usando o shader.
    glBufferData ( GL_ARRAY_BUFFER, sizeof data, data, GL_DYNAMIC_DRAW );
    glVertexAttribPointer ( 0, 4, GL_FLOAT, GL_FALSE, 0, 0 );
    glDrawArrays ( GL_TRIANGLES, 0, 6 );

    // Avança x e y de acordo com as características do glyph.
    x += ( glyph->advance.x >> 6 ) * sx;
    y += ( glyph->advance.y >> 6 ) * sy;
  }

  glPixelStorei ( GL_UNPACK_ALIGNMENT, 4 );
}

// Globais por causa do cleanup.
static GLuint texture, sampler;
static GLuint vao, vbo;
static GLuint program;
static GLuint vs, fs;
static FT_Library ft_lib;
static FT_Face face;

static void cleanup( void )
{
  FT_Done_Face ( face );
  FT_Done_FreeType ( ft_lib );

  glDeleteTextures ( 1, &texture );
  glDeleteSamplers ( 1, &sampler );
  glDeleteBuffers ( 1, &vbo );
  glDeleteVertexArrays ( 1, &vao );
  glDeleteShader ( vs );
  glDeleteShader ( fs );
  glDeleteProgram ( program );
}

int main( void )
{
  // Inicializa a libfreetype2. 
  if ( FT_Init_FreeType ( &ft_lib ) != 0 )
  {
    fputs( "Couldn't initialize FreeType library.\n", stderr );
    return 1;
  }

  atexit( cleanup );

  // Carrega a fonte.
  if ( FT_New_Face ( ft_lib, "./DejaVuSans.ttf", 0, &face ) != 0 )
  {
    fputs ( "Unable to load font.\n", stderr );
    return 1;
  }

  if ( glfwInit() != GL_TRUE )
  {
    fputs ( "Couldn't load GLFW library.\n", stderr );
    return 1;
  }

  // Vamos usar OpenGL 4.1 core profile.
  glfwWindowHint ( GLFW_CONTEXT_VERSION_MAJOR, 4 );
  glfwWindowHint ( GLFW_CONTEXT_VERSION_MINOR, 1 );
  glfwWindowHint ( GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE );

  const int WIDTH = 640;
  const int HEIGHT = 480;
  const double SCALEX = 2.0 / WIDTH;
  const double SCALEY = 2.0 / HEIGHT;

  GLFWwindow *window = glfwCreateWindow ( WIDTH, HEIGHT, "OpenGL Text Rendering", NULL, NULL );

  glfwMakeContextCurrent ( window );

  // Precisa do GLEW para obter os ponteiros das extensões padrão.
  if ( glewInit() != GLEW_OK )
  {
    fputs ( "Coulnd't initialize GLEW\n", stderr );
    return 1;
  }

  // Ok. vamos usar projeção ortogonal.
  glViewport ( 0, 0, WIDTH, HEIGHT );

  // Cria objetos do OpenGL
  glGenBuffers ( 1, &vbo );
  glGenVertexArrays ( 1, &vao );
  glGenTextures ( 1, &texture );
  glGenSamplers ( 1, &sampler );

  // Ajusta propriedades dos samplers (texturas)
  glSamplerParameteri ( sampler, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE );
  glSamplerParameteri ( sampler, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE );
  glSamplerParameteri ( sampler, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
  glSamplerParameteri ( sampler, GL_TEXTURE_MAG_FILTER, GL_LINEAR );

  // Inicializa shaders
  vs = glCreateShader ( GL_VERTEX_SHADER );
  glShaderSource ( vs, 1, &VERTEX_SHADER, 0 );
  glCompileShader ( vs );
  fs = glCreateShader ( GL_FRAGMENT_SHADER );
  glShaderSource ( fs, 1, &FRAGMENT_SHADER, 0 );
  glCompileShader ( fs );
  program = glCreateProgram();
  glAttachShader ( program, vs );
  glAttachShader ( program, fs );
  glLinkProgram ( program );
  glUseProgram ( program );

  // Pega locations de objetos dos shaders.
  glBindAttribLocation ( program, 0, "in_Position" );
  GLuint texUniform = glGetUniformLocation ( program, "tex" );
  GLuint colorUniform = glGetUniformLocation ( program, "color" );

  // Ajusta alguns estados do OpenGL.
  glEnable ( GL_BLEND );
  glDisable ( GL_CULL_FACE );
  glDisable ( GL_DEPTH_TEST );
  glBlendFunc ( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
  glClearColor ( 0.1, 0.2, 0.4, 0.0 );

  // Bindings de textura, sampler e vertex array e vertex buffer. 
  glActiveTexture ( GL_TEXTURE0 );
  glBindTexture ( GL_TEXTURE_2D, texture );
  glBindSampler ( 0, sampler );
  glBindVertexArray ( vao );
  glEnableVertexAttribArray ( 0 );
  glBindBuffer ( GL_ARRAY_BUFFER, vbo );

  // Vamos escalonar a fonte para 50...
  FT_Set_Pixel_Sizes ( face, 0, 50 );

  while ( ! glfwWindowShouldClose ( window ) )
  {
    glClear ( GL_COLOR_BUFFER_BIT );

    glUniform4f ( colorUniform, 1.0f, 1.0f, 1.0f, 1.0f );
    glUniform1i ( texUniform, 0 );

    // Desenha a string...
    render_text ( "Hello World!", face, -0.5, 0, SCALEX, SCALEY );

    glfwSwapBuffers ( window );
    glfwPollEvents();
  }

  return 0;
}

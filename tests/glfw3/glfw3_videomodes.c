#include <stdio.h>
#include <stdlib.h>
#include <GLFW/glfw3.h>

int main ( void )
{
  GLFWmonitor *primaryMonitor;
  const GLFWvidmode *videoModes;
  int videoModesCount;

  if ( ! glfwInit() )
    return EXIT_FAILURE;

  primaryMonitor = glfwGetPrimaryMonitor();
  if ( ! primaryMonitor )
    return EXIT_FAILURE;

  videoModes = glfwGetVideoModes ( primaryMonitor,
                                   &videoModesCount );
  if ( ! videomodes )
    return EXIT_FAILURE;

  /* Print video modes available */
  printf ( "There are %d video modes available:\n",
           videoModesCount );

  for ( int i = 0; i < videoModesCount; i++ )
    printf ( "  % 3d: % 4dx%-4d - R%dG%dB%d (%d Hz).\n",
             i,
             videoModes[i].width,
             videoModes[i].height,
             videoModes[i].redBits,
             videoModes[i].greenBits,
             videoModes[i].blueBits,
             videoModes[i].refreshRate );

  glfwTerminate();

  return EXIT_SUCCESS;
}


#include <stdio.h>
#include <stdlib.h>
#include <GLFW/glfw3.h>

int main ( void )
{
  int major, minor, revision;

  if ( ! glfwInit() )
    return EXIT_FAILURE;

  glfwGetVersion ( &major, &minor, &revision );
  glfwTerminate();

  printf ( "GLFW %d.%d.%d\n", major, minor, revision );

  return EXIT_SUCCESS;
}

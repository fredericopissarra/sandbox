#include <fstream>
#include <vector>
#include <string>

bool writeFileOFStream( const char *path, const std::vector<std::string>& lines )
{
  std::ofstream of( path, std::ios::trunc );

  if ( ! of )
    return false;

  for ( auto& i : lines )
  {
    of << i << '\n';

    if ( ! of )
      return false;
  }

  return true;
}

#include <cstdlib>
#include <ctime>

#include <vector>
#include <string>
#include <iostream>
#include <fstream>

#include "cycle_counting.h"

std::vector<std::string> createRandomLines( void );

__attribute__((noinline))
static void writeOnce( const std::vector<std::string>& lines )
{
  std::ofstream of( "test_alwaysopened.txt" );

  for ( auto& i : lines )
    of << i << '\n';
}

__attribute__((noinline))
static void writeLine( const std::string& line )
{
  std::ofstream of( "test_openondemand.txt", std::ios::app | std::ios::ate );

  of << line << '\n';
}

int main( void )
{
  counter_T c;

  srand( time( NULL ) );

  std::vector<std::string> v = createRandomLines();

  std::cout << "Open file and write all lines...";
  c = BEGIN_TSC();
    writeOnce( v );
  c = END_TSC( c );
  std::cout << ' ' << c << " cycles.\n";

  std::cout << "Write all lines opening and closing file for each line...";
  c = BEGIN_TSC();
  for ( auto& i : v )
    writeLine( i );
  c = END_TSC( c );
  std::cout << ' ' << c << " cycles.\n";

  return EXIT_SUCCESS;
}

#include <cstdlib>
#include <ctime>

#include <vector>
#include <string>
#include <iostream>

#include "cycle_counting.h"

std::vector<std::string> createRandomLines ( void );
bool writeFileStream ( const char *, const std::vector<std::string> & );
bool writeFileOFStream ( const char *, const std::vector<std::string> & );
bool writeFileSyscall ( const char *, const std::vector<std::string> & );

int main ( void )
{
  static const char *fnames[] = { "stream.txt", "ofstream.txt", "write.txt" };
  counter_T c1, c2, c3;

  srand ( time ( NULL ) );

  std::vector<std::string> v = createRandomLines();

  std::cout << "Writing using FILE * stream pointer...\n";
  c1 = BEGIN_TSC();

  if ( !writeFileStream ( fnames[0], v ) )
    return EXIT_FAILURE;

  c1 = END_TSC ( c1 );

  std::cout << "Writing using std::ofstream...\n";
  c2 = BEGIN_TSC();

  if ( !writeFileOFStream ( fnames[1], v ) )
    return EXIT_FAILURE;

  c2 = END_TSC ( c2 );

  std::cout << "Writing using file descriptor...\n";
  c3 = BEGIN_TSC();

  if ( !writeFileSyscall ( fnames[2], v ) )
    return EXIT_FAILURE;

  c3 = END_TSC ( c3 );

  std::cout << "\nDone.\n"
            "Stream    : " << c1 << " cycles.\n"
            "OFStream  : " << c2 << " cycles.\n"
            "Descriptor: " << c3 << " cycles.\n";

  return EXIT_SUCCESS;
}

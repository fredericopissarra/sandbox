#include <unistd.h>
#include <fcntl.h>
#include <vector>
#include <string>

bool writeFileSyscall( const char *path, const std::vector<std::string>& lines )
{
  int fd;

  fd = open ( path, O_CREAT | O_WRONLY | O_TRUNC, 0664 );

  if ( fd < 0 )
    return false;

  for ( auto& i : lines )
  {
    if ( write( fd, i.c_str(), i.size() ) < 0 )
    {
    error:
      close( fd );
      return false;
    }

    if ( write( fd, "\n", 1 ) < 0 )
      goto error;
  }

  close( fd );

  return true;
}

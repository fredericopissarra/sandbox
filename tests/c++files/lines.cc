#include <cstdlib>
#include <vector>
#include <string>

static std::string createRandomString( void )
{
  std::string s;

  for ( int i = 0; i < 120; i++ )
    s += ' ' + rand() % 95;

  return s;
}

std::vector<std::string> createRandomLines( void )
{
  std::vector<std::string> v;

  for ( int i = 0; i < 1000; i++ )
    v.push_back( createRandomString() );

  return v;
}

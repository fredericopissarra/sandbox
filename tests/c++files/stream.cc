#include <cstdio>

#include <vector>
#include <string>

bool writeFileStream( const char *path, const std::vector<std::string>& lines )
{
  FILE *f;

  f = fopen( path, "wt" );
  if ( ! f )
    return false;

  for ( auto& i : lines )
    if ( fprintf( f, "%s\n", i.c_str() ) < 0 )
    {
      fclose( f );
      return false;
    }

  fclose( f );

  return true;
}

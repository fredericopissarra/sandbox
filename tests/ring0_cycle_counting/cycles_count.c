#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/hardirq.h>
#include <linux/preempt.h>

#include <cycle_counting.h>

extern int a[];
extern void f(void);

static int __init cyclecnt_start ( void )
{
  int i;
  unsigned long flags, csum;
  counter_T c;

  printk ( KERN_INFO "Loading Cycle-Counting module...\n" );

  preempt_disable();
  raw_local_irq_save(flags);

  c = BEGIN_TSC();
  f();
  c = END_TSC(c);

  raw_local_irq_restore(flags);
  preempt_enable();

  csum = 0;
  for (i = 0; i < 100; i++)
    csum += a[i];

  printk ( KERN_INFO "\tCSum: %lu, Cycles: %lu\n", csum, c);

  return 0;
}

static void __exit cyclecnt_end ( void )
{
  printk ( KERN_INFO "Goodbye Cycle-Counter.\n" );
}

module_init ( cyclecnt_start );
module_exit ( cyclecnt_end );

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Frederico L. Pissarra");
MODULE_DESCRIPTION("Cycle Counting Module");


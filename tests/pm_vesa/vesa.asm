;█████████████████████████████████████████████████████████████████
;  vesa.asm
;█████████████████████████████████████████████████████████████████

  bits  16
  cpu   386

;▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁
struc VESAinfo
.signature:         resd  1
.version:           resw  1
.oemstrfptr:        resd  1
.capabilities:      resd  1
.videomodelistptr:  resd  1
.totalmem:          resw  1   ; in 64 KiB blocks.

; VBE 3
.oemsoftrev:        resw  1
.oemvendorptr:      resd  1
.oemprodptr:        resd  1
.oemprodrevptr:     resd  1
                    resb  222
.oemdata:           resb  256
endstruc

struc VESAmode
.attr:            resw  1
.win_a:           resb  1
.win_b:           resb  1
.granularity:     resw  1
.winsize          resw  1
.seg_a:           resw  1
.seg_b:           resw  1
.winfuncfptr:     resd  1
.pitch:           resw  1
.width:           resw  1
.height:          resw  1
.w_char:          resb  1
.y_char:          resb  1
.planes:          resb  1
.bitsperpixel:    resb  1
.banks:           resb  1
.mem_model:       resb  1
.banksize:        resb  1
.img_pages:       resb  1
                  resb  1
.red_mask:        resb  1
.red_position:    resb  1
.green_mask:      resb  1
.green_position:  resb  1
.blue_mask:       resb  1
.blue_position:   resb  1
                  resb  2
.directcolorattr: resb  1
.frmbufpaddr:     resd  1   ; (32 bits) Physical addr.
.offscrnmemoff:   resd  1
.offscrnmemsize:  resd  1
                  resb  6

; VBE 3
.linbytesperscanline:   resw  1
.bnknumofimgpages:      resb  1
.linnumofimgpages:      resb  1
.linredmasksize:        resb  1
.linredfieldposition:   resb  1
.lingreenmasksize:      resb  1
.lingreenfieldposition: resb  1
.linbluemasksize:       resb  1
.linbluefieldposition:  resb  1
.linrsvdmasksize:       resb  1
.linrsvdfieldposition:  resb  1
.maxpelclock:           resb  1  ; in Hz.

                  resb  189
endstruc

; 1024x768 RGB888 VESA mode (note: We aren't using CRTinfo on mode setting).
%define HIMODE  0x118
%define USELFB  ( 1 << 14 )
%define HIMODE_LFB  ( HIMODE | USELFB )

; Entry: ES:DI = VESAinfo block,
; Exit: ZF=1 if ok, =0 if fails.
%macro GET_VESA_INFO 0
  mov   ax,0x4f00
  int   0x10
  test  ah,ah
%endmacro

; Entry: ES:DI = VESAmode block, CX = mode
; Exit: ZF=1 if ok, =0 if fails
%macro GET_MODE_INFO 0
  mov   ax,0x4f01
  int   0x10
  test  ah,ah
%endmacro

; Entry: mode
; Exit: ZF=1 if ok, =0 if fails
%macro SET_MODE 1
  mov   bx,%1
  mov   ax,0x4f02
  int   0x10
  test  ah,ah
%endmacro
;▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁

  org   0x7c00

_start:
  ; clearscreen
  mov   ax,3
  int   0x10

  ; Adjust es and ds in real mode.
  mov   ax,cs
  mov   ds,ax
  mov   es,ax
  
  mov   di,heap
  mov   cx,HIMODE_LFB
  GET_MODE_INFO
  jnz   error

  ; Show LFB physical address
  mov   eax,[di+VESAmode.frmbufpaddr]
  mov   di,hexstr
  call  uitohex

  mov   si,lfbaddrmsg
  call  puts
  
;--- todo:
; wait keypress...
; set video mode...
; set i386 protected mode...
; draw

halt:
  hlt
  jmp   halt

error:
  mov   si,msg
  call  puts
  jmp   halt

;▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁
puts:
  lodsb
  test  al,al
  jz    .exit
  mov   bx,7
  mov   ah,0x0e
  int   0x10
  jmp   puts
.exit:
  ret

;▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁
; AL, DS:DI
_byte2hex:
  mov   bl,al
  shr   bl,4
  cmp   bl,9
  jbe   .skip1
  add   bl,7
.skip1:
  add   bl,'0'
  mov   [di],bl

  mov   bl,al
  and   bl,0x0f
  cmp   bl,9
  jbe   .skip2
  add   bl,7
.skip2:
  add   bl,'0'
  mov   [di+1],bl
  ret

; EAX = x, DS:DI = ptr
; Destroy EAX, EBX, ECX and EDI
uitohex:
  mov   ecx,eax   ; Salva EAX em ECX
  shr   eax,24
  call  _byte2hex
  add   di,2

  mov   eax,ecx
  shr   eax,16
  call  _byte2hex
  add   di,2

  mov   al,ch
  call  _byte2hex
  add   di,2

  mov   al,cl
  call  _byte2hex
  ret

;▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁
msg:
  db  `Error getting VESA info.\r\n`,0

lfbaddrmsg:
  db  `Physical 32 bits framebuffer address: 0x`
hexstr:
  times 8 db '0'
  db  `.\r\n`,0

;▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁
  times 510 - ($ - $$) db 0
  dw    0xaa55

;▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁
heap:

#include <stdio.h>
#include <dos.h>

#pragma pack(1)

typedef unsigned int uint16;
typedef unsigned long uint32;

struct vbe_info_structure {
  char   signature[4];  // must be "VESA" to indicate valid VBE support
  uint16 version;       // VBE version; high byte is major version, low byte is minor version
  uint32 oem;           // segment:offset pointer to OEM
  uint32 capabilities;  // bitfield that describes card capabilities
  uint32 video_modes;   // segment:offset pointer to list of supported video modes
  uint16 video_memory;  // amount of video memory in 64KB blocks
  uint16 software_rev;  // software revision
  uint32 vendor;        // segment:offset to card vendor string
  uint32 product_name;  // segment:offset to card model name
  uint32 product_rev;   // segment:offset pointer to product revision
  char reserved[222];   // reserved for future expansion
  char oem_data[256];   // OEM BIOSes store their strings in this area
}

int main( void )
{
  struct vbe_info_structure vi;
  uint16 far *vm;
  union REGS inr, outr;
  struct SREGS sregs;

  sregs.es = FP_SEG( &inr );
  inr.di = FP_OFF( &vi );  
  int86x( 0x10, &inr, &outr, &sregs );

  vm = MK_FP( vi.video_modes >> 16, vi.video_modes );

  while ( *vm != 0xffff )
    printf( "%x\n", *vm++ );
}

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/time.h>

#define GLFW_INCLUDE_GLCOREARB
#include <GL/glew.h>
#include <GLFW/glfw3.h>

#define WINDOW_WIDTH  1280
#define WINDOW_HEIGHT 720
#define FRAME_PERIOD ( 1.0 / 30 )

// Macro usada para atribuir offsets aos Vertex Attribute Pointers.
#define BUFFER_OFFSET(ofs) ((void *)(ofs))

static void   initOpenGL ( void );
static _Bool  loadShaders ( GLuint * );
static void   initObject ( GLuint *, GLuint * );
static double getTime ( void );
static void   draw_( GLFWwindow * );

int main ( void )
{
  GLuint vao, vbo, shaders_program;
  double t1, t = 0.0;   // t será o tempo em segundos (não em ms).
  GLFWwindow *window;
  GLint timeU, resolutionU;

  #ifdef FULLSCREEN
    GLFWmonitor *primary; // for fullscreen.
  #endif

  /* *** INICIALIZAÇÃO do contexto gráfico *** */
  if ( ! glfwInit() )
  {
    fputs ( "ERRO inicializando GLFW3.\n", stderr );
    return EXIT_FAILURE;
  }

  atexit ( glfwTerminate );

  /* Usando o core profile do OpenGL 4.3 */
  glfwWindowHint ( GLFW_CONTEXT_VERSION_MAJOR, 4 );
  glfwWindowHint ( GLFW_CONTEXT_VERSION_MINOR, 1 );
  glfwWindowHint ( GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE );

  /* 30 fps, RGBA8, não é uma janela que possa mudar de tamanho nesse exemplo. */
  glfwWindowHint ( GLFW_REFRESH_RATE, 30 );
  glfwWindowHint ( GLFW_RESIZABLE, 0 );
  glfwWindowHint ( GLFW_SAMPLES, 4 );   // Para Antialiasing.

  #ifdef FULLSCREEN
    primary = glfwGetPrimaryMonitor();
    if ( ! primary )
    {
      fputs( "ERRO obtendo o monitor primário.\n", stderr );
      return EXIT_FAILURE;
    }

    /* Cria a janela para usar como contexto. */
    window = glfwCreateWindow ( WINDOW_WIDTH, WINDOW_HEIGHT, "tris", primary, NULL );
  #else
    window = glfwCreateWindow ( WINDOW_WIDTH, WINDOW_HEIGHT, "tris", NULL, NULL );
  #endif

  if ( ! window )
  {
    fputs ( "ERRO ao criar janela.\n", stderr );
    return EXIT_FAILURE;
  }

  glfwMakeContextCurrent ( window );

  // Agora podemos pedir ao GLEW para fazer _late binding_ com as funções da API do OpenGL
  if ( glewInit() != GLEW_OK )
  {
    fputs ( "ERRO ao inicializar GLEW.\n", stderr );
    return EXIT_FAILURE;
  }

  //glfwSetInputMode ( window, GLFW_CURSOR, GLFW_CURSOR_DISABLED );

  // Agora precisamos inicializar o OpenGL para nosso desenho.
  initOpenGL();

  if ( ! loadShaders ( &shaders_program ) )
  {
    fputs ( "ERRO carregando shader(s).\n", stderr );
    return EXIT_FAILURE;
  }

  initObject ( &vao, &vbo );

  glUseProgram ( shaders_program );

  // Pega as localizações dos objetos dos shaders.
  timeU = glGetUniformLocation ( shaders_program, "time" );
  resolutionU = glGetUniformLocation ( shaders_program, "resolution" );

  // A resolução não muda.
  glUniform2i ( resolutionU, WINDOW_WIDTH, WINDOW_HEIGHT );

  t1 = getTime( );
  while ( ! glfwWindowShouldClose ( window ) )
  {
    double t2, delta;

    t2 = getTime( );
    delta = t2 - t1;

    if ( delta >= FRAME_PERIOD )
    {
      // Seta o tempo atual em segundos para o objeto 'time', dos shaders.
      glUniform1f ( timeU, t );

      draw_ ( window );

      t1 = t2;
      t += delta;
    }

    glfwPollEvents();

    if ( glfwGetKey ( window, GLFW_KEY_ESCAPE ) == GLFW_PRESS )
      break;
  }

  glfwDestroyWindow ( window );

  return EXIT_SUCCESS;
}

// Initializa OpenGL.
static void initOpenGL ( void )
{
  // Habilita antialiasing de polígonos...
  glHint ( GL_POLYGON_SMOOTH, GL_NICEST );

  glViewport ( 0, 0, WINDOW_WIDTH, WINDOW_HEIGHT );

  glEnable( GL_FRAMEBUFFER_SRGB );  // Gamma Correction (usa sRGB).
  glDisable( GL_DITHER );

  // Demais estados são "default".
}

// Usada por loadShaders().
static char *loadFile_ ( const char *path )
{
  int fd;
  struct stat st;
  char *p;

  fd = open ( path, O_RDONLY );

  if ( fd < 0 )
    return NULL;

  if ( fstat ( fd, &st ) )
  {
    close ( fd );
    return NULL;
  }

  p = malloc ( st.st_size + 1 );

  if ( p )
  {
    off_t r = st.st_size;
    char *q = p;

    do
    {
      ssize_t sz;

      // Só por precaução no acso de usarmos sinais.
      do
      {
        errno = 0;
        sz = read ( fd, q, r );
      } while ( errno == EINTR );

      if ( sz < 0 )
      {
        free ( p );

        close ( fd );

        return NULL;
      }

      r -= sz;
      q += sz;
    } while ( r );

    *q = '\0';
  }

  close ( fd );

  return p;
}

// Usada por loadShaders().
static _Bool createAndCompileShader_ ( GLuint *name, GLenum shader, char *code )
{
  int status, len;

  *name = glCreateShader ( shader );
  glShaderSource ( *name, 1, ( const char ** ) &code, NULL );

  // Uma vez carregado o fonte, não precisamos mais dele.
  free ( code );

  glCompileShader ( *name );

  glGetShaderiv ( *name, GL_COMPILE_STATUS, &status );

  if ( ! status )
  {
    char log[2048];
    static const char *shdr[] = { "???", "VERTEX", "FRAGMENT" };
    const char *shdrp;

    glGetShaderInfoLog ( *name, sizeof log - 1, &len, log );
    log[len] = '\0';

    glDeleteShader ( *name );

    switch ( shader )
    {
      case GL_VERTEX_SHADER:
        shdrp = shdr[1];
        break;

      case GL_FRAGMENT_SHADER:
        shdrp = shdr[2];
        break;

      default:
        shdrp = shdr[0];
    }

    fprintf ( stderr, "Error compiling %s shader:\n\t%s\n", shdrp, log );

    return 0;
  }

  return 1;
}

static _Bool loadShaders ( GLuint *program )
{
  char *code;
  GLuint vs, fs;
  int status;

  // vertex shader.
  code = loadFile_ ( "./simple.vert" );

  if ( ! code )
  {
    fputs ( "Erro carregando Vertex Shader.\n", stderr );
    return 0;
  }

  if ( ! createAndCompileShader_ ( &vs, GL_VERTEX_SHADER, code ) )
    return 0;

  // fragment shader.
  code = loadFile_ ( "./main.frag" );

  if ( ! code )
  {
    fputs ( "Erro carregando Fragment Shader.\n", stderr );
    return 0;
  }

  if ( ! createAndCompileShader_ ( &fs, GL_FRAGMENT_SHADER, code ) )
    return 0;

  /* Linka o programa. */
  *program = glCreateProgram();
  glAttachShader ( *program, vs );
  glAttachShader ( *program, fs );

  glLinkProgram ( *program );

  /* Não precisamos mais dos shaders, já que já temos o programa
     (se não houve erro de linkagem). */
  glDeleteShader ( vs );
  glDeleteShader ( fs );

  glGetProgramiv ( *program, GL_LINK_STATUS, &status );

  if ( ! status )
  {
    char log[2048];   // FIXME: Suficiente?
    int len;

    glGetProgramInfoLog ( vs, sizeof ( log ) - 1, &len, log );
    log[len] = '\0';

    fprintf ( stderr, "Error linking shaders program:\n\t%s\n", log );

    return 0;
  }

  return 1;
}

double getTime ( void )
{
  struct timeval tv;

  gettimeofday ( &tv, NULL );

  return tv.tv_sec + tv.tv_usec / 1000000.0;
}

static void initObject ( GLuint *vao, GLuint *vbo )
{
  struct vertices { float x, y; };

  // Um triangle fan para definir um retângulo na tela...
  static const struct vertices vertices[] =
  {
    { -1.0f,  1.0f },
    { -1.0f, -1.0f },
    {  1.0f, -1.0f },
    {  1.0f,  1.0f }
  };

  glGenVertexArrays ( 1, vao );
  glBindVertexArray ( *vao );

  /* Cria 2 buffers objects. Um para conter os vértices e outro para as cores dos vértices. */
  glGenBuffers ( 1, vbo );

  /* Carrega o vbo com os vertices e associa os atributos.
     GL_STATIC_DRAW porque o buffer não será modificado nunca.
     É a dica pro OpenGL passar o buffer IMEDIATAMENTE para a GPU. */
  glBindBuffer ( GL_ARRAY_BUFFER, *vbo );
  glBufferData ( GL_ARRAY_BUFFER, sizeof vertices, vertices, GL_STATIC_DRAW );

  /* Informa os offsets dos atributos dos vértices.
     Note que o stride precisa ser informado. */
  glVertexAttribPointer ( 0, 2, GL_FLOAT, GL_FALSE, sizeof vertices[0], BUFFER_OFFSET ( 0 ) );
  glEnableVertexAttribArray ( 0 );

  /* NOTE: Neste exemplo, ao sair, o VAO continuará "bounded". */
}

void draw_ ( GLFWwindow *window )
{
  glClear ( GL_COLOR_BUFFER_BIT /* | GL_DEPTH_BUFFER_BIT */ );

  /* NOTE: VAO e VBO já estão "bounded", neste ponto. */
  glDrawArrays ( GL_TRIANGLE_FAN, 0, 4 ); /* count é o número de vertices, não o de primitivos!
                                             GL_TRIANGLE_FAN só indica que o OpenGL ira desenhar
                                             triangulos! */

  glFlush();
  glfwSwapBuffers ( window );
}

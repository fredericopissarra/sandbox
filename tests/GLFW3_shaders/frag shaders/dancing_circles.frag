/* simple.frag */
#version 410

uniform float time; // tempo em ms.
uniform ivec2 resolution;

out vec4 fragColor;

// Created by Siegfried Nowhere - s9art.com - @siegfried.artworks
// License Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License.

#define TWO_PI              6.28318530718
#define r_(A)               mat2(cos(A), -sin(A), sin(A), cos(A))

//https://iquilezles.org/articles/palettes/

vec3 palette ( vec2 uv, float i )
{
  float t = length ( uv ) + i * 0.5 - time * 0.5;

  vec3 a = vec3 ( 0.468, 0.358, 0.5 );
  vec3 b = vec3 ( 0.448, 0.218, 0.388 );
  vec3 c = vec3 ( 1.0, 1.0, 1.0 );
  vec3 d = vec3 ( 0.002, 0.168, 0.498 );

  return a + b * cos ( TWO_PI * ( c * t + d ) );
}

float circle ( vec2 uv, vec2 center, float r )
{
  float brightness = 0.008;
  float l = length ( uv + center );

  return brightness / abs ( sin ( l - r ) );
}

float shape ( vec2 uv, float size )
{
  float mask = 0.0;
  float time = time * 0.03;

  const float sides = 7.0;

  for ( float i = 0.0; i < sides; i++ )
  {
    uv *= r_ ( TWO_PI / sides );
    vec2 uvOrig = uv;

    mask += circle ( uv, vec2 ( uv.x, uv.y + size * 1.0 ), size * 1.0 );
    uv = - ( uvOrig * r_ ( -2.65 * time ) );
  }

  return mask;
}

void main ( void )
{
  vec2 uv = ( gl_FragCoord.xy * 2.0 - resolution.xy ) / min ( resolution.x, resolution.y );
  vec3 color = shape ( uv, 0.9 ) * palette ( uv, 0.0 );

  fragColor = vec4 ( color, 1.0 );
}


// simple.vert
#version 410

layout (location = 0) in vec2 vrtx;

void main()
{
  gl_Position = vec4(vrtx, 0.0, 1.0);
}

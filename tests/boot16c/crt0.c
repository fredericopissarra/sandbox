/* C Runtime init */

/* definidas pelo linker. */
extern char _bss_begin;
extern char _bss_end;

/* Inicializa a section .bss, se houver. */
void _init( void )
{
  void *p, *q;
  unsigned int size;    // 32 bits!

  /* Por enquanto só inicializa o .bss */
  p = &_bss_begin;
  q = &_bss_end;
  if ( size = q - p )
    __asm__ __volatile__ (
      "rep; stosb" : : "a" (0), "D" (p), "c" (size) : "memory"
    );
}

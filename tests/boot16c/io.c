#include <io.h>

void setupvideo( void )
{ __asm__ __volatile__( "int $0x10" : : "a" ((short int)3) ); }

__attribute__((regparm(1),noinline))
void putch( char c )
{
  __asm__ __volatile__ (
    "movb $0x0e,%%ah\n\t"
    "movw $7,%%bx\n\t"
    "int $0x10"
    : : "a" (c) : "ebx" );
}

__attribute__((regparm(1),noinline))
void putstr( char *p )
{
  while ( *p ) putch( *p++ );
}

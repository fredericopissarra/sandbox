#ifndef IO_H_INCLUDED__
#define IO_H_INCLUDED__


void setupvideo( void );
__attribute__((regparm(1))) void putch( char );
__attribute__((regparm(1))) void putstr( char * );

#endif

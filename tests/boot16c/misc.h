#ifndef MISC_H_INCLUDED_
#define MISC_H_INCLUDED_

// funções auxiliares.

static inline unsigned short rd_cs( void )
{ unsigned short seg;
  __asm__ __volatile__ ( "movw %%cs,%0" : "=a" (seg) );
  return seg; }

static inline unsigned short rd_ds( void )
{ unsigned short seg;
  __asm__ __volatile__ ( "movw %%ds,%0" : "=a" (seg) );
  return seg; }

static inline unsigned short rd_es( void )
{ unsigned short seg;
  __asm__ __volatile__ ( "movw %%es,%0" : "=a" (seg) );
  return seg; }

static inline unsigned short rd_ss( void )
{ unsigned short seg;
  __asm__ __volatile__ ( "movw %%ss,%0" : "=a" (seg) );
  return seg; }

static inline void wr_ds( short int selector )
{ __asm__ __volatile__ ( "movw %w0,%%ds" : : "r" (selector) ); }

static inline void wr_es( short int selector )
{ __asm__ __volatile__ ( "movw %w0,%%es" : : "r" (selector) ); }

static inline void wr_fs( short int selector )
{ __asm__ __volatile__ ( "movw %w0,%%fs" : : "r" (selector) ); }

static inline void wr_gs( short int selector )
{ __asm__ __volatile__ ( "movw %w0,%%gs" : : "r" (selector) ); }

static inline void outb( short int port, unsigned char data )
{ __asm__ __volatile__ ( "outb %b1,%0" : : "dN" (port), "a" (data) ); }

static inline void outw( short int port, unsigned short data )
{ __asm__ __volatile__ ( "outw %w1,%0" : : "dN" (port), "a" (data) ); }

static inline void outl( short int port, unsigned int data )
{ __asm__ __volatile__ ( "outl %1,%0" : : "dN" (port), "a" (data) ); }

static inline unsigned char inb( short int port )
{ unsigned char data;
  __asm__ __volatile__ ( "inb %1,%b0" : "=a" (data) : "dN" (port) );
  return data; }

static inline unsigned short inw( short int port )
{ unsigned short data;
  __asm__ __volatile__ ( "inw %1,%w0" : "=a" (data) : "dN" (port) );
  return data; }

static inline unsigned int inl( short int port )
{ unsigned int data;
  __asm__ __volatile__ ( "inl %1,%0" : "=a" (data) : "dN" (port) );
  return data; }

#endif

#include <io.h>

/* Definidas no stage0.asm */
extern char signature[];

/* Nosso main() não retorna valores e não toma argumentos!
   Ainda, usei _main() pq main(), como é espercial, costuma ter algumas
   preparações por parte do compilador. */
void _main( void )
{
  setupvideo();
  putstr( signature );
  putstr( "\r\n\r\nStage 0...\r\nSystem Halted.\r\n" );
}


  bits  16
  cpu   386

  ; Mesmo que no modo de 16 bits não tenhamos "proteção"
  ; coloco esses símbolos e valores numa sessão read-only
  ; por conveniência e padronização.
  section .rodata

  global drive
  global stage1_numsectors
  global stage1_cylinder
  global stage1_head
  global stage1_sector

drive:             db  0x80
stage1_numsectors: db  1     ; 1 setor extra porque a ext3fs reserva apenas 1 KiB.

%ifndef USE_LBA
; Se CHS for usado, usa esses argumentos.
stage1_cylinder:  dw  0     ; apenas 10 bits aqui.
stage1_head:      db  0     ; apenas 8 bits aqui.
stage1_sector:    db  1     ; apenas 6 bits aqui.
%else
stage1_lba:       dq  1
%endif

  ; Usando uma section diferente só porque 'start' tem que estar
  ; no começo do código.
  section .text16

  extern _init
  extern _main

  global _start
_start:
  jmp   continue

  global  signature
signature:         db  'MyToyOS 1.0',0, ; 12 bytes!

  align 2
continue:
  cli                     ; Desabilita tratamento de interrupções.
  cld                     ; Para ter certeza da direção de scans, fills e cópias.
                          ; via rep scas/stos/movs.

  mov   ax,cs
  mov   ds,ax
  mov   es,ax
  
  mov   [drive],dl      ; DL vêm da Int 0x19.

  ; FIXME: Ajustar a pilha aqui, se necessário!

  call  dword _init      ; Precisa chamar antes para iniciar a section .bss
  call  dword _main

  ; Não precisamos, ainda, de um destrutor.
  ;call _dtor

.halt:
  hlt
  jmp   .halt

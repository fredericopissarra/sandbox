#include <stdio.h>

static void (*ptr)(void) = NULL;

void f(void) { puts( "hello" ); }

void g(void) { ptr = f; }

int main( void )
{
  ptr();
}

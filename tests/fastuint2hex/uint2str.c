#include <stdint.h>

// OBS: lowerAlpha é um flag para determinar se usamos 'a'..'f'
//      ou 'A'..'F'.
void UIntToHexString ( uint32_t num, char *s, _Bool lowerAlpha )
{
  uint64_t *p;
  uint64_t x;
  uint64_t mask;

  x = num;
  x = ( ( x & 0xFFFF ) << 32 ) | ( ( x >> 16 ) & 0xFFFF );
  x = ( ( x & 0x0000FF000000FF00ULL ) >> 8 ) | ( x & 0x000000FF000000FFULL ) << 16;
  x = ( ( x & 0x00F000F000F000F0ULL ) >> 4 ) | ( x & 0x000F000F000F000FULL ) << 8;

  mask = ( ( x + 0x0606060606060606ULL ) >> 4 ) & 0x0101010101010101ULL;

  x |= 0x3030303030303030ULL;

  // FIXME: Podemos nos livrar dessa multiplicação se 'lowerAlpha' não for usado!
  x += ( lowerAlpha ? 0x27 : 0x07 ) * mask;

  p = ( uint64_t * ) s;
  *p++ = x;

  *(char *)p = '\0';
}

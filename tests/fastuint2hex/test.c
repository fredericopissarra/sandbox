#include <stdio.h>
#include <stdint.h>

void UIntToHexString ( uint32_t, char *, int );

int main ( void )
{
  char s[9];

  // Usando 'uppercase' aqui.
  UIntToHexString ( 0x1234face, s, 0 );
  puts ( s );

  // Usando 'lowercase' aqui.
  UIntToHexString ( 0xdead, s, 1 );
  puts ( s );
}

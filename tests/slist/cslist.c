// Circular single linked list test.

#include <stdio.h>
#include <stdlib.h>

// Macro to initialize a list.
#define CSLIST_INIT(list__) { &(list__) }

// base structure of a single linked circular list.
struct cslist_head {
  struct cslist_head *next_;
};

// Add a node after an element in the list.
void cslist_add_next( struct cslist_head *node, struct cslist_head *prev )
{
  node->next_ = prev->next_;
  prev->next_ = node;
}

// Deletes a node after an element in the list.
struct cslist_head *cslist_del_next( struct cslist_head *prev )
{
  struct cslist_head *node;

  node = prev->next_;
  prev->next_ = node->next_;

  return node;
}

// Macro to iterate to the list.
#define cslist_foreach( iter__, head__ ) \
  for ( iter__ = (head__)->next_; iter__ != head__; iter__ = (iter__)->next_ )

// Simple test.
int main( void )
{
  // Derived from base structure...
  struct mynode {
    struct cslist_head *next_;    // must be here!

    // data.
    int x;
  };

  // Empty list.
  struct cslist_head *t, list = CSLIST_INIT(list);

  struct mynode *node;

  node = malloc( sizeof *node );
  node->x = 1;
  cslist_add_next( (struct cslist_head *)node, &list );
  t = ( struct cslist_head *)node;    // keep pointer to tail.

  node = malloc( sizeof *node );
  node->x = 2;
  cslist_add_next( (struct cslist_head *)node, t );
  t = ( struct cslist_head *)node;    // keep pointer to tail.

  node = malloc( sizeof *node );
  node->x = 3;
  cslist_add_next( (struct cslist_head *)node, t );

  // List all nodes.
  cslist_foreach( t, &list )
  {
    node = ( struct mynode *)t;

    printf( "%d\n", node->x );
  }  

  // while the list still have nodes, free them.
  while ( list.next_ != &list )
  {
    t = cslist_del_next( &list );
    free( t );
  }
}

#include <stddef.h>

// Numa lista vazia ambos os ponteiros são NULL.
#define SLIST_INIT { NULL, NULL }

// "Classe" base para um nó.
struct node {
  struct node *next;
  // Nós "derivados" precisam ter esse ponteiro
  // como primeiro elemento da estrutura.
};

struct slist {
  struct node *head, *tail;
};

void slist_init( struct slist *slstp )
{
  *slstp = ( struct slist )SLIST_INIT;
}

// Lista PODE estar vazia.
void slist_insertlast( struct slist *slstp, void *nodep )
{
  struct node *p;

  if ( ! slstp->tail )
    slstp->head = nodep;
  else
    slstp->tail->next = nodep;

  p = nodep;
  p->next = NULL;
  slstp->tail = nodep;
}

// Assume lista não vazia.
void slist_insertafter( struct slist *slstp, void *fromp, void *nodep )
{
  struct node *nextp, *p, *q;

  p = fromp;
  q = nodep;

  nextp = p->next;
  q->next = nextp;
  p->next = q;
  
  if ( slstp->tail == fromp )
    slstp->tail = nodep;
}

// Assume que nodep está na lista e que a lista não está vazia.
void *slist_deleteafter( struct slist *slstp, void *nodep )
{
  struct node *nextp, *p, *q;

  p = nodep;
  if ( slstp->tail == nodep )
    return NULL;

  nextp = p->next;
  p->next = nextp->next;

  if ( slstp->tail == nextp )
    slstp->tail = nodep;

  return nextp;
}

// Percorre toda a lista (não vazia) executando a função func em cada nó.
// Pára se percorreu a lista toda ou se func() retornar 0.
int slist_foreach( struct slist *slstp, int (*func)( struct slist *, void * ) )
{
  struct node *iterp;
   
  iterp = slstp->head;
  while ( iterp )
  {
    if ( ! func( slstp, iterp ) )
      return 0;

    iterp = iterp->next;
  }

  return 1;
}

// Destrói uma lista, chamando um destrutor para cada nó.
// Termina com uma lista vazia.
void slist_destroy( struct slist *slstp, void dtor( void * ) )
{
  if ( slstp->head )
  {
    struct node *deletedp;

    while ( slstp->head != slstp->tail )
      if ( deletedp = slist_deleteafter( slstp, slstp->head ) )
        dtor( deletedp );

    dtor( slstp->head );

    slstp->head = slstp->tail = NULL;
  }
}

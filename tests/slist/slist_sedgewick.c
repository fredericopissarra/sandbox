#include <stddef.h>

// Nossos nós são derivados desse.
struct node {
  struct node *next;
};

struct slist {
  struct node head, tail;   // OBS: Não são ponteiros!
                            //      São 'sentinelas' fixos.
};

// O macro para inicialização direta não é prático
// aqui.

// Precisa ser chamada para inicializar os batentes!
void slist_init( struct slist *lstp )
{
  lstp->head.next = lstp->tail.next = &lstp->tail;
}

void slist_insertlast( struct slist *slstp, void *nodep )
{
  struct node *p, *q;

  p = nodep;

  // Procura pelo último nó antes do batente 'tail'.
  // FIX: Poderíamos manter um ponteiro 'realtailp'
  //      na estrutura slist para evitar esse loop.
  //      Mas, a ideia aqui é a simplicidade!
  q = &slstp->head;
  while ( q->next != &slstp->tail )
    q = q->next;
  
  p->next = q->next;
  q->next = nodep;
}

// Obs: 'fromp' não pode ser o nó batente 'tail'!
void slist_insertafter( struct slist *slstp, void *fromp, void *newp )
{
  struct node *p, *q;

  p = newp;
  q = fromp;

  p->next = q->next;
  q->next = newp;
}

void *slist_deleteafter( struct slist *slstp, void *nodep )
{
  struct node *p, *q;

  p = nodep;

  // "Desatacha" o próximo nó da lista.
  q = p->next;
  p->next = p->next->next;

  // Se o nó "desatachado" é o sentinela do fim
  // da lista, sai com NULL.
  if ( q == &slstp->tail )
    return NULL;

  return q;
}

int slist_foreach( struct slist *slstp, int (*action)( struct slist *, void * ) )
{
  struct node *p;

  for ( p = slstp->head.next; p != &slstp->tail; p = p->next )
    if ( ! action( slstp, p ) )
      return 0;

  return 1;
}

void slist_destroy( struct slist *slstp, void dtor( void * ) )
{
  struct node *p;

  while ( p = slist_deleteafter( slstp, &slstp->head ) )
    dtor( p );

  // Só sobram os sentinelas aqui.
}

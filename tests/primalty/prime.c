/* prime.c */
#include <stdio.h>

static _Bool prime( unsigned int );

// test.
int main( void )
{
  unsigned int n;

  for ( n = 0; n < 1000; n++ )
  {
    if ( ( ( n + 1 ) % 32 ) == 0 ) putchar( '\n' );

    printf( "\033[1;%dm%u\033[m ",
            prime( n ) ? 32 : 31, n );
  }

  putchar( '\n' );
}

// Interesting approach from "Sir. Galahad" (C Board):
__attribute__((noinline))
_Bool prime( unsigned int n )
{
  unsigned int d;
  unsigned long long int f;

  if ( n <= 3 )
    return n >= 2;

  d = n % 6;
  if ( d != 1 && d != 5 )
    return 0;

  for ( f = 5; f * f <= n; f += 4 )
  {
    if ( ( n % f ) == 0 )
      return 0;

    f += 2;

    if ( ( n % f ) == 0 )
      return 0;
  }

  return 1;
}

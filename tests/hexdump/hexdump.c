#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <ctype.h>
#include <stdint.h>
#include <sys/stat.h>

#define BYTES_PER_LINE 16

static void hexdump ( const void *, size_t );

static void closefile( int *fd ) { if ( *fd >= 0 ) close( *fd ); }
static void freebuffer( unsigned char **p ) { free( *p ); }

// teste
int main ( int argc, char *argv[] )
{
  int fd __attribute__((cleanup(closefile))) = -1;
  struct stat st;
  ssize_t sz;
  unsigned char *p __attribute__((cleanup(freebuffer))) = NULL;

  if ( argc != 2 )
  {
    fprintf( stderr, "Usage: %s <file>\n", argv[0] );
    return EXIT_FAILURE;
  }

  if ( stat( argv[1], &st ) )
  {
    fprintf( stderr, "ERROR: Cannot stat '%s'.\n", argv[1] );
    return EXIT_FAILURE;
  }

  fd = open( argv[1], O_RDONLY );
  if ( fd < 0 )
  {
    fprintf( stderr, "ERROR: Cannot open '%s'.\n", argv[1] );
    return EXIT_FAILURE;
  }

  p = malloc( st.st_size );
  if ( ! p )
  {
    fprintf( stderr, "ERROR: Cannot allocate %zu bytes buffer.\n", st.st_size );
    return EXIT_FAILURE;
  }

  sz = read( fd, p, st.st_size );

  if ( sz < 0 )
  {
    fputs( "ERROR: reading file.\n", stderr );
    return EXIT_FAILURE;
  }

  // Vejamos o conteúdo!
  hexdump ( p, st.st_size );

  return EXIT_SUCCESS;
}

static void showhex ( const unsigned char *p, unsigned int size )
{
  // Calcula quantos espaços vazios temos na linha.
  unsigned int padding = BYTES_PER_LINE - size;

  while ( size-- )
    printf( "%02x ", *p++ );

  while ( padding-- )
    fputs ( "   ", stdout );
}

static void showascii ( const unsigned char *p, unsigned int size )
{
  // Calcula quantos espaços vazios temos na linha.
  unsigned int padding = BYTES_PER_LINE - size;

  putchar ( '|' );

  while ( size-- )
  {
    putchar ( isprint ( *p ) && ! isspace ( *p ) ? *p : '.' );
    p++;
  }

  while ( padding-- )
    putchar ( ' ' );

  puts ( "|" );
}

void hexdump ( const void *p, size_t size )
{
  const void *endp = p + size;
  const void *q;

  q = p;

  while ( q < endp )
  {
    ptrdiff_t sz;

    printf ( "%016tx: ", q - p );

    sz = endp - q;

    if ( sz >= BYTES_PER_LINE )
      sz = BYTES_PER_LINE;

    showhex ( q, sz );
    showascii ( q, sz );

    q += BYTES_PER_LINE;
  }
}

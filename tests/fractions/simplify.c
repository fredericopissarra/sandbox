#include <stdio.h>
#include <stdlib.h>

// Constraints: a and b must be positive integers.
static int gcd ( int a, int b )
{
  int tmp;

  while ( a )
  {
    tmp = b % a;
    b = a;
    a = tmp;
  }

  return b;
}

int main ( void )
{
  int n1, n2, _gcd;
  int num, denom;

  fputs ( "Enter a fraction (N1/N2): ", stdout );
  fflush( stdout );
  if ( scanf ( "%d/%d", &n1, &n2 ) != 2 )
  {
    fputs( "Invalid inputs!\n", stderr );
    return EXIT_FAILURE;
  }

  _gcd = gcd( n1, n2 );
  num = n1 / _gcd;
  denom = n2 / _gcd;

  printf ( "Result: %d/%d\n", num, denom );

  return EXIT_SUCCESS;
}

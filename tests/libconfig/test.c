#include <stdio.h>
#include <stdlib.h>
#include <libconfig.h>

static void destroy_cfg( config_t *cfgp )
{
  config_destroy( cfgp );
}

int main( void )
{
  int num;
  const char *str;

  config_t cfg __attribute__((cleanup(destroy_cfg)));

  config_init( &cfg );
  if ( ! config_read_file( &cfg, "./test.conf" ) )
  {
    fputs( "ERROR reading configuration file. Aborted.\n", stderr );
    return EXIT_FAILURE;
  }

  if ( ! config_lookup_int( &cfg, "xpto_num", &num ) )
  {
    fputs( "ERROR reading xpto_num config.\n", stderr );
    return EXIT_FAILURE;
  }

  if ( ! config_lookup_string( &cfg, "xpto_str", &str ) )
  {
    fputs( "ERROR reading xpto_str config.\n", stderr );
    return EXIT_FAILURE;
  }

  printf( "xpto_num=%d, xpto_str=\"%s\"\n", num, str );
}


#include <stdio.h>
#include <inttypes.h>
#include <cycle_counting.h>

#define swap(a,b) { __typeof(a) t = (a); (a) = (b); (b) = t; }

__attribute__((noinline))
unsigned int gcd_orig( unsigned int u, unsigned int v )
{
  if ( u < v ) swap( u, v );

  if ( ! v ) return u;

  return gcd_orig( v, u % v );
}

// Nota: Para unsigned ints apenas...
__attribute__((noinline))
unsigned int gcd_bin( unsigned int u, unsigned int v )
{
  int shift;

  if ( ! u )
    return v;

  if ( ! v )
    return u;

  // Conta a quantidade mínima de bits para deslocar
  // de volta, uma vez que precisamos obter o MAIOR divisor
  // comum (note o OR).
  shift = __builtin_ctz( u | v );

  // Joga fora os bits inferiores zerados.
  u >>= __builtin_ctz( u );

  do
  {
    // Joga fora os bits inferiores zerados do outro operando...
    v >>= __builtin_ctz( v );

    // v precisa ser maior que u.
    if ( v < u )
      swap( u, v );

    v -= u;
  } while ( v );

  // Recupera os bits zerados perdidos.
  return u << shift;
}

// test
int main( void )
{
  unsigned int g1, g2;
  counter_T c1, c2;

  c1 = BEGIN_TSC();
  g1 = gcd_orig( 12*3255, 6*3255 );
  c1 = END_TSC( c1 );

  c2 = BEGIN_TSC();
  g2 = gcd_bin( 12*3255, 6*3255 );
  c2 = END_TSC( c2 );

  printf( "g1=%u (%" PRIu64 " cycles)\n"
          "g2=%u (%" PRIu64 " cycles)\n",
          g1, c1, g2, c2 );
}

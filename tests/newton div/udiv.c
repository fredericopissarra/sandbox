#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#include <signal.h>

// The double precision ISO/IEC-60554 floating point structure.
union dbl_u
{
  double d;

  struct
  {
    uint64_t f: 52;
    int e: 11;
    int s: 1;
  } __attribute__ ( ( packed ) );
};

// Reciprocal of b approximation in floating point.
static double recip_ ( unsigned int b )
{
  // Expoent e = -log2(b)
  union dbl_u d = { .e = 1023 - 8 * sizeof b + __builtin_clz ( b ) };
  
  // Returns 2^(-log2(b))
  return d.d;
}

// Unsigned integer division by 2 32 bits values.
// Not the actual DIV instruction.
unsigned int udiv_ ( unsigned int a, unsigned int b )
{
  double x;

  // Exception in case of division by zero.
  // This is not the actual exception (Division By Zero).
  if ( ! b )
  {
    raise ( SIGFPE );
    return a;   // we need to return something!
  }

  x = recip_ ( b );

  // 4 Newton-Raphson root approximation method iterations.
  // where f(x)=1/x-b (for x=1/b).
  //
  // We have: x_{n+1} = x_{n}-f(x_n)/f'(x_n):
  //
  //    x_{n+1} = x_n * (2 - x_n * b).
  //
  x = x * ( 2.0 - x * b );
  x = x * ( 2.0 - x * b );
  x = x * ( 2.0 - x * b );
  x = x * ( 2.0 - x * b );    // there are clever ways to avoid this
                              // last approximation.

  return a * x;
}

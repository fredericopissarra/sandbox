; INOUT.ASM
; Para enviar/ler DWORDs para o espaço de I/O.
  .model small

  .386
  .code

  ;
  ; Those functions are exported.
  ;
  public _inpd
  public _outpd

  ;
  ; Stack frames for C functions.
  ;
inpdstk struc
        dw  ?
inport  dw  ?
inpdstk ends

outpdstk  struc
          dw  ?
outport   dw  ?
data      dd  ?
outpdstk  ends

  ;
  ; unsigned long inpd(unsigned short inport);
  ;
_inpd     proc  near
  mov   dx,[esp].inport
  in    eax,dx
  ; Coloca EAX em DX:AX
  mov   edx,eax
  shr   edx,16
  ret
_inpd     endp

  ;
  ; void outpd(unsigned short outport, unsigned long data);
  ;
_outpd    proc  near
  mov   dx,[esp].outport
  mov   eax,[esp].data
  out   dx,eax
  ret
_outpd    endp

  end

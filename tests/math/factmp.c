// Calcula fatorial usando aritmética de multipla precisão.
//
// Compilar com:
//    cc -O2 -o factmp factmp.c -lgmp
//
// Não esquecer de instalar libgmp-dev.
//
#include <stdio.h>
#include <stdlib.h>
#include <gmp.h>
#include <errno.h>
#include <time.h>

int main(int argc, char *argv[])
{
  char *p;
  mpz_t r;
  unsigned long n, c, total;
  time_t t1, t2;

  if ( argc != 2 )
  {
    fprintf( stderr, "Usage: %s <value>\n", argv[0] );
    return EXIT_FAILURE;
  }

  // O valor deve ser inteiro e dentro da faixa de unsigned long int.
  errno = 0;
  n = strtoul( *++argv, &p, 10 );
  if ( errno == ERANGE || *p )
  {
    fputs( "ERROR: Invalid value.\n", stderr );
    return EXIT_FAILURE;
  }

  // Inicializa r e seta-o com 1.
  mpz_init_set_ui(r, 1);

  t1 = time( NULL );

  c = 0;
  total = n;
  while ( n >= 1 )
  {
    mpz_mul_ui( r, r, n-- );
    printf( "%.2f%% complete... \r", 100.0 * ++c / total );
  }
  putchar( '\n' );

  t2 = time( NULL );

  mpz_out_str( stdout, 10, r );
  putchar('\n');

  // Joga r no lixo.
  mpz_clear(r);

  printf( "Total time: %lu seconds.\n", t2 - t1 );

  return EXIT_SUCCESS;
}

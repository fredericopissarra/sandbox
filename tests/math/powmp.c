// Compilar com:
//    cc -O2 -o powmp powmp.c -lgmp
//
// Não esquecer de instalar libgmp-dev.
//
#include <stdio.h>
#include <stdlib.h>
#include <gmp.h>

int main(int argc, char *argv[])
{
  mpz_t r, b;
  unsigned int e;

  if (argc != 3)
  {
    fputs("Usage: powmp 'base' 'expoent'\n", stderr);
    return 1;
  }

  mpz_init_set_ui(r, 1);             // resultado.
  mpz_init_set_str(b, argv[1], 10);  // base

  e = strtoul(argv[2], NULL, 10);
  mpz_pow_ui(r, b, e);

  // Libera a memória usada pela base e expoente.
  mpz_clear(b);

  gmp_printf("%Zd\n", r);

  // Libera a memória usada pelo resultado.
  mpz_clear(r);

  return 0;
}

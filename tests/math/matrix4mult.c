#include <x86intrin.h>

void mmul_scalar( float *r, const float *a, const float *b )
{
  int i, j;

  for ( i = 0; i < 16; i += 4 )
    for ( j = 0; j < 4; j++ )
      r[i + j] = a[i] * b[j] +
                 a[i + 1] * b[j + 4] +
                 a[i + 2] * b[j + 8] +
                 a[i + 3] * b[j + 12];
}

void mmul_sse( float *r, const float *a, const float *b )
{
  int i, j;

  __m128 a_line, b_line, r_line;

  for ( i = 0; i < 16; i += 4 )
  {
    // unroll the first step of the loop to avoid having to initialize r_line to zero
    a_line = _mm_load_ps( a );              // a_line = vec4(column(a, 0))
    b_line = _mm_set1_ps( b[i] );           // b_line = vec4(b[i][0])
    r_line = _mm_mul_ps( a_line, b_line );  // r_line = a_line * b_line

    for ( j = 1; j < 4; j++ )
    {
      a_line = _mm_load_ps( &a[j * 4] );  // a_line = vec4(column(a, j))
      b_line = _mm_set1_ps( b[i + j] );   // b_line = vec4(b[i][j])

      // r_line += a_line * b_line
      r_line = _mm_add_ps( _mm_mul_ps( a_line, b_line ), r_line );
    }

    _mm_store_ps( &r[i], r_line );   // r[i] = r_line
  }
}

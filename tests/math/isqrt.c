#include <signal.h>

// Returns the integer square root of an integer value.
//
// Not that critical in terms of stack usage.
// It will use 16-17 levels of recursive calling only, if x is big...
//
// Will raise a signal (SIGFPE) if x is negative.
int isqrt( int x )
{
  int sc, lc;

  if ( x < 0 )
    raise(SIGFPE);

  if ( x >= 2 )
  {
    // Interesting algorithm!
    sc = isqrt( x >> 2) << 1;
    lc = sc + 1;

    if ( ( lc * lc ) > x )
      return sc;

    return lc;
  }

  return 0;
}

#include <stddef.h>
#include <stdio.h>

void main ( void )
{
  struct s
  {
    int i;        // 0
    char c;       // 4
    double d;     // 5
    char a[];     // 13
  } __attribute__((packed));

  struct t
  {
    int i;   
    char c;  
    double d;
    char a[];
  };

  printf ( "offsets: i=%zu; c=%zu; d=%zu a=%zu\n"
           "sizeof ( struct s ) = %zu\n",
           offsetof ( struct s, i ), 
           offsetof ( struct s, c ),
           offsetof ( struct s, d ), 
           offsetof ( struct s, a ),
           sizeof ( struct s ) );

  printf ( "offsets: i=%zu; c=%zu; d=%zu a=%zu\n"
           "sizeof ( struct t ) = %zu\n",
           offsetof ( struct t, i ), 
           offsetof ( struct t, c ),
           offsetof ( struct t, d ), 
           offsetof ( struct t, a ),
           sizeof ( struct t ) );
}


#include <stdlib.h>

#define swap( a, b ) { int tmp; tmp = (a); (a) = (b); (b) = tmp; }

extern void setpixel ( int x, int y );

// "Acende pixels" da posição (x1,y1) até (x2,y2)
// usando o algoritmo de Bresenham.

void drawline ( int x1, int y1, int x2, int y2 )
{
  int dx, dy, x, y, e, incy;

  // Sempre traça a linha da esquerda para direita!
  if ( x1 > x2 )
  {
    swap ( x1, x2 );
    swap ( y1, y2 );
  }

  // y descerá ou subirá de acordo com a orientação.
  incy = 1;
  if ( y1 > y2 )
    incy = -incy;

  dx = abs( x2 - x1 );
  dy = abs( y2 - y1 );
  x = x1;
  y = y1;

  setpixel ( x, y );

  e = 2 * dy - dx;
  while ( x <= x2 )
  {
    if ( e < 0 )
      e += 2 * dy;
    else
    {
      y += incy;
      e += 2 * ( dy - dx );
    }

    setpixel ( ++x, y );
  }
}

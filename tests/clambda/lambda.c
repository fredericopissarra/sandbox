#include <stdio.h>

#define LAMBDA(...) ({ __VA_ARGS__; _;})

// Uma função que USA nosso callback.
void f( int n, void (*cb)(int) )
{ for ( int i = 0; i < n; i++ ) cb(i); }

// Chamando f, passando o callback.
int main( void )
{
  f( 4, LAMBDA( void _(int n){ printf( "%d\n", n ); } ) );

  // Diferente!
  f( 4, LAMBDA( void _(int n){ printf( "%d\n", 3*n ); } ) );

  // Diferente!
  printf( "%d\n", LAMBDA( int _(int n) { return n + 10; } )(3) );
}

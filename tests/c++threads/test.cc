#include <iostream>
#include <chrono>
#include <thread>
#include <mutex>

using namespace std::chrono_literals;

// ----------------

// Classe da thread
class mythread {
  std::mutex m;

  // objetos fornecidos na entrada da thread...
  int x_;

  // objetos necessários (resultado?) para a thread.
  long long result;
  bool done;

public:
  // Construtor da thread
  // objetos primitivos tem que ser inicializados (C++ standard).
  mythread( int x ) : x_(x), result(0), done(false) {}

  // Função que "roda" a thread.
  void run();

  // funções auxiliares da thread
  bool isDone() 
  { 
    // done é lido e escrito por diferentes threads nesse código.
    // sincronização necessária.
    std::lock_guard<std::mutex> g(m);
    return done;
  }

  long long getResult() { return result; }
};

// O código da thread
void mythread::run()
{
  int i = x_;

  // efetivalemente calcula o fatorial de x_.
  result = 1LL;
  while ( i-- > 1 )
  {
    std::this_thread::sleep_for( 500ms );     // Para esperar um cadinho (para ver o efeito).
    result *= i;
  }

  // Recurso acessado por duas threads (escrita)...
  std::lock_guard<std::mutex> g(m);
  done = true;
}

// ----------------
int main( void )
{
  time_t t1, t2;
  bool done;

  mythread t( 10 );  // cria e inicializa o objeto da thread.

  std::thread thr( mythread::run, &t ); // Põe a thread para rodar usando o objeto t.

  // Para esse teste eu não quero joinable threads...
  thr.detach();

  time( &t1 );
  while ( ! t.isDone() )
  {
    time( &t2 );
    if ( t2 != t1 )
    {
      std::cout << '.';
      t1 = t2;
    }
  }
  std::cout << '\n';

  // Mostra o resultado...
  std::cout << "Result=" << t.getResult() << '\n';

  return EXIT_SUCCESS;
}

#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <x86intrin.h>
#include <string.h>

// Minhas rotinas de contagem de ciclos.
#include <cycle_counting.h>

__noinline 
void _memcpy_sse(void *dest, void *src, size_t size)
{
  size_t xmm_blocks;
  size_t remaining_bytes;
  __m128i *xmmdest, *xmmsrc;

  xmm_blocks = size / 16;
  remaining_bytes = size % 16;

  // Copia de 16 em 16 bytes
  xmmdest = (__m128i *)dest;
  xmmsrc = (__m128i *)src;
  while (xmm_blocks--)
    *xmmdest++ = *xmmsrc++;

  memcpy(dest, src, remaining_bytes);
  // Copia os bytes restantes...
  //dest = xmmdest; src = xmmsrc;
  //while (remaining_bytes--)
  //  *(char *)dest++ = *(char *)src++;
}

__noinline
void _memcpy_asm(void *dest, void *src, size_t size)
{
  __asm__ __volatile__ (
    "rep; movsb" 
    : : "D" (dest), "S" (src), "c" (size) : "memory" );
}

void main(void)
{
  // Dois buffers grandes no segmento .bss...
  static char buffer1[65535], buffer2[65535];
  counter_T c1, c2, c3;

  // Por incrível que pareça, isso quase 14 vezes mais LERDO que um simples rep movsb
  // pelo menos a partir da arquitetura Sandy Bridge (?).
  c1 = BEGIN_TSC();
  _memcpy_sse(buffer2, buffer1, sizeof buffer1);
  c1 = END_TSC(c1);

  c2 = BEGIN_TSC();
  _memcpy_asm(buffer2, buffer1, sizeof buffer1);
  c2 = END_TSC(c2);

  c3 = BEGIN_TSC();
  __builtin_memcpy(buffer2, buffer1, sizeof buffer1);
  c3 = END_TSC(c3);

  printf("_memcpy_sse: %" PRIu64 " cycles\n"
         "_memcpy_asm: %" PRIu64 " cycles\n"
         "memmove    : %" PRIu64 " cycles\n",
        c1, c2, c3);
}

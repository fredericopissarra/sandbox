#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <cycle_counting.h>

char buffer[1000];

extern void bzero_loop(void *, size_t);
extern void bzero_sse(void *, size_t);

static inline void bzero_asm( void *p, size_t size )
{
  __asm__ __volatile__ ( "rep; stosb" : : "D" (p), "c" (size), "a" (0) : "memory" );
}

extern unsigned long long start_cycle_count(void);
extern unsigned long long end_cycle_count(void);

int main(int argc, char *argv[])
{
  counter_T t1, t2, t3, t4, t5;
  int i;

  i = 1000;
  t1 = BEGIN_TSC();
  while (i--) bzero_loop(buffer, sizeof(buffer));
  t1 = END_TSC(t1);

  i = 1000;
  t2 = BEGIN_TSC();
  while (i--) bzero_asm(buffer, sizeof(buffer));
  t2 = END_TSC(t2);

  i = 1000;
  t3 = BEGIN_TSC();
  while (i--) bzero_sse(buffer, sizeof(buffer));
  t3 = END_TSC(t3);

  i = 1000;
  t4 = BEGIN_TSC();
  while (i--) bzero(buffer, sizeof(buffer));
  t4 = END_TSC(t4);

  i = 1000;
  t5 = BEGIN_TSC();
  while (i--) memset(buffer, 0, sizeof(buffer));
  t5 = END_TSC(t5);

  printf("bzero_loop: %llu cycles.\n"
         "bzero_asm: %llu cycles.\n"
         "bzero_sse: %llu cycles.\n"
         "bzero: %llu cycles.\n"
         "memset: %llu cycles.\n", t1, t2, t3, t4, t5);

  return 0;
}

#include <Python.h>

int main ( int argc, char *argv[] )
{
  PyObject *pName, *pModule, *pDict, *pFunc;
  PyObject *pArgs, *pValue;
  int i;

  if ( argc < 3 )
  {
    fprintf ( stderr, "Usage: call <val1> <val2>\n" );
    return EXIT_FAILURE;
  }

  Py_Initialize();
  atexit( Py_Finalize );

  // Sem isso o módulo não pode ser carregado!
  PySys_SetPath( L"./" );

  pModule = PyImport_ImportModule ( "multiply" );

  if ( pModule )
  {
    pFunc = PyObject_GetAttrString ( pModule, "multi" );
    /* pFunc is a new reference */

    if ( pFunc && PyCallable_Check ( pFunc ) )
    {
      pArgs = PyTuple_New ( argc - 1 );

      for ( i = 0; i < argc - 1; ++i )
      {
        pValue = PyLong_FromLong ( atoi ( argv[i + 1] ) );

        if ( ! pValue )
        {
          Py_DECREF ( pArgs );
          Py_DECREF ( pModule );
          fprintf ( stderr, "Cannot convert argument\n" );
          return EXIT_FAILURE;
        }

        /* pValue reference stolen here: */
        PyTuple_SetItem ( pArgs, i, pValue );
      }

      pValue = PyObject_CallObject ( pFunc, pArgs );
      Py_DECREF ( pArgs );

      if ( pValue )
      {
        printf ( "Result of call: %ld\n", PyLong_AsLong ( pValue ) );
        Py_DECREF ( pValue );
      }
      else
      {
        Py_DECREF ( pFunc );
        Py_DECREF ( pModule );
        PyErr_Print();
        fprintf ( stderr, "Call failed\n" );
        return EXIT_FAILURE;
      }
    }
    else
    {
      if ( PyErr_Occurred() )
        PyErr_Print();

      fprintf ( stderr, "Cannot find function \"%s\"\n", argv[2] );
    }

    Py_XDECREF ( pFunc );
    Py_DECREF ( pModule );
  }
  else
  {
    PyErr_Print();
    fprintf ( stderr, "Failed to load \"%s\"\n", argv[1] );
    return EXIT_FAILURE;
  }

  return EXIT_SUCCESS;
}

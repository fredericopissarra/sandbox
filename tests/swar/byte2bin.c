#include <stdio.h>
#include <stdint.h>
#include <x86intrin.h>
#include <cycle_counting.h>

__attribute__((noinline))
char *byte2bin_sse (  char *str, uint8_t byte )
{
  const __v16qu mask = { 0x80, 0x40, 0x20, 0x10, 0x08, 0x04, 0x02, 0x01 };

  __m128i tmp = _mm_set1_epi8( byte );

  tmp = _mm_and_si128( tmp, ( __m128i ) mask );
  tmp = _mm_cmpeq_epi8( tmp, ( __m128i ) mask );
  tmp = _mm_and_si128( tmp, _mm_set1_epi8( 1 ) );
  tmp = _mm_add_epi8( tmp, _mm_set1_epi8( '0' ) );
  _mm_storel_epi64( ( __m128i *) str, tmp );

  str[8] = '\0';

  return str;
}

__attribute__((noinline))
char *byte2bin_swar ( char *str, uint8_t byte )
{
  uint64_t r;

#define MASK_2 0x0100040010008000ULL
#define MASK_3 0x0002000800200080ULL

  r = byte * MASK_2;
  r |= byte * MASK_3;
  r = ( r >> 7 ) & 0x0101010101010101ULL;
  r += 0x3030303030303030ULL;
  * ( uint64_t * ) str = r;

  str[8] = '\0';

  return str;
}

int main ( void )
{
  char buf1[9], buf2[9];
  counter_T c1, c2;

  c1 = BEGIN_TSC();
  byte2bin_sse ( buf1, 0xc3 );
  c1 = END_TSC ( c1 );

  c2 = BEGIN_TSC();
  byte2bin_swar ( buf2, 0xc3 );
  c2 = END_TSC ( c2 );

  printf ( "SSE:  \"%s\" (%lu cycles)\n"
           "SWAR: \"%s\" (%lu cycles)\n",
           buf1, c1, buf2, c2 );
}

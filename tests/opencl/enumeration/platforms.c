#include <stdio.h>
#include <stdlib.h>
#include <CL/cl.h>

int main( void )
{
  int i, j;
  char *info;
  size_t infoSize;
  cl_uint platformCount;
  cl_platform_id *platforms;
  const char *attributeNames[5] = { "Name", "Vendor",
                                    "Version", "Profile", "Extensions"
                                  };
  const cl_platform_info attributeTypes[5] = { CL_PLATFORM_NAME, CL_PLATFORM_VENDOR,
                                               CL_PLATFORM_VERSION, CL_PLATFORM_PROFILE, CL_PLATFORM_EXTENSIONS
                                             };
  const int attributeCount = sizeof ( attributeNames ) / sizeof ( char * );

  // get platform count
  clGetPlatformIDs ( 5, NULL, &platformCount );

  // get all platforms
  if ( ! ( platforms = ( cl_platform_id * ) malloc ( sizeof ( cl_platform_id ) * platformCount ) ) )
  {
    fputs ( "Error allocating memory for platform ids.\n", stderr );
    return EXIT_FAILURE;
  }

  clGetPlatformIDs ( platformCount, platforms, NULL );

  // for each platform print all attributes
  for ( i = 0; i < platformCount; i++ )
  {
    printf ( "\n %d. Platform \n", i + 1 );

    for ( j = 0; j < attributeCount; j++ )
    {
      // get platform attribute value size
      clGetPlatformInfo ( platforms[i], attributeTypes[j], 0, NULL, &infoSize );

      if ( ! ( info = ( char * ) malloc ( infoSize ) ) )
      {
        free ( platforms );
        fputs ( "Error allocating memory for attribute value.\n", stderr );
        return EXIT_FAILURE;
      }

      // get platform attribute value
      clGetPlatformInfo ( platforms[i], attributeTypes[j], infoSize, info, NULL );

      printf ( "  %d.%d %-11s: %s\n", i + 1, j + 1, attributeNames[j], info );
      free ( info );
    }

    putchar ( '\n' );
  }

  free ( platforms );
  return 0;
}

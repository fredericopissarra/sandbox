#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>

#include <loader.h>

// Retorna NULL se não conseguiu abrir arquivo.
// Retorna ponteiro contendo a string alocada com o código carregado.
char *LoadFile( const char *path )
{
  char *p;
  struct stat st;
  int fd;

  if ( stat( path, &st ) )
    return NULL;

  fd = open( path, O_RDONLY );
  if ( fd < 0 )
    return NULL;

  p = malloc( st.st_size + 1 );
  if ( p )
    // FIXME: Alterar para suportar leituras parciais...
    if ( read( fd, p, st.st_size ) != st.st_size )
    {  free( p ); p = NULL; }
    else
      p[st.st_size] = '\0';

  close( fd );
  return p;
}

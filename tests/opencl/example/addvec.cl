__kernel void addvec( __global *a,
                      __global *b,
                      __global *result )
{
  unsigned int i = get_global_id(0);

  result[i] = a[i] + b[i];
}

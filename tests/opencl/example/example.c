#define CL_TARGET_OPENCL_VERSION 300

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <cl/cl.h>
#include <loader.h>

#define ARRAY_ELEMS(a__) ( sizeof a__ / sizeof a__[0] )

static void fill_array ( float *, unsigned int );
static void show_array ( const char *, const float *, unsigned int );

int main ( void )
{
  cl_platform_id platform;
  cl_device_id device;
  cl_context context;
  cl_command_queue queue;
  cl_program program;
  cl_kernel kernel;
  cl_mem mem_a, mem_b, mem_r;
  cl_uint num_platforms, num_devices, err;
  const char *source;

  float a[16], b[16], result[16];

  source = ( const char * ) LoadFile ( "addvec.cl" );

  if ( ! source )
  {
    fputs ( "ERROR loading CL source.\n", stderr );
    return EXIT_FAILURE;
  }

  fill_array ( a, ARRAY_ELEMS( a ) );
  memcpy( b, a, sizeof a );

  // Assume que temos apenas uma plataforma.
  if ( clGetPlatformIDs ( 1, &platform, &num_platforms ) )
  {
    fputs ( "ERROR: Getting platform.\n", stderr );
    return EXIT_FAILURE;
  }

  // Assume que temos apenas 1 device (GPU)...
  if ( clGetDeviceIDs ( platform, CL_DEVICE_TYPE_GPU, 1, &device, &num_devices ) )
  {
    fputs ( "ERROR: Getting GPU device.\n", stderr );
    return EXIT_FAILURE;
  }

  // Cria um contexto com o device...
  err = 0;
  context = clCreateContext ( NULL, 1, &device, NULL, NULL, &err );

  if ( err )
  {
    clReleaseDevice ( device );
    fputs ( "ERROR: Creating context.\n", stderr );
    return EXIT_FAILURE;
  }

  // cria os buffers relativos aos argumentos.
  err = 0;
  cl_uint e = 0;
  mem_a = clCreateBuffer ( context, CL_MEM_READ_ONLY, sizeof a, NULL, &e ); err |= e;
  mem_b = clCreateBuffer ( context, CL_MEM_READ_ONLY, sizeof b, NULL, &e ); err |= e;
  mem_r = clCreateBuffer ( context, CL_MEM_WRITE_ONLY, sizeof result, NULL, &e ); err |= e;
  if ( err )
  {
    clReleaseContext( context );
    clReleaseDevice( device );
    fputs( "ERROR: Creating buffer objects.\n", stderr );
    return EXIT_FAILURE;
  }

  // Cria uma fila de comando associada ao device e contexto.
  err = 0;
  queue = clCreateCommandQueueWithProperties ( context, device, NULL, &err );

  if ( err )
  {
    clReleaseContext ( context );
    clReleaseDevice ( device );
    fputs ( "ERROR: Creating command queue.\n", stderr );
    return EXIT_FAILURE;
  }

  // Carrega o programa...
  err = 0;
  size_t size[] = { strlen( source ), 0 };
  program = clCreateProgramWithSource ( context, 1, ( const char ** ) &source, size, &err );

  if ( err )
  {
    clReleaseCommandQueue ( queue );
    clReleaseContext ( context );
    clReleaseDevice ( device );
    fputs ( "ERROR: Loading program to OpenCL.\n", stderr );
    return EXIT_FAILURE;
  }

  // Compila e linka...
  if ( clBuildProgram ( program, 1, &device, NULL, NULL, NULL ) )
  {
    char *log;
    size_t size;

    // Mostra LOG em caso de erro.
    clGetProgramBuildInfo ( program, device, CL_PROGRAM_BUILD_LOG, 0, NULL, &size );
    log = malloc ( size + 1 );
    clGetProgramBuildInfo ( program, device, CL_PROGRAM_BUILD_LOG, size, log, NULL );
    log[size] = '\0';

    clReleaseCommandQueue ( queue );
    clReleaseProgram ( program );
    clReleaseContext ( context );
    clReleaseDevice ( device );

    fprintf ( stderr, "%s\n", log );
    free ( log );
    return EXIT_FAILURE;
  }

  free( (void *) source );

  // Cria kernel.
  err = 0;
  kernel = clCreateKernel ( program, "addvec", &err );

  if ( err )
  {
    clReleaseCommandQueue ( queue );
    clReleaseProgram ( program );
    clReleaseContext ( context );
    clReleaseDevice ( device );
    fputs ( "ERROR: Creating kernel.\n", stderr );
    return EXIT_FAILURE;
  }

  clEnqueueWriteBuffer ( queue, mem_a, 1, 0, sizeof a, a, 0, NULL, NULL );
  clEnqueueWriteBuffer ( queue, mem_b, 1, 0, sizeof b, b, 0, NULL, NULL );

  clSetKernelArg ( kernel, 0, sizeof mem_a, &mem_a );
  clSetKernelArg ( kernel, 1, sizeof mem_b, &mem_b );
  clSetKernelArg ( kernel, 2, sizeof mem_r, &mem_r );

  // Executa kernel.
  size_t num_work_items = ARRAY_ELEMS( a );       // Temos 16 itens apenas!
  size_t num_items_per_group = ARRAY_ELEMS( a );  // Grupos com 16 work_itens sáo aceitáveis!

  if ( clEnqueueNDRangeKernel ( queue, kernel, 1, NULL, &num_work_items, &num_items_per_group, 0, NULL, NULL ) )
  {
    clReleaseCommandQueue ( queue );
    clReleaseKernel ( kernel );
    clReleaseMemObject ( mem_r );
    clReleaseMemObject ( mem_b );
    clReleaseMemObject ( mem_a );
    clReleaseProgram ( program );
    clReleaseContext ( context );
    clReleaseDevice ( device );

    fputs ( "ERROR: Executing kernel\n", stderr );
    return EXIT_FAILURE;
  }

  // Lê resultado.
  clEnqueueReadBuffer ( queue, mem_r, 1, 0, sizeof result, result, 0, NULL, NULL );

  clReleaseCommandQueue ( queue );
  clReleaseKernel ( kernel );
  clReleaseMemObject ( mem_r );
  clReleaseMemObject ( mem_b );
  clReleaseMemObject ( mem_a );
  clReleaseProgram ( program );
  clReleaseContext ( context );
  clReleaseDevice ( device );

  show_array ( "a", a, ARRAY_ELEMS( a ) );
  show_array ( "b", b, ARRAY_ELEMS( b ) );
  show_array ( "result", result, ARRAY_ELEMS( result ) );

  return EXIT_SUCCESS;
}

void fill_array ( float *p, unsigned int elems )
{
  float t = 1.0f;
  float *q = p + elems;

  while ( p < q )
  {
    *p++ = t;
    t *= 10;
  }
}

void show_array ( const char *prefix, const float *p, unsigned int elems )
{
  const float *q = p + elems;

  printf ( "%s: { ", prefix );

  while ( p < q )
    printf ( "%g, ", *p++ );

  puts ( "};" );
}

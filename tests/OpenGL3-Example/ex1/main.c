#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <GL/gl.h>
#include <GL/glfw.h>

#define WINDOW_WIDTH  1280
#define WINDOW_HEIGHT 720

// Macro usada para atribuir offsets aos Vertex Attribute Pointers.
#define BUFFER_OFFSET(ofs) (NULL + (ofs))

// "Handle" do programa contendo shaders.
static unsigned int shaders_program;

static void         InitOpenGL ( void );
static char        *LoadText ( const char * );
static _Bool        LoadShaders ( unsigned int * );
static unsigned int CreateVAO ( void );
static void         DrawVAO ( int, size_t );

int main ( void )
{
  GLFWmonitor *monitor;
  GLFWwindow *window;

  /* *** INICIALIZAÇÃO do contexto gráfico usando GLFW 2.7 *** */
  if ( ! glfwInit() )
  {
    fputs ( "initializing graphics library\n", stderr );
    return EXIT_FAILURE;
  }

  monitor = glfwGetPrimaryMonitor();

  if ( ! monitor )
    return EXIT_FAILURE;

  /* O tratamento de eventos é feito "manualmente", daqui pra frente. */
  glfwDisable ( GLFW_AUTO_POLL_EVENTS );

  /* Usando o core profile do OpenGL 3.0 (uma de minhas máquinas de teste só suporta a versão 3!) */
  //glfwWindowHint ( GLFW_RED_BITS, 8 );
  //glfwWindowHint ( GLFW_GREEN_BITS, 8 );
  //glfwWindowHint ( GLFW_BLUE_BITS, 8 );
  //glfwWindowHint ( GLFW_ALPHA_BITS, 8 );
  glfwWindowHint ( GLFW_REFRESH_RATE, 30 );
  //glfwWindowHint ( GLFW_DOUBLEBUFFER, 1 );
  glfwWindowHint ( GLFW_RESIZABLE, 0 );
  glfwWindowHint ( GLFW_DECORATED, 0 );
  //glfwWindowHint ( GLFW_DEPTH_BITS, 24 );
  glfwWindowHint ( GLFW_OPENGL_VERSION_MAJOR, 4 );
  glfwWindowHint ( GLFW_OPENGL_VERSION_MINOR, 3 );
  glfwWindowHint ( GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE );
  glfwWindowHint ( GLFW_SAMPLES, 4 );

  window = glfwCreateWindow ( WIDOW_WIDTH, WINDOW_HEIGHT, "ex1", monitor, NULL );

  if ( ! window )
  {
    glfwTerminate();
    fputs ( "cannot open window\n", stderr );
    return EXIT_FAILURE;
  }

  // --- Isso está aqui só para eu saber com o que estou lidando.
#ifdef DEBUG
  {
    // get version info
    const GLubyte *renderer = glGetString ( GL_RENDERER ); // get renderer string
    const GLubyte *version = glGetString ( GL_VERSION ); // version as a string
    int major, minor, rev;

    glfwGetGLVersion ( &major, &minor, &rev );

    printf ( "Renderer: %s\n", renderer );
    printf ( "OpenGL version supported %s\n", version );
    printf ( "GLFW reports OpenGL version %d.%d.%d\n", major, minor, rev );
  }
#endif

  glfwMakeCurrentContext ( window );
  glfwShowWindow ( window );
  glfwSetInputMode ( window, GLFW_CURSOR, GLFW_CURSOR_DISABLE );

  InitOpenGL();

  if ( ! LoadShaders ( &shaders_program ) )
  {
    glfwTerminate();
    return EXIT_FAILURE;
  }

  CreateVAO();
  glUseProgram ( shaders_program );

  while ( ! glfwWindowShouldClose( window ) )
  {
    glClear ( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

    /* NOTE: VAO já está "bound", neste ponto. */
    glDrawArrays ( GL_TRIANGLES, 0, 3 ); /* count é o número de vertices, não o de primitivos!
                                         GL_TRIANGLES só indica que o OpenGL ira desenhar triangulos! */

    glFlush();
    glfwSwapBuffers();

    glfwPollEvents();

    if ( glfwGetKey ( GLFW_KEY_ESC ) == GLFW_PRESS )
      break;
  }

  glfwDestroyWindow( window );
  glfwTerminate();
  return EXIT_SUCCESS;
}

// Initializa OpenGL.
static void InitOpenGL ( void )
{
  glViewport ( 0, 0, WINDOW_WIDTH, WINDOW_HEIGHT );

  // Uses default black background.
  //glClear( 0.0f, 0.0f, 0.0f, 1.0f );

  // Uses default clear value 1.0f for depth buffer.
  //glClearDepth ( 1.0f );

  glEnable ( GL_CULL_FACE );
  glFrontFace ( GL_CCW );
  glCullFace ( GL_BACK );

  /* Liga o teste de profundidade. */
  glEnable ( GL_DEPTH_TEST );
  //glDepthFunc ( GL_LESS );
}

// Função auxiliar: Apenas lê um arquivo texto e o coloca num buffer. */
static char *LoadText ( const char *filename )
{
  struct stat st;
  char *buffer = NULL;
  int fd;

  fd = open ( filename, O_RDONLY );

  if ( fd < 0 )
    return NULL;

  if ( fstat ( fd, &st ) )
  {
  error:
    close( fd );
    return NULL;
  }

  buffer = malloc ( stat.st_size + 1 );
  if ( ! buffer )
    goto error;

  if ( read ( buffer, st.st_size, fd ) <= 0 )
  {
    free ( buffer );
    goto error;
  }

  buffer[ st.st_size ] = '\0';

  close ( fd );

  return buffer;
}

/* Carrega, compila e linka os shaders. */
static _Bool LoadShaders ( unsigned int *program )
{
  char *vertex_shader;
  char *fragment_shader;
  int status;
  int len;
  char log[1024];
  unsigned int vs, fs;

  // vertex shader
  vertex_shader = LoadText( "simple.vert" );

  if ( ! vertex_shader )
  {
    fputs ( "Erro carregando Vertex Shader.\n", stderr );
    return 0;
  }

  vs = glCreateShader ( GL_VERTEX_SHADER );
  glShaderSource ( vs, 1, &vertex_shader, NULL );

  free ( vertex_shader ); /* Não precisamos mais do texto carregado. */

  glCompileShader ( vs );

  glGetShaderiv ( vs, GL_COMPILE_STATUS, &status );
  if ( ! status )
  {
    glGetShaderInfoLog ( vs, sizeof log - 1, &len, log );
    fprintf ( stderr, "Error compiling vertex shader:\n\t%s\n", log );
    return 0;
  }

  // --- fragment shader
  fragment_shader = LoadText( "simple.frag" );

  if ( ! fragment_shader )
  {
    fputs ( "Erro carregando Fragment Shader.\n", stderr );
    return 0;
  }

  fs = glCreateShader ( GL_FRAGMENT_SHADER );
  glShaderSource ( fs, 1, &fragment_shader, NULL );

  free ( fragment_shader ); /* Não precisamos mais do texto carregado. */

  glCompileShader ( fs );
  glGetShaderiv ( fs, GL_COMPILE_STATUS, &status );

  if ( !status )
  {
    glGetShaderInfoLog ( fs, sizeof ( log ) - 1, &len, log );
    fprintf ( stderr, "Error compiling fragment shader:\n\t%s\n", log );
    return 0;
  }

  // Link program.
  *program = glCreateProgram();
  glAttachShader ( *program, vs );
  glAttachShader ( *program, fs );

  /* Como GLSL 1.3 (do OpenGL 3.0) não tem "layout", nos shaders, então é preciso definir
     a localização de variáveis globais dos shaders aqui. */
  glBindAttribLocation ( *program, 0, "vrtx" ); /* vertex */
  glBindAttribLocation ( *program, 1, "clr" ); /* color */

  glLinkProgram ( *program );
  glGetProgramiv ( *program, GL_LINK_STATUS, &status );

  if ( ! status )
  {
    glGetProgramInfoLog ( vs, sizeof ( log ) - 1, &len, log );
    fprintf ( stderr, "Error linking shaders program:\n\t%s\n", log );
    return 0;
  }

  return 1;
}

static unsigned int CreateVAO ( void )
{
  /* Esses arrays vão desaparecer no final da função. */
  float points[] =
  {
    0.0f,  0.5f, 0.0f,
    -0.5f, -0.5f, 0.0f,
    0.5f, -0.5f, 0.0f
  };

  float colors[] =
  {
    0.0f,  0.0f, 1.0f, 1.0f,
    0.0f,  1.0f, 0.0f, 1.0f,
    1.0f,  0.0f, 0.0f, 1.0f
  };

  unsigned int vao, vbo[2];

  glGenVertexArrays ( 1, &vao );
  glBindVertexArray ( vao );

  // Cria 2 buffers objects. Um para conter os vértices e outro para as cores dos vértices. */
  glGenBuffers ( 2, vbo );

  /* Carrega o vbo com os vertices e associa os atributos. */
  glBindBuffer ( GL_ARRAY_BUFFER, vbo[0] );
  glBufferData ( GL_ARRAY_BUFFER, sizeof ( points ), points, GL_STREAM_DRAW );
  glEnableVertexAttribArray ( 0 );
  glVertexAttribPointer ( 0, 3, GL_FLOAT, 0, 0, BUFFER_OFFSET ( 0 ) );

  /* Carrega o vbo com as cores e associa os atributos. */
  glBindBuffer ( GL_ARRAY_BUFFER, vbo[1] );
  glBufferData ( GL_ARRAY_BUFFER, sizeof ( colors ), colors, GL_STREAM_DRAW );
  glEnableVertexAttribArray ( 1 );
  glVertexAttribPointer ( 1, 4, GL_FLOAT, 0, 0, BUFFER_OFFSET ( 0 ) );

  /* NOTE: Neste exemplo, ao sair, o VAO continuará "bounded". */

  return vao;
}



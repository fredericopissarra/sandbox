#include <GL/glut.h>

void reshape( int w, int h )
{
  glViewport( 0.0f, 0.0f, w, h );

  glMatrixMode( GL_PROJECTION );
  glLoadIdentity();
  glOrtho( 0.0, w, 0.0, h, -1.0, 1.0 );
  glScalef( 1.0f, -1.0f, 1.0f );
  glTranslatef( 0.0f, -h, 0.0f );
}

void display( void )
{
  glClear( GL_COLOR_BUFFER_BIT );

  glBegin( GL_TRIANGLES );
    glColor3f( 0.0f, 0.0f, 1.0f );
    glVertex2i( 0.0f, 0.0f );
    glColor3f( 0.0f, 1.0f, 0.0f );
    glVertex2i( 200.0f, 200.0f );
    glColor3f( 1.0f, 0.0f, 0.0f );
    glVertex2i( 20.0f, 200.0f );
  glEnd();

  glFlush();
}

int main( int argc, char **argv )
{
  glutInit( &argc, argv );
  glutCreateWindow( "single triangle" );
  glutDisplayFunc( display );
  glutReshapeFunc( reshape );
  glutMainLoop();

  return 0;
}

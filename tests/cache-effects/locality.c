/* locality.c */
/* Teste de preenchimento de array usando cache locality */

#include <stdio.h>
#include <x86intrin.h>

#define SYNC_MEM
#include <cycle_counting.h>

#define CACHELINE_SIZE 64

static void fill0 ( char *, unsigned int );
static void fill0_2 ( char *, unsigned int );
static void fill0_3 ( char *, unsigned int );
static void flusharray ( char *, unsigned int );

int main ( int argc, char *argv[] )
{
  // Cria um array grande.
  static char a[8000];

  counter_T c1, c2, c3;

  flusharray ( a, sizeof a );
  c1 = BEGIN_TSC();
  fill0 ( a, sizeof a );
  c1 = END_TSC ( c1 );

  flusharray ( a, sizeof a );
  c2 = BEGIN_TSC();
  fill0_2 ( a, sizeof a );
  c2 = END_TSC ( c2 );

  flusharray ( a, sizeof a );
  c3 = BEGIN_TSC();
  fill0_3 ( a, sizeof a );
  c3 = END_TSC ( c3 );

  printf ( "fill0:   %lu.\n"
           "fill0_2: %lu.\n"
           "fill0_3: %lu.\n",
           c1, c2, c3 );
}

// Preenche o array sequencialmente.
__attribute__ ( ( noinline ) )
void fill0 ( char *p, unsigned int size )
{
  while ( size-- )
    *p++ = 0;
}

// Preenche o array 1 byte por linha.
__attribute__ ( ( noinline ) )
void fill0_2 ( char *p, unsigned int size )
{
  unsigned int i, j;
  unsigned int lines = size / CACHELINE_SIZE;
  unsigned int remainder = size % CACHELINE_SIZE;

  // O loop interno carrega para o cache L1 todas as linhas
  // se possível. O loop interno vai preenchendo os bytes
  // sequencialmente, para cada linha.
  for ( j = 0; j < CACHELINE_SIZE; j++ )
    for ( i = 0; i < lines; i++ )
      p[CACHELINE_SIZE * i + j] = 0;

  // Preenche os bytes restantes da última linha, se houver.
  p += CACHELINE_SIZE * i;
  while ( remainder-- )
    *p++ = 0;
}

__attribute__((noinline))
void fill0_3( char *p, unsigned int size )
{
  char *q = p;
  unsigned int lines, rem;

  lines = size / CACHELINE_SIZE;
  rem = size % CACHELINE_SIZE;
  if ( rem ) lines++;

  // "puxa" todas as linhas para o cache L1D.
  // Mesmo que não caiba, L2 e L3 conterão as
  // linhas "puxadas" da memória tb.
  while ( lines-- )
  {
    _mm_prefetch( q, 0 );
    q += CACHELINE_SIZE;
  }

  // Preenche sequencialmente.
  while ( size-- )
    *p++ = 0;
}

// Flushing de 'lines' linhas de cache à partir do ponteiro 'p'.
void flusharray ( char *p, unsigned int size )
{
  unsigned int lines = size / CACHELINE_SIZE;
  unsigned int remainder = size % CACHELINE_SIZE;

  if ( remainder )
    lines++;

  while ( lines-- )
  {
    _mm_clflush ( p );
    p += CACHELINE_SIZE;
  }
}

%ifndef STRUCTS_INC_INCLUDED_
%define STRUCTS_INC_INCLUDED_

;▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
; Descriptors PTR.
;▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
struc descr_ptr
  .limit: resw  1
  .addr:  resd  1
endstruc

;▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
; Generic structure for a descriptor (i386)
;▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
struc descr32
  .lim_lo:    resw  1
  .base_lo:   resb  3

  .attribs1:  resb  1

  .lim_hi:                ; lower 4 bits of this byte.
  .attribs2:  resb  1     ; upper 4 bits of this byte.

  .base_hi:   resb  1
endstruc

;▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
; TSS structure.
;▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
struc tss32
  .prev_:       resw  1
                resw  1   ; zero.

  .esp0_:       resd  1
  .ss0_:        resw  1
                resw  1   ; zero

  .esp1_:       resd  1
  .ss1_:        resw  1
                resw  1   ; zero

  .esp2_:       resd  1
  .ss2_:        resw  1
                resw  1   ; zero

  .cr3_:        resd  1
  .eip_:        resd  1
  .eflags_:     resd  1
  .eax_:        resd  1
  .ecx_:        resd  1
  .edx_:        resd  1
  .ebx_:        resd  1
  .esp_:        resd  1
  .ebp_:        resd  1
  .esi_:        resd  1
  .edi_:        resd  1

  .es_:         resw  1
                resw  1   ; zero

  .cs_:         resw  1
                resw  1   ; zero

  .ss_:         resw  1
                resw  1   ; zero

  .ds_:         resw  1
                resw  1   ; zero

  .fs_:         resw  1
                resw  1   ; zero

  .gs_:         resw  1
                resw  1   ; zero

  .ldt_:        resw  1
                resw  1   ; zero

                resw  1   ; zero
  .iomapaddr_:  resw  1   ; upper word

  .ssp_:        resd  1
  ; iomap here (not used for now).
endstruc

;▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
; Page Directory and Page Table 4 KiB page entries
; structure bitmasks and macro.
; PDE and PTE have the base address of a page
; in the upper 20 bits (32 bits address algigned
; to 4 KiB boundary) and the lowest 12 bits
; are used as flags.
;
; Here, PG_ applies to both, PDE applies to PDEs
; (duh!) and PTE_ to PTEs (duh! again).
;▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
; 1 means the page is present in physical memory.
%define PG_PRESENT_BIT       (1 << 0)

; 1 means the page can be writen.
%define PG_RW_BIT            (1 << 1)

; 1 means the page is accessible only by 'kernel'
; (privilege 0).
%define PG_SUPERVISOR_BIT    (1 << 2)

;
%define PG_WRTHROUGH_BIT     (1 << 3)

; 1 means the contents of the page isn't cacheable.
%define PG_CACHEDISABLE_BIT  (1 << 4)

; 1 means the page was accessed by the processor.
%define PG_ACCESSED_BIT      (1 << 5)

; 1 extends the page size
%define PDE_PSE_BIT          (1 << 7)

; 1 mean the page has data different from physical memory.
%define PTE_DIRTY_BIT        (1 << 6)

%define PTE_PAT_BIT          (1 << 7)
%define PTE_GLOBAL_BIT       (1 << 8)

; Mask the 12 lower bits to calculate the where
; the page begin (20 bits field in PDE or PTE).
%define PG_ADDR_MASK(addr_)  ((addr_) & ~0xfff)

; address is a 20 bit aligned by page (offset 0).
; the lower 12 bits of the entry are bit fields by bits above.
; To get the initial page address just use PG_ADDR_MASK.
; To set the initial page address:
;    entry = PG_ADDR_MASK(addr) | bits.

%endif

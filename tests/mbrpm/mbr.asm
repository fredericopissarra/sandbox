;███████████████████████████████████████████████████
; i386 protected mode MBR example.
;
; Test with:
;   make run
; (Required: nasm and qemu).
;
;███████████████████████████████████████████████████

  bits  16
  cpu   386     ; just to avoid using 486+ instruction set.
                ; delete this line if you don't care!

  BOOTSEG equ 0x7c0

  ; Some structures used for protected mode.
  %include "structs.inc"

  ; Normalize CS:IP.
  ; This will make it easy to calculate flat model offsets.
  ; This way, the offsets will be zero based.
  jmp   BOOTSEG:_start

;▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
; Tables
;▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒

; To load GDTR.
_gdt_desc:
  istruc descr_ptr
    at descr_ptr.limit, dw  _gdt_limit
    at descr_ptr.addr,  dd  _gdt        ; Offset will be adjusted later.
  iend

; To load IDTR.
_idt_desc:
  istruc descr_ptr
    at descr_ptr.limit, dw  _idt_limit
    at descr_ptr.addr,  dd  _idt        ; Offset will be adjusted later.
  iend

  ; Global Descript Table (GDT)
  ; I could use 'descr32' structure here, but this is simplier...
_gdt:
  dq    0                        ; NULL selector.
  dq    0x00_cf_9a_000000_ffff   ; code32,r/x,flat,ring0,present
  dq    0x00_cf_92_000000_ffff   ; data32,r/w,flat,ring0,present
_tss32:
  dq    0x00_00_89_000000_0000   ; Basic **not busy** TSS32 GDT
                                 ; entry (adjusted later).
_gdt_limit equ $ - _gdt - 1
; it is "limit", not "size".

; The protected mode selectors (privilege 0)
CODE32_SEG equ (1 << 3)
DATA32_SEG equ (2 << 3)
TSS32_SEG  equ (3 << 3)

  ; NULL IDT ( just one entry! ).
  ; All interrupts are masked in this example.
_idt:
  dq    0
_idt_limit equ $ - _idt - 1
; Again, it is limit, not size.

;▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
; 16 bits code.
;▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
_start:
  ; At this point I could save DL with drive #.
  ; I don't need this for this simple 'Hello' example.

  ; Just to make sure we are in video texto-mode 3.
  ; Clears the screen.
  mov   ax,3          ; Mode 80x25 (16 colors)
  int   0x10

  cli                 ; Disable interrupts (leave NMIs unchanged)
  cld                 ; Just to make sure DF is UP.

  ; Make sure the selectors are ok (don't need this, but to be sure...).
  mov   ax,cs
  mov   ds,ax
  mov   es,ax

  ; Here I use EBX and EBP to store important offsets, since,
  ; by the calling convention in use, they will always be preserved.

  ; EBX is the "physical" addres of this segment (0x7c00).
  movzx ebx,ax
  shl   ebx,4

  movzx esp,sp            ; Make sure upper 16 bits of ESP is zeroed.
                          ; *ESP will be DWORD aligned later*.

  ; EBP is the stack top "physical" address.
  mov   ax,ss
  and   eax,0xffff
  shl   eax,4
  lea   ebp,[esp+eax]

  ; Normalizes GDT_DESC and IDT_DESC offsets based on EBX.
  add   [_gdt_desc + descr_ptr.addr],ebx
  add   [_idt_desc + descr_ptr.addr],ebx

  ; I'm using a TSS here (it is advisable, but not
  ; **really** necessary). So, setup the base address of this
  ; entry in GDT.
  xor   edx,edx
  lea   di,[_tss32]
  lea   eax,[ebx+heap]    ; TSS goes just after the MBR in this
                          ; example.
  mov   dx,tss32_size
  mov   cx,dx
  call  setuptssdescr

  ; Zero TSS.
  lea   di,[heap]
  xor   al,al
  rep   stosb             ; rep stosb is faster in modern processors.

  ; Enable gate A20 (fast method).
  ; All PC-AT supports this method...
  ; This is necessary for protected mode 4GiB granularity.
  in    al,0x92
  and   al,~1         ; Zero reset bit (otherwise the system will reset).
  or    al,0b00000010 ; Set gate A20 bit.
  out   0x92,al

  ;*** Should test the A20 here. Skipped for simplicity ***

  ; Normalize the jmp address to serialize processor later.
  add   [_jmp_addr],ebx

  ; Load GDTR and IDTR
  lgdt  [_gdt_desc]
  lidt  [_idt_desc]

  ; Enable i386 protected mode.
  mov   eax,cr0
  or    ax,1
  mov   cr0,eax

  ; An ugly hack. But this is a 32 bits far jump to protected mode.
  ; I choose to do this because we had to normalize the offset.
  db    0x66
  db    0xea        ; JMP inter-segment...
_jmp_addr:
  dd    _protmode   ; offset (unormalized - normalized above)
  dw    CODE32_SEG  ; Codeseg32 (8)

;▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁
; Auxiliary 16 bits routines.

; Entry: DS:DI = GDT entry addr.
;        EAX = base addr
;        EDX = limit
; Destroys EAX, EDX
setuptssdescr:
  mov   [di+descr32.lim_lo],dx
  shr   edx,16
  and   byte [di+descr32.lim_hi],0b11110000
  and   dl,0b1111
  or    [di+descr32.lim_hi],dl
  
; We'll not use this entry point.
; Ajust only the base address of a descriptor.
; Entry: DS:DI = GDT entry addr, EAX = base addr.
; Destroys EAX.

;setupdescrbase: 
  mov   word [di+descr32.base_lo],ax
  shr   eax,16
  mov   byte [di+descr32.base_lo+2],al
  mov   byte [di+descr32.base_hi],ah
  ret

;▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
; i386 protected mode code
; We cannot use BIOS services at this point. 
;▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
  bits  32

hello_world:
  db    `Hello, Clarisse.`,0
system_halted:
  db    `System Halted.`,0

_protmode:
  ; At this point CS=8. Reload other selectors.
  mov   ax,DATA32_SEG
  mov   ds,ax
  mov   es,ax
  mov   ss,ax
  mov   esp,ebp       ; Retrieve the "physical" top of the stack and

  ; Align ESP to DWORD.
  add   esp,3
  and   esp,~3        ; align it by DWORD.

  ; Load TR with TSS32.
  mov   ax,TSS32_SEG
  ltr   ax

  ;*** FINALLY the setup is concluded. Do something!

  ; Hide cursor (VGA)
  mov   dx,0x3d4      ; CRT Controller index reg
  mov   al,0x0a       ; Cursor Start Register
  out   dx,al
  inc   edx           ; CRT Controller data reg
  in    al,dx
  or    al,0b00100000 ; Desable cursor (bit 5=1).
  out   dx,al

  ; The pointers must be normalized to flat address space.
  lea   esi,[ebx+hello_world]
  mov   edi,0xb8000      ; VRAM beginning.
  push  edi
  call  show_string
  pop   edi
  add   edi,80*2         ; next line
  lea   esi,[ebx+system_halted]
  call  show_string

  ; Halt.
halt:
  hlt
  jmp   halt

;▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁
; Auxiliary 32 bits routines.

; EDI = Address on screen.
; ESI = String Ptr
;
; Destroys AX, ESI e EDI.
show_string:
  mov   ah,7    ; atribute 7 (white char, black background).
  jmp   .test
.loop:
  stosw
.test:
  lodsb
  test  al,al
  jnz   .loop
  ret

; --- OBS: We could use this gotoxy() function.
; Entry: EDI = vram base.
;        EDX = x, EAX=y
; Result: EDI = char address.
;
;gotoxy:
;  imul eax,eax,160             ; y = 160*y (each line has 80 words).
;  lea  edi,[eax+edx*2+0xb8000] ; EDI = y + 2*x + 0xb8000
;  ret
;
; --- But, we want a tiny bootsector here and the routine
;     should check for boundaries.

; At offset 0x1b0 we could have the partition table, but I don't need
; this right now.

;▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
; MBR signature.
;▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
  times 510 - ($ - $$) db 0
  dw    0xaa55

; Here starts sector 2, if we need it. We just have to make sure the
; code below (if it exists) has a limit of 512 bytes/sector.

; This point we can use as a 'heap'. For now this is the base address
; of TSS. Later we can code other sectors here (for a bigger MBR).
heap:

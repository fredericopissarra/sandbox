#ifndef UTF16_TO_WIN1252_H_
#define UTF16_TO_WIN1252_H_

#include <stdint.h>

char *utf16_to_win1252( char **out, uint16_t *in );

#endif


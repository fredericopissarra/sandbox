#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <signal.h>
#include <readline/readline.h>
#include <readline/history.h>

#define SAFE_FREE(ptr__) { free(ptr__); (ptr__)=NULL; }

static int process_command ( const char * );

static void sigint_handler ( int s )
{
  fputs ( "\nInterrupted.\n", stderr );
  exit ( EXIT_FAILURE );
}

int main ( void )
{
  char *p;

  signal ( SIGINT, sigint_handler );

  do
  {
    SAFE_FREE ( p );

    rl_clear_pending_input();

    // readline gerencia o próprio buffer. Não precisamos liberar 'p'.
    p = readline ( "> " );

    // Temos uma linha?
    if ( p && *p )
    {
      if ( process_command ( p ) )
        add_history ( p );
      else
        fprintf ( stderr, "ERROR: command '%s'!\n", p );
    }
  } while ( 1 );
}

static int help ( const char *p )
{
#ifdef DEBUG
  fprintf ( stderr, "HELP arguments: '%s'\n\n", p );
#endif

  puts ( "Available commands:\n\n"
         "  help    - This help screen.\n"
         "  quit    - Quit the program.\n" );

  return 1;
}

_Noreturn static int quit ( const char *p )
{
  puts ( "Exited." );
  exit ( EXIT_SUCCESS );
}

// Estrutura da tabela de comandos.
struct cmd
{
  char *str;
  int ( *fptr ) ( const char * );
};

static void freestr_ ( char **p )
{
  free ( *p );
}

// Se o comando existe, processa-o e retorna o valor retornado pela função associada.
// senão, retorna 0 para indicar erro.
static int process_command ( const char *p )
{
  static const struct cmd cmds[] =
  {
    { "help", help },
    { "quit", quit }
  };

  char *line __attribute__ ( ( cleanup ( freestr_ ) ) ) = NULL;

  line = strdup ( p );

  if ( line )
  {
    char *q, *r;

    // Isola o comando (q) e separa os argumentos (r).
    q = line + strspn ( line, " \f\r\t\v" );
    r = q + strcspn ( q, " \f\r\t\v" );

    if ( r > q )
      *r++ = '\0';
    else
      r = q + strlen ( q );

    // Varre a tabela de comandos, procurando um válido...
    // se achar, chama a função...
    for ( int i = 0; i < sizeof cmds / sizeof cmds[0]; i++ )
      if ( ! strcasecmp ( q, cmds[i].str ) )
        if ( cmds[i].fptr )
          return cmds[i].fptr ( r );
  }

  return 0;
}


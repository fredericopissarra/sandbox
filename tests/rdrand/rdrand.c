#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/time.h>
#include "../../src/cycle_counting.h"

static unsigned int rand_rdrand(void);
static unsigned int rand_urandom(void);

int main(void)
{
  unsigned int r1, r2, r3;
  counter_T c1, c2, c3;
  struct timeval tv;

  gettimeofday(&tv, NULL);
  srand(tv.tv_sec * 1000000 + tv.tv_usec);  // overflow?

  c1 = BEGIN_TSC();
  r1 = rand();
  c1 = END_TSC(c1);

  c2 = BEGIN_TSC();
  r2 = rand_rdrand();
  c2 = END_TSC(c2);
  
  c3 = BEGIN_TSC();
  r3 = rand_urandom();
  c3 = END_TSC(c3);

  printf("rand():       %10u (%lu cycles)\n"
         "rdrand:       %10u (%lu cycles)\n"
         "/dev/urandom: %10u (%lu cycles)\n",
         r1, c1,
         r2, c2,
         r3, c3);

  return 0;
}

static unsigned int rand_rdrand(void)
{
  unsigned int r;

  __asm__ __volatile__ ("1: rdrand %0\n"
                        "   jnc 1b"
                        : "=a" (r));

  return r;
}

static unsigned int rand_urandom(void)
{
  int fd;
  unsigned int r = 0;

  if ((fd = open("/dev/urandom", O_RDONLY)) != -1)
  {
    if (read(fd, &r, sizeof r) == -1)
      r = 0;

    close(fd);
  }

  return r;
}


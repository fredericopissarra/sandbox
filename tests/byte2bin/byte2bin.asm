  bits  64
  default rel

  section .text

; Converts one byte to a string with 8 chars ('0', '1') plus a NUL char.
; The buffers pointed by ptr must be 9 chars log.
;
; SWAR = SIMD Withing A Register.

;--- This is the usual routine (more or less 40 cycles):
; void byte2bin( uint8_t byte, char *ptr )
; {
;   unsigned int size = 8;
;
;   while ( size-- )
;   {
;     *outp++ = '0' + !!(byte & 0x80);
;     byte <<= 1;
;   }
;   *outp = '\0';
; }

  global byte2bin

  align 4
byte2bin:
  ; Almost all of these instructions can run on all execution units 
  ; (0,1,5 or 6), so they will be paralelized. But we hava an 8
  ; iteractions loop!
  lea rcx, [rsi+8]      ; Past the end of buffer (units 1 and 5).
  mov rax, rsi

  ; This loop repeats 8 times!
  align 4
.loop:
  mov edx, edi
  shr dl, 7
  add dl, '0'
  inc rax
  add edi, edi
  mov BYTE [rax-1], dl  ; Only unit 4
  cmp rcx, rax
  jne .loop             ; Only unit 6.

  mov BYTE [rcx], 0     ; Puts the NUL char. Only unit 4.
  ret

;--- Routine using SWAR (more or less 10 cycles).
; void byte2bin_swar( uint8_t byte, char *ptr );

  global byte2bin_swar

  align 4
byte2bin_swar:
  ; Every single one of these instructions (except imul and ret)
  ; can run on all 4 execution units (0,1,5 or 6), so they will
  ; be paralelized.
  movzx   edi, dil    ; Zero upper 56 bits of RDI.

  ; MAGIC!! Hehe.
  mov     rax, 0x0002000800200080
  mov     rdx, 0x0000040010004001
  imul    rax, rdi    ; These 2 instructions takes 6 cycles.
  imul    rdi, rdx    ; 3 cycles each, and they run only on execution unit 1.
  or      rdi, rax

  ; Isolate the lsbs and add '0' to each byte.
  mov     rax, 0x0101010101010101
  and     rdi, rax
  mov     rax, 0x3030303030303030
  add     rdi, rax

  ; These 2 only in unit 4.
  movbe   [rsi], rdi        ; move rdi (converting to big endian).
  mov     BYTE [rsi+8], 0   ; writes the NUL char.
  ret

  ; FASTER? (4~6 cycles?) But expensive!
  global  byte2bin_tbl

  align 4
byte2bin_tbl:
  movzx   edi,dil
  lea     rax,[bintbl]    ; RIP relative address.
  lea     rdi,[rdi + rdi*8]
  add     rax,rdi
  ret

  section .rodata

  ; 2.37 KiB table
  align 8
bintbl:
  db "00000000",0
  db "00000001",0
  db "00000010",0
  db "00000011",0
  db "00000100",0
  db "00000101",0
  db "00000110",0
  db "00000111",0
  db "00001000",0
  db "00001001",0
  db "00001010",0
  db "00001011",0
  db "00001100",0
  db "00001101",0
  db "00001110",0
  db "00001111",0
  db "00010000",0
  db "00010001",0
  db "00010010",0
  db "00010011",0
  db "00010100",0
  db "00010101",0
  db "00010110",0
  db "00010111",0
  db "00011000",0
  db "00011001",0
  db "00011010",0
  db "00011011",0
  db "00011100",0
  db "00011101",0
  db "00011110",0
  db "00011111",0
  db "00100000",0
  db "00100001",0
  db "00100010",0
  db "00100011",0
  db "00100100",0
  db "00100101",0
  db "00100110",0
  db "00100111",0
  db "00101000",0
  db "00101001",0
  db "00101010",0
  db "00101011",0
  db "00101100",0
  db "00101101",0
  db "00101110",0
  db "00101111",0
  db "00110000",0
  db "00110001",0
  db "00110010",0
  db "00110011",0
  db "00110100",0
  db "00110101",0
  db "00110110",0
  db "00110111",0
  db "00111000",0
  db "00111001",0
  db "00111010",0
  db "00111011",0
  db "00111100",0
  db "00111101",0
  db "00111110",0
  db "00111111",0
  db "01000000",0
  db "01000001",0
  db "01000010",0
  db "01000011",0
  db "01000100",0
  db "01000101",0
  db "01000110",0
  db "01000111",0
  db "01001000",0
  db "01001001",0
  db "01001010",0
  db "01001011",0
  db "01001100",0
  db "01001101",0
  db "01001110",0
  db "01001111",0
  db "01010000",0
  db "01010001",0
  db "01010010",0
  db "01010011",0
  db "01010100",0
  db "01010101",0
  db "01010110",0
  db "01010111",0
  db "01011000",0
  db "01011001",0
  db "01011010",0
  db "01011011",0
  db "01011100",0
  db "01011101",0
  db "01011110",0
  db "01011111",0
  db "01100000",0
  db "01100001",0
  db "01100010",0
  db "01100011",0
  db "01100100",0
  db "01100101",0
  db "01100110",0
  db "01100111",0
  db "01101000",0
  db "01101001",0
  db "01101010",0
  db "01101011",0
  db "01101100",0
  db "01101101",0
  db "01101110",0
  db "01101111",0
  db "01110000",0
  db "01110001",0
  db "01110010",0
  db "01110011",0
  db "01110100",0
  db "01110101",0
  db "01110110",0
  db "01110111",0
  db "01111000",0
  db "01111001",0
  db "01111010",0
  db "01111011",0
  db "01111100",0
  db "01111101",0
  db "01111110",0
  db "01111111",0
  db "10000000",0
  db "10000001",0
  db "10000010",0
  db "10000011",0
  db "10000100",0
  db "10000101",0
  db "10000110",0
  db "10000111",0
  db "10001000",0
  db "10001001",0
  db "10001010",0
  db "10001011",0
  db "10001100",0
  db "10001101",0
  db "10001110",0
  db "10001111",0
  db "10010000",0
  db "10010001",0
  db "10010010",0
  db "10010011",0
  db "10010100",0
  db "10010101",0
  db "10010110",0
  db "10010111",0
  db "10011000",0
  db "10011001",0
  db "10011010",0
  db "10011011",0
  db "10011100",0
  db "10011101",0
  db "10011110",0
  db "10011111",0
  db "10100000",0
  db "10100001",0
  db "10100010",0
  db "10100011",0
  db "10100100",0
  db "10100101",0
  db "10100110",0
  db "10100111",0
  db "10101000",0
  db "10101001",0
  db "10101010",0
  db "10101011",0
  db "10101100",0
  db "10101101",0
  db "10101110",0
  db "10101111",0
  db "10110000",0
  db "10110001",0
  db "10110010",0
  db "10110011",0
  db "10110100",0
  db "10110101",0
  db "10110110",0
  db "10110111",0
  db "10111000",0
  db "10111001",0
  db "10111010",0
  db "10111011",0
  db "10111100",0
  db "10111101",0
  db "10111110",0
  db "10111111",0
  db "11000000",0
  db "11000001",0
  db "11000010",0
  db "11000011",0
  db "11000100",0
  db "11000101",0
  db "11000110",0
  db "11000111",0
  db "11001000",0
  db "11001001",0
  db "11001010",0
  db "11001011",0
  db "11001100",0
  db "11001101",0
  db "11001110",0
  db "11001111",0
  db "11010000",0
  db "11010001",0
  db "11010010",0
  db "11010011",0
  db "11010100",0
  db "11010101",0
  db "11010110",0
  db "11010111",0
  db "11011000",0
  db "11011001",0
  db "11011010",0
  db "11011011",0
  db "11011100",0
  db "11011101",0
  db "11011110",0
  db "11011111",0
  db "11100000",0
  db "11100001",0
  db "11100010",0
  db "11100011",0
  db "11100100",0
  db "11100101",0
  db "11100110",0
  db "11100111",0
  db "11101000",0
  db "11101001",0
  db "11101010",0
  db "11101011",0
  db "11101100",0
  db "11101101",0
  db "11101110",0
  db "11101111",0
  db "11110000",0
  db "11110001",0
  db "11110010",0
  db "11110011",0
  db "11110100",0
  db "11110101",0
  db "11110110",0
  db "11110111",0
  db "11111000",0
  db "11111001",0
  db "11111010",0
  db "11111011",0
  db "11111100",0
  db "11111101",0
  db "11111110",0
  db "11111111",0

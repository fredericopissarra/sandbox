#include <stdio.h>
#include <stdint.h>

extern void byte2bin( uint8_t, char * );
extern void byte2bin_swar( uint8_t, char * );
extern const char *byte2bin_tbl( uint8_t );

int main( void )
{
  char buffer[9];

  byte2bin( 0x5a, buffer );
  printf( "%#x: \"%s\"\n", 0x5a, buffer );

  byte2bin_swar( 0x99, buffer );
  printf( "%#x: \"%s\"\n", 0x99, buffer );

  printf( "%#x: \"%s\"\n", 0xa5, byte2bin_tbl( 0xa5 ) );
}

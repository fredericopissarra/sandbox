// Base for byte2bin.asm

#include <stdint.h>

__attribute__((noinline))
void byte2bin_swar( uint8_t byte, char *outp )
{
  uint64_t r;

  r = byte * 0x0002000800200080ULL;
  r |= byte * 0x0000040010004001ULL;
  r &= 0x0101010101010101ULL;
  r += 0x3030303030303030ULL;
  r = __builtin_bswap64( r );
  *(uint64_t *)outp = r;
  outp[8] = '\0';
}

__attribute__((noinline))
void byte2bin( uint8_t byte, char *outp )
{
  unsigned int size = 8;

  while ( size-- )
  {
    *outp++ = '0' + !!(byte & 0x80);
    byte <<= 1;
  }
  *outp = '\0';
}

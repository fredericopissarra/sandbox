#include <stdlib.h>
#include <gtk/gtk.h>

int main(int argc, char *argv[])
{
  GtkBuilder *builder;
  GtkWidget *window;

  gtk_init(&argc, &argv);

  // Instancia o GtkBuilder para a janela "App.glade".
  // Conecta todos os sinais.
  builder = gtk_builder_new_from_file("App.glade");
  gtk_builder_connect_signals(builder, NULL);  

  // Obtem o ponteiro para a janela e mostra-a.
  window = GTK_WIDGET(gtk_builder_get_object(builder, "window1"));
  gtk_widget_show_all(window);

  gtk_main();

  return EXIT_SUCCESS;
}

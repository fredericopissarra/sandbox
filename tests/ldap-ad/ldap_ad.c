#include <stdio.h>
#include <stdlib.h>
#define LDAP_DEPRECATED 1
#include <ldap.h>

#define HOST     "192.168.1.1"
#define DN       "username@site"
#define USER     "username"
#define PASSWD   "secret"
#define BASE_DN  "OU=MyOrg,DC=site"

int main ( int argc, char **argv )
{
  LDAP *ldap;
  LDAPMessage *answer, *entry;
  BerElement *ber;

  int  auth_method    = LDAP_AUTH_SIMPLE;
  int  ldap_version   = LDAP_VERSION3;

  char *ldap_host     = HOST;
  int   ldap_port     = LDAP_PORT;    // default LDAP port.

  char *ldap_dn       = DN;
  char *ldap_pw       = PASSWD;
  char *base_dn       = BASE_DN;

  // The search scope must be either LDAP_SCOPE_SUBTREE or LDAP_SCOPE_ONELEVEL
  int  scope          = LDAP_SCOPE_SUBTREE;

  // The search filter, "(objectClass=*)" returns everything. Windows can return
  // 1000 objects in one search. Otherwise, "Size limit exceeded" is returned.
  char *filter        = "(&(objectClass=user)(sAMAccountName=" USER "))";

  // The attribute list to be returned, use {NULL} for getting all attributes
  char *attrs[]       = {"memberOf", NULL};

  // Specify if only attribute types (1) or both type and value (0) are returned
  int  attrsonly      = 0;

  int  result;

  /* First, we print out an informational message. */
  printf ( "Connecting to host %s...\n\n", ldap_host );

  /* STEP 1: Get a LDAP connection handle and set any session preferences. */
  /* For ldaps we must call ldap_sslinit(char *host, int port, int secure) */
  if ( (ldap = ldap_init( ldap_host, ldap_port ) ) == NULL )
  {
    perror ( "ldap_init failed" );
    return EXIT_FAILURE;
  }
  else
    puts ( "Generated LDAP handle." );

  /* The LDAP_OPT_PROTOCOL_VERSION session preference specifies the client */
  /* is an LDAPv3 client. */
  result = ldap_set_option ( ldap, LDAP_OPT_PROTOCOL_VERSION, &ldap_version );

  if ( result != LDAP_OPT_SUCCESS )
  {
    perror ( ldap_err2string( result ) );
    return EXIT_FAILURE;
  }
  else
    puts ( "Set LDAPv3 client version." );

  /* STEP 2: Bind to the server. */
  // If no DN or credentials are specified, we bind anonymously to the server */
  // result = ldap_simple_bind_s( ldap, NULL, NULL );
  result = ldap_simple_bind_s ( ldap, ldap_dn, ldap_pw );

  if ( result != LDAP_SUCCESS )
  {
    fprintf ( stderr, "ldap_simple_bind_s: %s\n", ldap_err2string ( result ) );
    return EXIT_FAILURE;
  }
  else
    puts ( "LDAP connection successful." );

  /* STEP 3: Do the LDAP search. */
  result = ldap_search_s ( ldap, base_dn, scope, filter,
                           attrs, attrsonly, &answer );

  if ( result != LDAP_SUCCESS )
  {
    fprintf ( stderr, "ldap_search_s: %s\n", ldap_err2string ( result ) );
    return EXIT_FAILURE;
  }
  else
    puts ( "LDAP search successful." );

  /* Return the number of objects found during the search */
  int entries_found = ldap_count_entries ( ldap, answer );
  if ( entries_found == 0 )
  {
    fprintf ( stderr, "LDAP search did not return any data.\n" );
    return EXIT_FAILURE;
  }
  else
    printf ( "LDAP search returned %d objects.\n", entries_found );

  /* cycle through all objects returned with our search */
  for ( entry = ldap_first_entry ( ldap, answer ); entry; entry = ldap_next_entry ( ldap, entry ) )
  {
    char *dn, *attribute;

    /* Print the DN string of the object */
    dn = ldap_get_dn ( ldap, entry );
    printf ( "Found Object: %s\n", dn );

    // cycle through all returned attributes
    for ( attribute = ldap_first_attribute ( ldap, entry, &ber ); 
          attribute; 
          attribute = ldap_next_attribute ( ldap, entry, ber ) )
    {
      char **values;

      /* Print the attribute name */
      printf ( "  Found Attribute: %s\n", attribute );

      if ( ( values = ldap_get_values ( ldap, entry, attribute ) ) != NULL )
      {
        /* cycle through all values returned for this attribute */
        char **v;
        for (v = values; *v; v++)
          /* print each value of a attribute here */
          printf ( "    %s: %s\n", attribute, *v );

        ldap_value_free ( values );
      }
    }

    ldap_memfree ( dn );
  }

  ldap_msgfree ( answer );
  ldap_unbind ( ldap );

  return EXIT_SUCCESS;
}

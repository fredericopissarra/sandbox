#include <stdio.h>
#include <stdint.h>
#include <ctype.h>
#include <x86intrin.h>

// AES 128 exige 9 iterações mais a final.
#define DO_ENC_BLOCK(m,k)               \
  do {                                  \
    m = _mm_xor_si128       (m, k[ 0]); \
                                        \
    m = _mm_aesenc_si128    (m, k[ 1]); \
    m = _mm_aesenc_si128    (m, k[ 2]); \
    m = _mm_aesenc_si128    (m, k[ 3]); \
    m = _mm_aesenc_si128    (m, k[ 4]); \
    m = _mm_aesenc_si128    (m, k[ 5]); \
    m = _mm_aesenc_si128    (m, k[ 6]); \
    m = _mm_aesenc_si128    (m, k[ 7]); \
    m = _mm_aesenc_si128    (m, k[ 8]); \
    m = _mm_aesenc_si128    (m, k[ 9]); \
                                        \
    m = _mm_aesenclast_si128(m, k[10]); \
  } while(0)

#define DO_DEC_BLOCK(m,k)               \
  do {                                  \
    m = _mm_xor_si128       (m, k[10]); \
                                        \
    m = _mm_aesdec_si128    (m, k[11]); \
    m = _mm_aesdec_si128    (m, k[12]); \
    m = _mm_aesdec_si128    (m, k[13]); \
    m = _mm_aesdec_si128    (m, k[14]); \
    m = _mm_aesdec_si128    (m, k[15]); \
    m = _mm_aesdec_si128    (m, k[16]); \
    m = _mm_aesdec_si128    (m, k[17]); \
    m = _mm_aesdec_si128    (m, k[18]); \
    m = _mm_aesdec_si128    (m, k[19]); \
                                        \
    m = _mm_aesdeclast_si128(m, k[0]);  \
  } while(0)

static __m128i aes_128_key_expansion ( __m128i key, __m128i keygen )
{
  keygen = _mm_shuffle_epi32 ( keygen, _MM_SHUFFLE ( 3, 3, 3, 3 ) );

  key = _mm_xor_si128 ( key, _mm_slli_si128 ( key, 4 ) );
  key = _mm_xor_si128 ( key, _mm_slli_si128 ( key, 4 ) );
  key = _mm_xor_si128 ( key, _mm_slli_si128 ( key, 4 ) );

  return _mm_xor_si128 ( key, keygen );
}

#define AES_128_key_exp(k, rcon) \
  aes_128_key_expansion((k), _mm_aeskeygenassist_si128((k), (rcon)))

// Obs: `enc_key` precisa ser padded para ter 16 bytes de tamanho.
void aes128_load_key ( __m128i *key_schedule, char *enc_key )
{
  key_schedule[0] = _mm_loadu_si128 ( ( const __m128i * ) enc_key );
  key_schedule[1] = AES_128_key_exp ( key_schedule[0], 1 );
  key_schedule[2] = AES_128_key_exp ( key_schedule[1], 2 );
  key_schedule[3] = AES_128_key_exp ( key_schedule[2], 4 );
  key_schedule[4] = AES_128_key_exp ( key_schedule[3], 8 );
  key_schedule[5] = AES_128_key_exp ( key_schedule[4], 16 );
  key_schedule[6] = AES_128_key_exp ( key_schedule[5], 32 );
  key_schedule[7] = AES_128_key_exp ( key_schedule[6], 64 );
  key_schedule[8] = AES_128_key_exp ( key_schedule[7], 128 );
  key_schedule[9] = AES_128_key_exp ( key_schedule[8], 27 );

  key_schedule[10] = AES_128_key_exp ( key_schedule[9], 54 );

  // Calcula a chave de decodificação...
  key_schedule[11] = _mm_aesimc_si128 ( key_schedule[9] );
  key_schedule[12] = _mm_aesimc_si128 ( key_schedule[8] );
  key_schedule[13] = _mm_aesimc_si128 ( key_schedule[7] );
  key_schedule[14] = _mm_aesimc_si128 ( key_schedule[6] );
  key_schedule[15] = _mm_aesimc_si128 ( key_schedule[5] );
  key_schedule[16] = _mm_aesimc_si128 ( key_schedule[4] );
  key_schedule[17] = _mm_aesimc_si128 ( key_schedule[3] );
  key_schedule[18] = _mm_aesimc_si128 ( key_schedule[2] );
  key_schedule[19] = _mm_aesimc_si128 ( key_schedule[1] );
}

// Codifica um bloco 'plainText' para um 'cipherText' usando a chave
// expandida em key_schedule. 
void aes128_enc ( void *cipherText, void *plainText, __m128i *key_schedule )
{
  __m128i m = _mm_loadu_si128 ( plainText );
  DO_ENC_BLOCK ( m, key_schedule );
  _mm_storeu_si128 ( cipherText, m );
}

// Decodifica um bloco 'cipherText' para um 'plainText' usando a chave
// expandida em key_schedule.
void aes128_dec ( void *plainText, void *cipherText, __m128i *key_schedule )
{
  __m128i m = _mm_loadu_si128 ( ( __m128i * ) cipherText );
  DO_DEC_BLOCK ( m, key_schedule );
  _mm_storeu_si128 ( ( __m128i * ) plainText, m );
}

static void showblock ( char *p )
{
  int i;

  putchar ( '"' );

  for ( i = 0; i < 16; i++, p++ )
    if ( *p != '"' && isprint ( *p ) )
      putchar ( *p );
    else
      printf ( "\\x%02hhx", *p );

  putchar ( '"' );
}

// Código de teste (simples ECB).
int main ( void )
{
  char text[16] = "Fred";
  char key[16] = "xpto";
  char cypher[16], plain[16];
  __m128i key_schedule[20] = {0};

  fputs ( "key[]          = ", stdout ); showblock ( key ); puts( ";" );

  aes128_load_key ( key_schedule, key );
  fputs ( "key_schedule[] = ", stdout ); showblock ( (char *)key_schedule );
  for ( int i = 1; i < 20; i++ )
  { fputs ( "\n                 ", stdout ); showblock ( (char *)&key_schedule[i] ); }
  puts( ";" );

  aes128_enc ( cypher, text, key_schedule );
  fputs ( "text[]         = ", stdout ); showblock ( text ); puts( ";" );
  fputs ( "cypher[]       = ", stdout ); showblock ( cypher ); puts( ";" );

  aes128_dec ( plain, cypher, key_schedule );
  fputs ( "plain[]        = ", stdout ); showblock ( plain ); puts( ";" );

  return EXIT_SUCCESS;
}

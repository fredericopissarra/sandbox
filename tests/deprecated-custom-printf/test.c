/* custom_printf.c */
#include <stddef.h>
#include <stdio.h>
#include <printf.h>

// This function prints the arguments in
// a stream using the information from the
// convertion.
static int print_bin ( FILE *,
                       const struct printf_info * restrict,
                       const void * const * restrict );

// This function checks the arguments.
static int print_bin_info ( const struct printf_info * restrict,
                            size_t, int * restrict );

void main ( void )
{
  // This is a deprecated function, but still works on Linux.
  register_printf_function ( 'b',
                             print_bin, print_bin_info );

  printf ( "%d -> %1$#x -> %1$#b\n", 201 );
}

//--------------

int print_bin_info ( const struct printf_info * restrict info,
                     size_t n, int * restrict types )
{
  // Our converter will only accept 'int'.
  // I should deal with `char`, `long int` and `long long int` as well...
  // No more preparations must be done for this converter.
  *types = PA_INT;
  return 1;
}

int print_bin ( FILE *stream,
                const struct printf_info * restrict info,
                const void * const * restrict args )
{
  #define MAX_BUFFER_SIZE 33

  // This is a simple converter. I should deal with width.precision and flags
  // from the format.

  char buffer[MAX_BUFFER_SIZE];
  char *p = buffer + sizeof buffer - 1;

  buffer[sizeof buffer - 1] = '\0';

  unsigned int value = * ( unsigned int * )*args;

  // Convert the significant bits to string...
  do
  {
    *--p = '0' + ( value & 1 );
  }
  while ( value >>= 1 );

  // Finally, prints the string to the strem (adding 0b if the # modifier is used).
  // returns the # of chars printed.
  return fprintf ( stream, "%s%s", info->alt ? "0b" : "", p );
}

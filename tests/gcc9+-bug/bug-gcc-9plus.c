/* Thanks to @luigi */
#include <stdio.h>

int main ( void )
{
  unsigned int a; // issue only occurs for unsigned
  unsigned int b;
  unsigned int c;

  c = 0;
  for ( a = 0; a < 10; a++ ) // upper limit for variable a doesn't really matter
    for ( b = 0; b < 2; b++ ) // bug only for b < 2, no issue for other limits
    {
      c++; // 1st iteration: a == 0, b == 0, c == 1
           // 2nd iteration: a == 0, b == 1, c == 2
           // 3rd iteration: a == 1, b == 0; c == 3 (exit with 123).

      printf( "a=%u, c=%u\n", a, c );

      if ( c < a ) // c will *never* be smaller than a from now on
        return 123; // and yet this is somehow reachable with -O1/2/3 (GCC 9+)
    }

  return 0; // with -O0 this returns 0 properly
}

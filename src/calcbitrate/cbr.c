#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <libgen.h>

int main( int argc, char *argv[] )
{
  double width, height, fps;

  if ( argc != 4 )
  {
    fprintf( stderr, "Usage: %s <width> <height> <fps>\n",
      basename( argv[0] ) );

    return EXIT_FAILURE;
  }

  width = atof( argv[1] );
  height = atof( argv[2] );
  fps = atof( argv[3] );

  printf( "Bitrate (min) = %f Mb/s\n", width * height * fps / 13.824e6 );

  return EXIT_SUCCESS;
}

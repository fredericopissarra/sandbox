#include <string.h>
#include <strl.h>

#ifdef __cplusplus
extern "C" {
#endif

size_t strlcpy ( char * restrict dst, const char * restrict src, size_t maxlen )
{
  const size_t srclen = strlen ( src );

  if ( srclen + 1 < maxlen )
    memcpy ( dst, src, srclen + 1 );
  else if ( maxlen != 0 )
  {
    memcpy ( dst, src, maxlen - 1 );
    dst[maxlen - 1] = '\0';
  }

  return srclen;
}

// Similar, mas para concatenação.
size_t strlcat ( char * restrict dst, const char * restrict src, size_t maxlen )
{
  char *d = dst;
  const char *s = src;
  size_t n = maxlen;
  size_t dlen;

  while ( n-- != 0 && *d != '\0' )
    d++;

  dlen = d - dst;
  n = maxlen - dlen;

  if ( n == 0 )
    return dlen + strlen ( s );

  while ( *s != '\0' )
  {
    if ( n != 1 )
    {
      *d++ = *s;
      n--;
    }

    s++;
  }

  *d = '\0';

  return dlen + ( s - src ); /* count does not include NUL */
}

#ifdef __cplusplus
}
#endif

#ifndef __GETOPT_H__
#define __GETOPT_H__

#pragma warning(disable:4996)

/* All the headers include this file. */
#include <crtdefs.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <stdio.h>
#include <windows.h>

#ifdef __cplusplus
extern "C" {
#endif

#define REPLACE_GETOPT    /* use this getopt as the system getopt(3) */

#ifdef REPLACE_GETOPT
  extern int     opterr;       /* if error message should be printed */
  extern int     optind;       /* index into parent argv vector */
  extern int     optopt;       /* character checked for validity */
  #undef  optreset      /* see getopt.h */
  #define optreset    __mingw_optreset
  extern int     optreset;     /* reset getopt */
  extern char    *optarg;      /* argument associated with option */
#endif

#ifndef __CYGWIN__
  #define __progname __argv[0]
#else
  extern char __declspec(dllimport) *__progname;
#endif

#ifdef REPLACE_GETOPT
  /*
   * getopt --
   *  Parse argc/argv argument vector.
   *
   * [eventually this will replace the BSD getopt]
   */
  int getopt(int nargc, char *nargv[], const char *options);
#endif /* REPLACE_GETOPT */

#ifdef _BSD_SOURCE
/*
 * BSD adds the non-standard `optreset' feature, for reinitialisation
 * of `getopt' parsing.  We support this feature, for applications which
 * proclaim their BSD heritage, before including this header; however,
 * to maintain portability, developers are advised to avoid it.
 */
  # define optreset  __mingw_optreset
  extern int optreset;
#endif

/*
 * POSIX requires the `getopt' API to be specified in `unistd.h';
 * thus, `unistd.h' includes this header.  However, we do not want
 * to expose the `getopt_long' or `getopt_long_only' APIs, when
 * included in this manner.  Thus, close the standard __GETOPT_H__
 * declarations block, and open an additional __GETOPT_LONG_H__
 * specific block, only when *not* __UNISTD_H_SOURCED__, in which
 * to declare the extended API.
 */

#if !defined(__UNISTD_H_SOURCED__) && !defined(__GETOPT_LONG_H__)
#define __GETOPT_LONG_H__
#endif

#ifdef __GETOPT_LONG_H__
struct option   /* specification for a long form option...  */
{
  const char *name;   /* option name, without leading hyphens */
  int         has_arg;    /* does it take an argument?    */
  int        *flag;   /* where to save its status, or NULL  */
  int         val;    /* its associated status value    */
};

enum        /* permitted values for its `has_arg' field...  */
{
  no_argument = 0,        /* option never takes an argument */
  required_argument,    /* option always requires an argument */
  optional_argument   /* option may take an argument    */
};

/*
 * getopt_long --
 *  Parse argc/argv argument vector.
 */
int getopt_long(int nargc, char *nargv[], const char *options,
    const struct option *long_options, int *idx);

/*
 * getopt_long_only --
 *  Parse argc/argv argument vector.
 */
int getopt_long_only(int nargc, char *nargv[], const char *options,
    const struct option *long_options, int *idx);

/*
 * Previous MinGW implementation had...
 */
#ifndef HAVE_DECL_GETOPT
/*
 * ...for the long form API only; keep this for compatibility.
 */
# define HAVE_DECL_GETOPT 1
#endif

#endif /* __GETOPT_LONG_H__ */

#ifdef __cplusplus
}
#endif

#endif /* __GETOPT_H__ */

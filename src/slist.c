// Sedgewick method.

#include <stdio.h>
#include <stdlib.h>

// A "base" node.
struct node { struct node *next; };

// A list (with sentinel nodes).
struct list { struct node head, tail; };

// list initializer
#define LIST_INIT(list__) \
  (struct list){ .head.next = &(list__).tail, \
                 .tail.next = &(list__).tail }

_Bool slist_empty( struct list *list )
{ return list->head.next == &list->tail; }

void slist_insertAfter( struct node *node, struct node *new )
{
  new->next = node->next;
  node->next = new;
}

// return NULL if list is empty (or the next node is the tail).
struct node *slist_deleteAfter( struct node *node )
{
  struct node *next;

  next = node->next;

  if ( next == next->next )
    return NULL;

  node->next = next->next;

  return next;
}

// macro for list iteration.
#define for_each(iter__,list__) \
  for ( (iter__) = (list__)->head.next; \
        (iter__) != (iter__)->next; \
        (iter__) = (iter__)->next )

// using an auxiliary pointer to allow deletion of "current" node...
#define for_each_safe(iter__,tmp__,list__) \
  for ( (iter__) = (list__)->head.next, (tmp__) = (iter__)->next; \
        (iter__) != (iter__)->next; \
        (iter__) = (tmp__) )

void slist_clear( struct list *list, void (*dtor)( void * ) )
{
  while ( 1 )
  {
    struct node *node;

    node = slist_deleteAfter( &list->head );
    if ( ! node )
      break;

    // call the destructor if it's a valid node.
    if ( dtor )
      dtor( node );
  }
}


int main( void )
{
  struct list list = LIST_INIT(list);
  struct node *i;

  struct mynode {
    struct node *next;  // next pointer MUST be here!

    int value;
  };

  struct mynode *my;

  my = malloc( sizeof *my ); my->value = 1;
  slist_insertAfter( &list.head, (struct node *)my );
  i = (struct node *)my;  // using i as the previous node for the next insertion.

  my = malloc( sizeof *my ); my->value = 2;
  slist_insertAfter( i, (struct node *)my );
  i = (struct node *)my;

  my = malloc( sizeof *my ); my->value = 3;
  slist_insertAfter( i, (struct node *)my );

  for_each( i, &list )
  {
    struct mynode *p = (struct mynode *)i;

    printf( "%d ", p->value );
  }
  
  putchar( '\n' );

  slist_clear( &list, free );   // unlink and free() the nodes.
}

// Daniel Lemire's decimal digits counter routine (fixed).
#include <limits.h>

#if 1
static inline unsigned int int_log2(unsigned int x)
{
  return sizeof x * CHAR_BIT - 1 - __builtin_clz(x);
}

int digits_count(unsigned int x)
{
  static const unsigned int table[] =
  {
    9, 99, 999, 9999, 99999,
    999999, 9999999, 99999999, 999999999
  };

  if ( ! x ) 
    return 1;

  unsigned int y = 9 * int_log2(x) / 64;

  y += x > table[y];
  return y + 1;
}
#else
int digits_count( unsigned int );
#endif

#if 1
// test
#include <stdio.h>

int main( void )
{
  static const unsigned int a[] = { 0, 1, 9, 10, 19, 99, 100, 999, UINT_MAX };
  const unsigned int *p, *q;

  q = a + sizeof a / sizeof a[0];

  for ( p = a; p < q; p++ )
    printf( "%u -> %d decimal digits.\n", *p, digits_count( *p ) );
  
}
#endif

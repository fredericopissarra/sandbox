#ifndef LIST_H_
#define LIST_H_

/* Circular list management. */

// Need to insert this at the beginning of "overloaded" structures.
// See list_head, below.
#define LIST_HEAD_PTRS  struct list_head *prev_, *next_

// name is an identifier which is NOT a pointer.
// This macro initialized a 'static' list.
#define LIST_HEAD_INIT(name__) { &(name__), &(name__) }

// Para facilitar a declaração de uma lista vazia.
#define DECLARE_LIST(name__) struct list_head name = LIST_HEAD_INIT(name__)

// Base structure for a list head.
// All derived node structures must implement LIST_HEAD_PTRS at the beginning.
struct list_head { LIST_HEAD_PTRS; };

// Initialize a list.
static inline void list_init( struct list_head *list )
{ list->prev_ = list->next_ = list; }

// Add an element between prev and next (private function).
static inline void list_add_(struct list_head *element,
                             struct list_head *prev, struct list_head *next )
{
  element->prev_ = prev;
  element->next_ = next;
  prev->next_ = element;
  next->prev_ = element;
}

// Add an element at the begining of the list.
static inline void list_add_begin( struct list_head *element,
                                   struct list_head *head )
{ list_add_( element, head, head->next_ ); }

// Add an element at the end of the list.
static inline void list_add_end( struct list_head *element,
                                 struct list_head *head )
{ list_add_( element, head->prev_, head ); }

// Delete an element between prev and next (private function).
static inline void list_del_( struct list_head *prev,
                              struct list_head *next )
{
  prev->next_ = next;
  next->prev_ = prev;
}

// Delete an element from a list.
static inline void list_del( struct list_head *element )
{ list_del_ ( element->prev_, element->next_ ); }

// Check if a list is empty.
static inline int list_is_empty( struct list_head *head )
{ return head == head->next_; }

// Check if an iterator is at head.
static inline int list_at_head( struct list_head *iter, struct list_head *head )
{ return iter == head; }

// Check if an element is at the beginning of a list.
static inline int list_is_first( struct list_head *element, struct list_head *head )
{ return element == head->next_; }

// Check if an element is at the end of a list.
static inline int list_is_last( struct list_head *element, struct list_head *head )
{ return element == head->prev_; }

// Get container ptr from member ptr.
#define container_of(ptr__,type__,member__) \
  ({ void *tmp__ = ( void * )(ptr__); \
    ( ( type__ * ) ( tmp__ - offsetof( type__, member__ ) ) ); })

// Macros to iterate through the list.

// iterate forward
#define list_for_each( iter__, head__ ) \
  for ( iter__ = (head__)->next_; iter__ != (head__); iter__ = iter__->next_ )

// iterate backward
#define list_for_earch_prev( iter__, head__ ) \
  for ( iter__ = (head__)->prev_; iter__ != (head__); iter__ = iter__->prev_ )

// These are safe versions of above. They use a temporary pointer to hold
// the 'next' (or 'prev') node from iterator.
#define list_for_each_safe( iter__, tmp__, head__ ) \
  for ( iter__ = (head__)->next_, tmp__ = (iter__)->next_; \
        iter__ != (head__); \
        iter__ = tmp__, tmp__ = (iter__)->next_ )

#define list_for_each_prev_safe( iter__, tmp__, head__ ) \
  for ( iter__ = (head__)->prev_, tmp__ = (iter__)->prev_; \
        iter__ != (head__); \
        iter__ = tmp__, tmp__= (iter__)->prev_ )

#endif

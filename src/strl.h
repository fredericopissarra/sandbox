#ifndef STRL_H_
#define STRL_H_

#include <stddef.h>

#ifdef __cplusplus
extern "C" {
#endif

size_t strlcpy ( char * restrict dst, const char * restrict src, size_t maxlen );
size_t strlcat ( char * restrict dst, const char * restrict src, size_t maxlen );

#ifdef __cplusplus
}
#endif

#endif

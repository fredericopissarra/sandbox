#ifndef CYCLE_COUNTING_INCLUDED__
#define CYCLE_COUNTING_INCLUDED__

#ifndef __GNUC__
# error Works only on GCC or CLANG
#endif

/* ==========================================
    Quick & Dirty cycle counting...

    As funções usadas para contar a quantidade de ciclos
    de clock gastos num bloco de código.

    Exemplo de uso:

      counter_T cnt;

      cnt = BEGIN_TSC();      
      f();
      cnt = END_TSC(cnt);

    Defina SYNC_MEM se quiser uma serialização mais completa (mfence).
    Isso é normalmente desnecessário!
   ========================================== */
#include <stdint.h>

#if defined(__x86_64__) || defined(__i386__)
  #include <cpuid.h>

  static inline uint64_t BEGIN_TSC( void )
  {
    int a, b, c, d;

    #ifdef SYNC_MEM
      __builtin_ia32_mfence();
    #endif
    
    __cpuid( 0, a, b, c, d );

    return __builtin_ia32_rdtsc();
  }

  static inline uint64_t END_TSC( volatile uint64_t old )
  {
    #ifdef SYNC_MEM
      __builtin_ia32_mfence();
    #endif

    return __builtin_ia32_rdtsc() - old;
  }
#elif defined(__arm__)
  #if __ARM_32BIT_STATE == 1
    // This works on ARMv7+?! 
    #if __ARM_ARCH__ >= 7
      static inline uint64_t BEGIN_TSC( void )
      {
        unsigned int r0, r1;

        __asm__ __volatile__ (
      #ifdef SYNC_MEM
          "dsb\n\t"
          "isb\n\t"
      #endif
          "mrrc p15,1,%0,%1,c14"        // FIXME: Must check this.
                                        //        Get the virtual counter.
          : "=r" (r0), "=r" (r1)
        );

        return ((uint64_t)r1 << 32) | r0;
      }

      static inline uint64_t END_TSC( volatile uint64_t old )
      {
        unsigned int r0, r1;

        __asm__ __volatile__ (
      #ifdef SYNC_MEM
          "dsb\n\t"
          "isb\n\t"
      #endif
          "mrrc p15,1,%0,%1,c14"      // FIXME: Must check this.
                                      //        get the virtual counter.
          : "=r" (r0), "=r" (r1)
        );

        return (((uint64_t)r1 << 32) | r0) - old;
      }
    #else
      #error ARMv8 or superior only.
    #endif
  #else   // otherwise we are in aarch64 mode.
    static inline uint64_t BEGIN_TSC( void )
    {
      uint64_t count;

      __asm__ __volatile__ ( 
    #ifdef SYNC_MEM
      "dsb\n\t"
      "isb\n\t"
    #endif
      "mrs %0,cntvct_el0" : "=r" (count) );

      return count;
    }

    static inline uint64_t END_TSC( volatile uint64_t old )
    {
      uint64_t count;

      __asm__ __volatile__ ( 
    #ifdef SYNC_MEM
        "dsb\n\t"
        "isb\n\t"
    #endif
        "mrs %0,cntvct_el0" : "=r" (count) 
      );

      return count - old;
    }
  #endif
#else
# error i386, x86-64 and ARM only.
#endif

#pragma GCC diagnostic pop 

#endif

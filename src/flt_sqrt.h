#ifndef FLT_SQRT_H_
#define FLT_SQRT_H_

// Tries to correct the problem with sqrt() calls in GCC, until version 8.2.

// Intel & AMD
#if defined(__i386__) || defined(__x86_64__)
# ifdef __SSE_MATH__
inline float sqrtf_( float x )
{
  __asm__ __volatile__ ( "sqrtss %0,%0" : "+x" (x) );
  return x;
}
# endif

# ifdef __SSE2_MATH__
inline double sqrt_( double x )
{
  __asm__ __volatile__ ( "sqrtsd %0,%0" : "+x" (x) );
  return x;
}
# endif

# ifndef __SSE_MATH__
inline float sqrtf_( float x )
{
  __asm__ __volatile__ ( "fsqrt" : "+t" (x) );
  return x;
}
# endif

# ifndef __SSE2_MATH__
inline double sqrt_( double x )
{
  __asm__ __volatile__ ( "fsqrt" : "+t" (x) );
  return x;
}
# endif

/* long double always use fp87! */
inline long double sqrtl_( long double x )
{
  __asm__ __volatile__ ( "fsqrt" : "+t" (x) );
  return x;
}

#endif

// ARM (AArch64 & AArch32)
// Assuming newest architectures capable of VFP or NEON.
#ifdef __ARM_ARCH__
# if defined(__ARM_ARCH_ISA_A64) && __ARM_ARCH_ISA_A64__ != 0
inline float sqrtf_( float x )
{
  __asm__ __volatile__ ( "fsqrt %0,%0" : "+w" (x) );
  return x;
}
# else
inline float sqrtf_( float x )
{
  __asm__ __volatile__ ( "fsqrt %0,%0" : "+t" (x) );
  return x;
}
# endif

inline double sqrt_( double x )
{
  __asm__ __volatile__ ( "fsqrt %0,%0" : "+w" (x) );
  return x;
}
#endif

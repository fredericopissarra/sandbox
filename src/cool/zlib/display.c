/* display.c

  Install zlib1g-dev first!
  Compile with:
    gcc -O2 -o display display.c -lz 
*/
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <zlib.h>

#define CHUNK 4096  /* Or other suitable buffer size */

// Here we put the array with the graphics created by txt2h.
// static char graphics[] = "...";

int main( void )
{
  int ret;
  unsigned int have;
  char outbuff[CHUNK];
  z_stream strm = {};

  if ( inflateInit( &strm ) != Z_OK )
  {
    fputs( "ERROR initializing zlib.\n", stderr );
    return EXIT_FAILURE;
  }

  // 'graphics' is the array identifier.
  strm.avail_in = sizeof graphics;
  strm.next_in = graphics;
  do {
    strm.avail_out = CHUNK;
    strm.next_out = outbuff;
  
    // Don't need to check for errors here. We're dealing with
    // 'memory' streams with correct data.
    ret = inflate( &strm, Z_NO_FLUSH );

    // write to screen.
    have = CHUNK - strm.avail_out;
    if ( have )
      write( STDOUT_FILENO, outbuf, have );
  } while ( ret != Z_STREAM_END );

  inflateEnd( &strm );

  return EXIT_SUCCESS;
}

#include <stdio.h>
#include <stdlib.h>
#include <libgen.h>
#include <sys/stat.h>
#include <zlib.h>

#define CHUNK 16384

// Just to test if the filenames provided refer to the same file or not (Unix).
// 1 means same file or error stating... 0 means not the same file.
static _Bool same_file( const char * restrict pname1, const char * restrict pname2 )
{
  struct stat s1, s2;
  _Bool r;

  r = ! stat( pname1, &s1 ) && ! stat( pname2, &s2 );

  if ( r )
    r = s1.st_ino == s2.st_ino;

  return r;
}

// Write bytes in an array, in C - don't check for errors here just to
// stay simple.
static void write_bytes( FILE *f, char *p, size_t size )
{
  static int sz;

  for ( size_t i = 0; i < size; i++ )
  {
    if ( ! sz )
      fputs( "\n  ", f );

    fprintf( f, "0x%02hhx,", *p++ );
    if ( ++sz == 8 )
      sz = 0;
  }
}

int main( int argc, char *argv[] )
{
  if ( argc != 3 )
  {
    fprintf( stderr, "Usage: %s <infile> <out_c_file>\n", basename( argv[0] ) );
    return EXIT_FAILURE;
  }

  if ( same_file( argv[1], argv[2] ) )
  {
    fputs( "ERROR: Same file provided or failure to stat one of them.\n", stderr );
    return EXIT_FAILURE;
  }

  FILE *fin, *fout;
  size_t have;
  int flush, ret;
  z_stream zstrm = {};
  static char inbuf[CHUNK];
  static char outbuf[CHUNK];

  fin = fopen( argv[1], "rb" );
  if ( ! fin )
  {
    fprintf( stderr, "Cannot open file '%s'.\n", argv[1] );
    return EXIT_FAILURE;
  }

  // This probably will never fail. Here only to not allow creating the output
  // file in case of error.
  if ( deflateInit( &zstrm, Z_BEST_COMPRESSION ) )
  {
    fclose( fin );
    fputs( "ERROR initializing deflation.\n", stderr );
    return 1;
  }

  fout = fopen( argv[2], "w" );
  if ( ! fout )
  {
    fclose( fin );
    fprintf( stderr, "Cannot create or truncate file '%s'.\n", argv[2] );
    return EXIT_FAILURE;
  }

  // Ok... write the output array...
  fputs( "static char text[] = {", fout );

  // We'll read, possibly, multiple blocks of CHUNK size...
  do {
    zstrm.next_in = inbuf;
    zstrm.avail_in = fread( inbuf, 1, CHUNK, fin );

    // Remember: fread() will affect file flags in `fin`, so we
    // can use ferror() and feof() safely. And fread return # of elements read
    // when called this way.

    if ( ferror( fin ) )
    {
      fputs( "ERROR reading input file.\n", stderr );
    error:
      fclose( fin );
      fclose( fout );
      unlink( argv[2] );    // delete output file 'case we have an error!

      deflateEnd( &zstrm );
      return EXIT_FAILURE;
    }

    // ask deflate() not to flush data unless it is the last block.
    flush = feof( fin ) ? Z_FINISH : Z_NO_FLUSH;

    // We'll possibly write multiple compressed blocks from zstrm...
    do {
      zstrm.avail_out = CHUNK;
      zstrm.next_out = outbuf;

      // This possibly won't fail, but we should test it...
      ret = deflate( &zstrm, flush );
      if ( ret == Z_STREAM_ERROR )
      {
        fputs( "ERROR compressing...\n", stderr );
        goto error;
      }

      // If we have some bytes in the output buffer, write them...
      have = CHUNK - zstrm.avail_out;
      if ( have )
        write_bytes( fout, outbuf, have );

      // if buffer is full, do it again, because there can be more data to compress...
    } while ( ret == Z_OK && ! zstrm.avail_out );
    // OBS: This can't be test as `ret != Z_STREAM_END` because this status is
    //      only valid if flusing is set to Z_FINISH (happening only in the last
    //      block to be processed. So we have to test if default() returns Z_OK
    //      AND if the buffer is full (no more space available for output).

    // Keep doing it until we reach the last input block. 
  } while ( flush != Z_FINISH );

  fputs( "\n};\n", fout );

  deflateEnd( &zstrm );

  fclose ( fin );
  fclose ( fout );

  return EXIT_SUCCESS;
}

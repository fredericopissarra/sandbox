/* boo.c

   Compile:
     cc -O2 -mtune=native -s -fno-stack-protector -o boo boo.c
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef DEBUG
#define NOINLINE __attribute__((noinline))
#else
#define NOINLINE
#endif

/* $ = white,
   o = lightgray,
   " = faint lightgray,
   space = black.
   \n = next line. */
static const char boo[] = 
"                          oooo$$$$$$$$$$$$oooo\n"
"                      oo$$$$$$$$$$$$$$$$$$$$$$$$o\n"
"                   oo$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$o         o$   $$ o$\n"
"   o $ oo        o$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$o       $$ $$ $$o$\n"
"oo $ $ \"$      o$$$$$$$$$    $$$$$$$$$$$$$    $$$$$$$$$o       $$$o$$o$\n"
"\"$$$$$$o$     o$$$$$$$$$      $$$$$$$$$$$      $$$$$$$$$$o    $$$$$$$$\n"
"  $$$$$$$    $$$$$$$$$$$      $$$$$$$$$$$      $$$$$$$$$$$$$$$$$$$$$$$\n"
"  $$$$$$$$$$$$$$$$$$$$$$$    $$$$$$$$$$$$$    $$$$$$$$$$$$$$  \"\"\"$$$\n"
"   \"$$$\"\"\"\"$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$     \"$$$\n"
"    $$$   o$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$     \"$$$o\n"
"   o$$\"   $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$       $$$o\n"
"   $$$    $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$\" \"$$$$$$ooooo$$$$o\n"
"  o$$$oooo$$$$$  $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$   o$$$$$$$$$$$$$$$$$\n"
"  $$$$$$$$\"$$$$   $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$     $$$$\"\"\"\"\"\"\"\"\n"
" \"\"\"\"       $$$$    \"$$$$$$$$$$$$$$$$$$$$$$$$$$$$\"      o$$$\n"
"            \"$$$o     \"\"\"$$$$$$$$$$$$$$$$$$\"$$\"         $$$\n"
"              $$$o          \"$$\"\"$$$$$$\"\"\"\"           o$$$\n"
"               $$$$o                                o$$$\"\n"
"                \"$$$$o      o$$$$$$o\"$$$$o        o$$$$\n"
"                  \"$$$$$oo     \"\"$$$$o$$$$$o   o$$$$\"\"\n"
"                     \"\"$$$$$oooo  \"$$$o$$$$$$$$$\"\"\"\n"
"                        \"\"$$$$$$$oo $$$$$$$$$$\n"
"                                \"\"\"\"$$$$$$$$$$$\n"
"                                    $$$$$$$$$$$$\n"
"                                     $$$$$$$$$$\"\n"
"                                      \"$$$\"\"\"\"";  // no final '\n'.

// Obtém as dimensões da figura com base no ASCII art.
NOINLINE
static void getdimensions( const char *p, unsigned int *maxx, unsigned int *maxy )
{
  unsigned int x;

  *maxx = x = 0;
  *maxy = 1;

  while ( *p )
  {
    if ( *p++ == '\n' )
    {
      (*maxy)++;

      if ( x > *maxx )
        *maxx = x;

      x = 0;
    }
    
    x++;
  }
}

NOINLINE
static void makepicture( void *p, unsigned maxx, unsigned maxy, const char *mapp, const unsigned char *palette )
{
  unsigned char *imgp;
  unsigned int y, offset;

  imgp = p;
  y = 0;
  while ( y < maxy )
  {
    offset = y * maxx;
    while ( *mapp )
    {
      unsigned char c;

      c = *mapp++;

      if ( c == '\n' )
        break;

      imgp[offset++] = palette[ c ];
    }

    y++;
  }
}

NOINLINE
static void writepicture( FILE *f, void *p, unsigned int maxx, unsigned int maxy )
{
  size_t size;

  fprintf( f, "P6\n%u %u\n255\n", maxx, maxy );
  
  size = maxx * maxy;
  while ( size-- )
  {
    unsigned int d;

    // Converte o índice da cor em RGB.
    d = *(unsigned char *)p++;
    d |= ( d << 8 ) | ( d << 16 );

    fwrite( &d, 3, 1, f );
  }
}

int main( int argc, char *argv[] )
{
  FILE *f;
  void *buffer;
  unsigned int maxx, maxy;

  // Nossa paleta de cores customizada para esse ASCII ART
  // em particular. 256 itens porque um 'unsigned char' pode
  // ir de 0 a 255.
  static const unsigned char palette[256] =
    { ['$']=0xff, ['o']=0x3f, ['"']=0x1f };

  if ( ! *++argv )
  {
    fputs( "USAGE: boo <filename>\n", stderr );
    return EXIT_FAILURE;
  }

  getdimensions( boo, &maxx, &maxy );

  // Aloca buffer da imagem. Aproveita para zerar todo o fundo (preto).
  buffer = calloc( maxx * maxy, sizeof( unsigned char ) );
  if ( ! buffer )
  {
    fputs( "ERROR allocating buffer.\n", stderr );
    return EXIT_FAILURE;
  }

  makepicture( buffer, maxx, maxy, boo, palette );

  f = fopen( *argv, "w" );
  if ( ! f )
  {
    perror( "fopen" );
    free( buffer );
    return EXIT_FAILURE;
  }

  writepicture( f, buffer, maxx, maxy );

  fclose( f );
  free( buffer );

  // Mostra ascii art e onde salvou.
  puts(boo);
  printf( "Writen as '%s' PPM graphics file.\n", *argv );

  return EXIT_SUCCESS;
}

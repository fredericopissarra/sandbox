/* boo.c

  Compile com:
    gcc -O2 -s -o boo boo.c -lz
*/
#include <unistd.h>
#include <stdlib.h>
#include <zlib.h>

// My buffer size ( 1.5 KiB + 128 B is enough in this case ).
#define CHUNK 1664

// zlib compressed text (level 9) - 255 bytes (uncompressed: 1654 bytes).
#include "boo.h"

int main( void )
{
  int ret;
  size_t have;
  char outbuff[CHUNK];    // Buffer can be on the stack (no problem).
  z_stream strm = {};     // FIXED: Need to be zeroed before hand, otherwise inflateInit fails.

  if ( inflateInit( &strm ) != Z_OK )
    return EXIT_FAILURE;

  // Setup of input buffer...
  strm.avail_in = sizeof text;
  strm.next_in = ( char * )text;

  do
  {
    // Setup of output buffer.
    strm.avail_out = CHUNK;
    strm.next_out = outbuff;

    // We don't need to check for errors.
    // Both input and output streams are buffers in memory.
    ret = inflate( &strm, Z_NO_FLUSH );

    // If there is data in output buffer, write to screen.
    have = CHUNK - strm.avail_out;
    if ( have )
      write( STDOUT_FILENO, outbuff, have );

    // Not in the stream end yet? Stay in loop.
  } while ( ret != Z_STREAM_END );

  inflateEnd( &strm );

  return EXIT_SUCCESS;
}

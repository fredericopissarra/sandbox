#!/bin/bash

cd src/

yacc -ldv project.y
lex --noline project.l
gcc -O2 -s -march=native -o parser y.tab.c lex.yy.c -lfl -lm

./parser < inp

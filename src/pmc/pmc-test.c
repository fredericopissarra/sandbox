#include <unistd.h>
#include <stdio.h>
#include "cycle_pmc.h"

int main( void )
{
  int fd;
  unsigned long c;

  if ( ( fd = perf_init(PERF_COUNT_HW_CPU_CYCLES, getpid(), ANY_CPU, 0 ) ) < 0 )
  {
    fputs( "ERROR: Cannot open perf counter.\n", stderr );
    return EXIT_FAILURE;
  }

  perf_start(fd);
  puts( "Hello." );
  c = perf_stop(fd);

  perf_destroy(fd);

  printf( "%lu cycles.\n", c );

  return EXIT_SUCCESS;
}

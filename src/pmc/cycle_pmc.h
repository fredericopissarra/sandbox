#ifndef CYCLE_PMC_H_INCLUDED_
#define CYCLE_PMC_H_INCLUDED_

#ifndef __GNUC__
# error Need GCC or clang.
#endif

#if !defined(__linux) && !defined(__x86_64)
# error Need to be linux in amd64 mode.
#endif

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <linux/perf_event.h>
#include <asm/unistd.h>

#define ANY_CPU -1
#define ANY_PID 0

// OBS: counter precisa ser um dos PERF_COUNT_HW_* (ver manpage).
// pid = 0 e cpu = -1 (todos os PIDs e CPUs).
// 
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wattributes"
__attribute__((always_inline))
static int perf_init( long counter, 
                      pid_t pid,
                      int cpu, 
                      unsigned long flags )
{
  int fd;
  struct perf_event_attr pea = {
    .type = PERF_TYPE_HARDWARE,
    .size = sizeof pea,
    .config = counter,
    .disabled = 1,
    .exclude_kernel = 1,
    .exclude_hv = 1,
  //.exclude_idle = 1   /* Precisa? */
  };

  register long g asm ("r10");
  register long f asm ("r8");

  if ( getuid() )
  {
    fputs( "ERROR: Need root privilege.\n", stderr );
    exit(EXIT_FAILURE);
  }

  f = flags | PERF_FLAG_FD_NO_GROUP;  // precisa! r8.
  g = -1;   // precisa! r10!
  __asm__ __volatile__ (
    "syscall"
      : "=a" (fd) 
      : "a" (__NR_perf_event_open),
        "D" (&pea),
        "S" (pid),
        "d" (cpu),
        "r" (g),
        "r" (f)
      : "memory" ); // necessário por causa de 'pea'.

  return fd;
}
#pragma GCC diagnostic pop

#define perf_destroy(fd) close((fd))

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wattributes"
__attribute__((always_inline))
static void perf_start( int fd )
{
  ioctl( fd, PERF_EVENT_IOC_RESET, 0 );
  ioctl( fd, PERF_EVENT_IOC_ENABLE, 0 );
}
#pragma GCC diagnostic pop

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wattributes"
__attribute__((always_inline))
static unsigned long perf_stop( int fd )
{
  unsigned long n;

  ioctl( fd, PERF_EVENT_IOC_DISABLE, 0 );

  #pragma GCC diagnostic ignored "-Wunused-result"
  read( fd, &n, sizeof n );

  return n;
}
#pragma GCC diagnostic pop

#endif

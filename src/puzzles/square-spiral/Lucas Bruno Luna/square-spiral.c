// Lucas Brunno Luna
// Dezembro-2018
//
// Revisão: Frederico Lamberti Pissarra

#include <stdlib.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

#define sqr(x) ( (x) * (x) )

static void print_answer ( uint64_t, uint64_t, uint64_t );

int main ( void )
{
  uint64_t n, i, j;
  uint64_t s, a, l, m, q, r, jump_flag, jump_mask;

  int64_t k;

  // Isso é para ser usado pela linha de comando:
  //
  // ./square-spiral <<< '1024 12000' # matrix 1024x1024, procura pelo # 12000.
  //
  if ( scanf ( "%" SCNu64 "%" SCNu64, &s, &n ) != 2 )
  {
    fputs ( "\033[31;1mERROR\033[m: Cannot convert the input data.\n", stderr );
    return EXIT_FAILURE;
  }

  if ( s == 0 )
  {
    fputs ( "\033[31;1mERROR\033[m: the side of the square must be a positive integer.\n", stderr );
    return EXIT_FAILURE;
  }

  if ( n == 0 || n > sqr ( s ) )
  {
    fputs ( "\033[31;1mERROR\033[m: value not in the square.\n", stderr );

    return EXIT_FAILURE;
  }

  //------------------------------------------------

  if ( n == sqr ( s ) )
  {
    print_answer ( n, ( s >> 1 ) + 1, ( s >> 1 ) + ( s & 1 ) );
    return EXIT_SUCCESS;
  }

  // OBS: Uso de `sqrtl` porque `long double` tem 64 bits de precisão!
  i = j = a = ceil ( 0.5 * ( s - sqrtl ( sqr ( s ) - n ) ) );
  l = s - ( 1 + ( ( a - 1 ) << 1 ) );
  m = 1 + ( ( s * ( a - 1 ) - sqr ( a - 1 ) ) << 2 );
  q = ( n - m ) / l;
  r = ( n - m ) % l;
  jump_flag = ( q & 1 ) ^ ( q >> 1 );
  jump_mask = -jump_flag;
  k = 1 | jump_mask;
  i += l & jump_mask;
  j = i;

  if ( q & 1 )
    i += k * ( l - r );
  else
    j += k * r;

  print_answer ( n, i, j );

  return EXIT_SUCCESS;
}

void print_answer ( uint64_t n, uint64_t line, uint64_t column )
{
  printf ( "The number \033[1m%" PRIu64 "\033[m is in line \033[1m%" PRIu64 "\033[m, column \033[1m%" PRIu64 "\033[m.\n",
           n, line, column );
}
